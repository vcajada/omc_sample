<?php

namespace App\Traits;

use Monolog\Logger;
use \App\Service\Analytics;

/**
 * Analytics Trait
 *
 * @author Manuel Capinha
 */
trait AnalyticsTrait
{
    /**
     * Return the Current User Model
     *
     */

    public function getDatalayer()
    {
        return \OMC\Analytics::getDataLayer();
    }
}
