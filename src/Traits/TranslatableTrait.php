<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

use \App\Service\Utils;

/*
 * USAGE for example model Article
 *

- Use TranslatableTrait App\Model\Article.
- Set $translation_foreign_key and $translated_attributes properties in model.
- Create translation model with name ArticleTranslation.
- Create translation table with name article_translation and translated_attributes. Example table below.
- Go global :D

DROP TABLE IF EXISTS `article_translations`;

CREATE TABLE `article_translations` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`article_id` int(10) unsigned DEFAULT NULL,
`locale` varchar(2) DEFAULT NULL,
`title` varchar(255) DEFAULT NULL,
`ship_mainland` float DEFAULT NULL,
`ship_islands` float DEFAULT NULL,
`expedition_time` int(11) DEFAULT 0,
`description` longtext DEFAULT NULL,
`conditions` longtext DEFAULT NULL,
`details` longtext DEFAULT '',
PRIMARY KEY (`id`),
UNIQUE KEY `locale` (`locale`,`article_id`),
KEY `idx_loc` (`locale`)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `article_option_translations` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`article_option_id` int(10) unsigned DEFAULT NULL,
`locale` varchar(2) DEFAULT NULL,
`price` decimal(10,2) DEFAULT NULL,
`cost` decimal(10,2) DEFAULT NULL,
`original_price` decimal(10,2) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `locale` (`locale`,`article_option_id`),
KEY `idx_loc` (`locale`)

) ENGINE=InnoDB DEFAULT CHARSET=latin1

alter table carts add column locale varchar(2) default 'pt';
alter table promocodes add column locale varchar(2) default 'pt';
alter table slides add column locale varchar(2) default 'pt';


// MC says:
insert into article_translations (article_id, locale, title, description, conditions)
	(select A.id, F.lang, A.title, F.description, F.conditions from articles AS A
	LEFT JOIN article_fields AS F ON F.id = A.id) ;

*/

trait TranslatableTrait
{
    private $locale_key = 'locale';

    /**
     * Get all of the models's translations.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function translations()
    {
        $model_name = self::class.'Translation';
        return $this->hasMany($model_name, $this->translation_foreign_key);
    }

    /**
     * Translate the model to the given or the current language. If doesn't exist in language provided fallback to original model.
     *
     * @param  string|null  $lang
     * @return \Pine\Translatable\Translation|null
     */
    public function translate($locale = null)
    {
        if ($locale === null) {
            $locale = Utils::getLocale();
        }

        if ($locale !== null &&
            $translation = $this->getTranslation($locale)
        ) {
            return $translation;
        } elseif ($translation = $this->getTranslation($this->getDefaultLocale())) {
            return $translation;
        } else {
            return $this;
        }
    }

    public function getDefaultLocale()
    {
        if(!isset($GLOBALS['request'])){
            return Utils::getLocale(); // Will try to use session set locale
        }
        $request = $GLOBALS['request'];
        return $request->getDefaultLocale();
    }

    /*
     * Translate model for given locale.
     */
    public function getTranslation($locale)
    {
        return $this->translations->firstWhere($this->locale_key, $locale);
    }

    /*
     * Translate model for given locale or Create new translated model if not exists.
     */
    public function getTranslationOrNew($locale = null)
    {
        if (!$locale) { $locale = Utils::getLocale(); }

        if ($translated_model = $this->getTranslation($locale)) {
            return $translated_model;
        }

        return $this->getNewTranslation($locale);
    }

    /*
     * Create translation
     */
    public function getNewTranslation($locale)
    {
        $model_name = self::class . 'Translation';
        $new_translated_model = new $model_name();
        $new_translated_model->setAttribute($this->locale_key, $locale);
        $this->translations->add($new_translated_model);

        return $new_translated_model;
    }

    /*
     * Fetch attribute. If attribute is translatable fetches translated attribute. Overwrites eloquent's get attribute
     */
    public function getAttribute($key)
    {
        if (in_array($key ,$this->translated_attributes)) {
            return $this->translate()->getAttributeValue($key);
        }
        return parent::getAttribute($key);
    }

    /*
     * Convert model to translated array. Overwrites eloquent's to array method.
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        foreach ($this->translated_attributes as $key) {
            $attributes[$key] = $this->translate()->$key;
        }

        return $attributes;
    }

    /**
    * @param array $options
    *
    * @return bool
    */
    public function save(array $options = [])
    {
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }
                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($this->fireModelEvent('saving') === false) {
                    return false;
                }
                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }
                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            return $this->saveTranslations();
        }
        return false;
    }

    protected function saveTranslations()
    {
        $saved = true;
        foreach ($this->translations as $translation) {
            if ($saved && $this->isTranslationDirty($translation)) {
                if (! empty($connection_name = $this->getConnectionName())) {
                    $translation->setConnection($connection_name);
                }
                $translation->setAttribute($this->getRelationKey(), $this->getKey());
                $saved = $translation->save();
            }
        }
        return $saved;
    }

    /*
     * Sets attribute. If attribute is translatable sets translated attribute.
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key ,$this->translated_attributes)) {
            $this->getTranslationOrNew()->$key = $value;
            return $this;
        }
        return parent::setAttribute($key, $value);
    }

    /**
    * @param \Illuminate\Database\Eloquent\Model $translation
    *
    * @return bool
    */
    protected function isTranslationDirty(Model $translation)
    {
        $dirty_attributes = $translation->getDirty();
        unset($dirty_attributes[$this->locale_key]);
        return count($dirty_attributes) > 0;
    }

    /**
     * @return string
     */
    public function getRelationKey()
    {
        if ($this->translation_foreign_key) {
            $key = $this->translation_foreign_key;
        } elseif ($this->primaryKey !== 'id') {
            $key = $this->primaryKey;
        } else {
            $key = $this->getForeignKey();
        }
        return $key;
    }
}
