<?php

namespace App\Traits;

/**
 * Current User trait.
 *
 * @author Manuel Capinha
 */
trait CurrentUserTrait
{
    /**
     * Return the Current User Model
     *
     */
    public function currentUser()
    {
        $user_session = $this->session->get('omc_user');
        $user_id = $user_session['id_user'];
        if(!$user_id){ return; }
        return \App\Model\User::find($user_id);
    }

    public function forceLogin($user){
        if(!$user){
            $this->logger->debug('forceLogin called with empty user');
            return false;
        }

        // build user session
        $this->session->set('omc_user', array(
            'id_user' => $user->id,
            'nome' => $user->name
        ));

        // Update logins for user
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();

        return true;
    }

    public function autologin($sson){
        $this->logger->debug('Autologin for ' . $sson );

        $user = \App\Service\Users::getBySSON($sson);
        if($user){
            return $this->forceLogin($user);
        }
        return false;
    }
}
