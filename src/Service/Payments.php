<?php

namespace App\Service;

use \App\Model\Cart;

class Payments extends BaseService
{

    // DEPRECATED
    public function getMBData($cartsession)
    {
        $cart = self::getCartfromSession($cartsession);
        $amount = self::getTotal($cart);

        if($cart->payment_reference){
            if($cart->payment_total == $amount){
                return [
                    'entity' => $cart->payment_entity,
                    'reference' => $cart->payment_reference,
                    'amount'    => $cart->payment_total
                ];
            }

            //$this->app->logger->warn('Cart total ammount differs from MB details, regenerating');
        }

        $user = self::getUserDataByID($cart->user_id);
        $addr = self::getUserAddress($cart->user_id);

        $additionalInfo = "";
        $name = $user->name;
        $address = $addr->addr_street . " " . $addr->addr_street2;
        $postCode = $addr->addr_cp1."-".$addr->addr_cp2;
        $city = $addr->addr_city;
        $NIC = $addr->addr_nif;
        $externalReference = $cart->id;
        $contactPhone = $addr->addr_phone;
        $email = $user->email;
        $IDUserBackoffice = -1; //-1 in most cases
        $timeLimitDays = 3; //3, 30 or 90 days; only used for entity 11249 and ignored for entity 10241 (must be 0)
        $sendEmailBuyer = false;


        //variables do store the results to show to the user
        $reference="";
        $entity="";
        $value="";
        $error="";

        $res = $this->getReferenceFromWebService(
		$mb_params['origin'], $mb_params['username'], $mb_params['password'], $mb_params['mb_api'], $amount, $additionalInfo, $name, $address,
		$postCode, $city, $NIC, $externalReference, $contactPhone, $email, $IDUserBackoffice,
		$timeLimitDays, $sendEmailBuyer,
		$reference, $entity, $value, $error);


        if($res){

            $cart->payment_reference = $reference;
            $cart->payment_entity = $entity;
            $cart->payment_total = $value;
            $cart->save();

            $paydata = array(
                'entity' => $entity,
                'reference' => $reference,
                'amount'    => $value
            );

               return $paydata;

	       }
	       else{
		      return false;
           }
    }

    public function getCCData($cartsession)
    {
        $cart = self::getCartfromSession($cartsession);
        $amount = self::getTotal($cart);

        $paydata = array(
                'cartid'    => $cart->id,
                'amount'    => $amount
            );

        return $paydata;

    }

    public static function getTotal($cart){
        return  ($cart->cart_total + $cart->cart_shipping - $cart->cart_discount);
    }

    public function getCartfromSession($cartsession)
    {
        /*
        $cart = Capsule::table('carts')
            ->select('*')
            ->where('cart_hash', '=', $cartsession)
            ->first();
            */

        $cart = \App\Model\Cart::where('cart_hash','=',$cartsession)->first();

        return $cart;
    }

    public function getUserDataByID($userid)
    {
        $user = \App\Model\User::select('*')
            ->where('id', $userid)
            ->get();

        return $user[0];
    }

    // DEPRECATED
    public function getUserAddress($userid)
    {
        $address = \App\Model\Address::where('user_id', $userid)
            ->where('is_main', 'yes')
            ->get();

        return $address[0];
    }

    // DEPRECATED
    public function getReferenceFromWebService(
		$origin, $username, $password, $mb_api, $amount, $additionalInfo, $name, $address,
		$postCode, $city, $NIC, $externalReference, $contactPhone, $email, $IDUserBackoffice,
		$timeLimitDays, $sendEmailBuyer,
		&$reference, &$entity, &$value, &$error){

        $wsURL = $mb_api;

		try
		{

			$parameters = array(
				"origin" => $origin,
				"username" => $username,
				"password" => $password,
				"amount" => $amount,
				"additionalInfo" => $additionalInfo,
				"name" => $name,
				"address" => $address,
				"postCode" => $postCode,
				"city" => $city,
				"NIC" => $NIC,
				"externalReference" => $externalReference,
				"contactPhone" => $contactPhone,
				"email" => $email,
				"IDUserBackoffice" => $IDUserBackoffice,
				"timeLimitDays" => $timeLimitDays,
				"sendEmailBuyer" => $sendEmailBuyer		
            );
			
			$client = new \SoapClient($wsURL);
					
			$res = $client->getReferenceMB($parameters);
		
			if ($res->getReferenceMBResult)
			{
				$entity = $res->entity;
				$value = number_format($res->amountOut, 2);
				$reference = $res->reference;
				$error = "";
				return true;
			}
			else
			{
				$error = $res->error;
				return false;
			}
		
		}
		catch (Exception $e){
			$error = $e->getMessage();
			return false;
		}
		
   }

    // DEPRECATED
    public function setPaid($token, $ref){

        $cart = self::getCartfromSession($token);
        if($cart->payment_reference == $ref){

            $amount = self::getTotal($cart);

            $update = \App\Model\Cart::where('id', $cart->id)
                ->update(array(
                    'pay_status' => 'PAID',
                    'value_paid' => $amount,
                    'status'    => 'DONE',
                    'date_paid' => date("Y-m-d H:i:s")
                )
            );

        }

    }

    public function processOrders($token){
        $cart = self::getCartfromSession($token);
        $orders = \App\Model\CartArticle::where('cart_id', $cart->id)
            ->get();

        foreach($orders as $order){
            $order_payment = self::setOrderPaid($order->item_id, $order->ord_total);
        }
    }

    public function setOrderPaid($orderid, $order_total){
        $update = \App\Model\CartArticle::where('item_id', $orderid)
            ->update(array(
                'ord_paid' => $order_total
            )
        );
    }

    public function setPaidCart($carthash){

        $cart = self::getCartfromSession($carthash);
        $amount = ($cart->cart_total + $cart->cart_shipping - $cart->cart_discount);

            $update = \App\Model\Cart::where('id', $cart->id)
                ->update(array(
                    'pay_status' => 'PAID',
                    'value_paid' => $amount,
                    'status'    => 'DONE',
                    'date_paid' => date("Y-m-d H:i:s")
                )
            );
        return true;
    }

    public function curlPost($args)
    {
        $baseURL = $args['url'];
        $params  = $args['params'];
        $timeout = (isset($args['timeout']) ? $args['timeout'] : 5);

        $contentTypes = array(
            'json' => 'application/json',
        );


        $headers = array();
        if (isset($args['contentType']) && $contentTypes[$args['contentType']]) {
            $headers[] = 'Content-Type: ' . $contentTypes[$args['contentType']];
        }

        $payload = (isset($args['contentType']) ? json_encode($params) : http_build_query($params));

        $fp = fopen('/tmp/curl_errorlog.txt', 'w');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $baseURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_STDERR, $fp);


        if (isset($args['username'])) {
            curl_setopt($ch, CURLOPT_USERPWD, $args['username'] . ":" . $args['password']);
        }

        if ($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $curlResp  = curl_exec($ch);
        $curlError = curl_error($ch);

        curl_close($ch);


        return array(
            'response' => $curlResp,
            'error'    => $curlError,
        );

    }

}

?>
