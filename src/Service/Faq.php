<?php

namespace App\Service;

use Symfony\Component\Translation\TranslatorInterface;

class Faq
{

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getTranslatedFaqs() {

        $translated_faqs = [
            [
                'title' => $this->translator->trans('Como se faz o pagamento?'),
                'texts' => [
                    $this->translator->trans('Pagar é fácil, seguro e sem complicações! Pode pagar com Cartão de Crédito Visa, Mastercard ou Cartão Multibanco.'),
                    $this->translator->trans('Garantimos o pagamento seguro em todas as suas compras.'),
                    $this->translator->trans('<ol>
                        <li>Escolha o colchão que pretende adquirir com as respectivas medidas e clique em comprar.</li>
                        <li>Preencha os dados de entrega e facturação, escolha o modo de pagamento que prefere (Visa/Mastercard ou Multibanco) e clique em Finalizar Compra.</li>
                        <li>Introduza os dados do seu Cartão de Crédito ou pague as respectivas referências multibanco no seu Homebanking ou numa caixa multibanco. E já está! É só aguardar pela entrega da sua encomenda!</li>
                    </ol>'),
                    $this->translator->trans('<b>Observação importante sobre pagamentos com cartão de crédito:</b>'),
                    $this->translator->trans('A plataforma de pagamentos do site <a href="/">O meu Colchão</a> utiliza o serviço 3D-Secure, que reforça a segurança das transações online. Este serviço é gratuito e permite realizar pagamentos on-line com segurança acrescida, através da introdução de um código numérico de utilização única (um código por compra) enviado por SMS para o telemóvel do titular do cartão. Está disponível para cartões das redes MasterCard/Maestro e Visa/Visa Electron. A ativação do serviço 3D-Secure tem que ser solicitada à entidade bancária emissora do seu Cartão de Crédito. A informação do seu cartão de crédito nunca é armazenada nos nossos servidores e é apenas responsabilidade da SIBS. Se não tiver este serviço ativo, terá que recorrer ao pagamento através do Multibanco, escolhendo a outra opção.'),
                ],
            ],
            [
                'title' => $this->translator->trans('Até quando posso devolver um produto'),
                'texts' => [
                    $this->translator->trans('Pode devolver um produto até 30 dias após a respectiva entrega. O produto tem que ser entregue em perfeito estado de conservação e com todo o seu conteúdo original (embalagens incluídas). Não são aceites devoluções de produtos incompletos, danificados pelo cliente. Todas as devoluções têm que ser enviadas ou apresentadas com a respectiva factura e <a href="//www.omeucolchao.pt/downloads/Impresso_Assistencia_Tecnica_OMC2018.pdf" target="_blank">este formulário preenchido</a>. Para mais informações consulte a nossa <a href="/politica-devolucoes/">política de devoluções.</a>')
                ],
            ],
            [
                'title' => $this->translator->trans('Como funcionam as entregas'),
                'texts' => [
                    $this->translator->trans('Após o pagamento da compra, a encomenda é processada. A entrega do colchão será feita na morada indicada no processo de compra, podendo demorar até 15 dias úteis a ser feita a respectiva entrega. Em caso de qualquer dúvida por favor utilize o formulário de contacto disponível na área de Suporte ao Cliente.')
                ],
            ],
            [
                'title' => $this->translator->trans('Como funcionam os portes de envio'),
                'texts' => [
                    $this->translator->trans('Entregas ao domicílio (indique a morada onde deseja receber o produto) gratuitas para Portugal continental:'),
                    $this->translator->trans('Envios para as ilhas de colchões apenas de colchões enrolados com um valor extra de 39€ de portes de envio que são automaticamente somados ao valor da compra se optar por entrega para as ilhas.')
                ],
            ],
            [
                'title' => $this->translator->trans('Li todas as perguntas mas continuo com dúvidas. O que posso fazer?'),
                'texts' => [
                    $this->translator->trans('Para qualquer questão pode sempre contactar-nos através da nossa zona de Contactos disponível em www.omeucolchao.pt/contacto/ ou através do número 213 261 361.')
                ]
            ]
        ];

        return $translated_faqs;
    }

}


?>
