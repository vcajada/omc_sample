<?php
namespace App\Service;

use \App\Model\CartArticle;

use App\Model\Cart;

class Carts extends BaseService
{

    private $data;

    public function __get($key)
    {
        return $this->data->$key;
    }

    public function __set($key, $value)
    {
        return $this->data->$key = $value;
    }

    public function loadById($id, $with = []) {
        if ($id) {
            $cart_query = Cart::where('id', '=', $id);
            if (!empty($with)) {
                $cart_query->with($with);
            }
            $this->data = $cart_query->first();
        }
    }

    public function loadByHash($hash, $with = []) {
        if ($hash) {
            $cart_query = Cart::where('cart_hash', '=', $hash);
            if (!empty($with)) {
                $cart_query->with($with);
            }
            $this->data = $cart_query->first();
        }
    }

    public function getTotal(){
        if (!$this->data) { return 0; }
        return  ($this->data->cart_total + $this->data->cart_shipping - $this->data->cart_discount);
    }


    public function setPaid() {
        $amount = $this->getTotal();

        $this->data->update(
            [
                'pay_status' => 'PAID',
                'value_paid' => $amount,
                'status'    => 'DONE',
                'date_paid' => date("Y-m-d H:i:s")
            ]
        );

        foreach($this->data->articles as $article){
            $article->update(array('ord_paid' => $article->ord_total));
        }
    }

    public function getCartList($cartsession)
    {
        $cacheKey = 'getCartList';

        $cart = self::getCartfromSession($cartsession);

        return $cart_items = CartArticle::with(['option.article.images' => function($query) {
            $query->where('is_main', 1);
        }])->where('cart_id', $cart->id)->get()->toArray();
    }

    public function create($user_id, $session)
    {
        $cacheKey = 'newCart';

        // build cart session
        $carthash =  time() . self::GenerateHash(50);
        $session->set('omc_cart', $carthash);

        $newcart = \App\Model\Cart::insert(
                array(
                    'date_created'  => date("Y-m-d H:i:s"),
                    'user_id'       => $user_id,
                    'cart_hash'     => $carthash,
                    'status'        => 'NEW'
                )
            );
        return [true, $newcart[0]];

    }

    public static function GenerateHash ($ln = 10)
    {
        $lp = '';
        $salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        while ($i < $ln) {
            $lp = $lp . substr($salt, rand() % 33, 1);
            $i++;
        }
        return $lp;
    }

    public function has_item($article_id, $cartsession)
    {
        $cart = self::getCartfromSession($cartsession);

        $cartitem = \App\Model\CartArticle::where('cart_id', '=', $cart->id)
            ->where('article_id', '=', $article_id)
            ->first();

        if($cartitem){
            return [true, $cartitem];
        }else{
            return [false];
        }
    }

    public static function getCartfromSession($cartsession)
    {
        $cart = Cart::where('cart_hash','=',$cartsession)->first();
        return $cart;
    }

    public function getCartModelfromSession($cartsession){
        $cart = Cart::where('cart_hash','=',$cartsession)->first();
        return $cart;
    }

    public function getCartModelfromID($cartid){
        $cart = Cart::where('id','=',$cartid)->first();
        return $cart;
    }

    public function add_item($article_id, $cartsession, $item_quant)
    {
        $cart = self::getCartfromSession($cartsession);

        $newcartitem = \App\Model\CartArticle::insert(
                array(
                    'cart_id'       => $cart->id,
                    'article_id'    => $article_id,
                    'quant'         => $item_quant
                )
            );
    }

    public function update_item($current_id, $item_quant)
    {
        $cartupdate = \App\Model\CartArticle::where('item_id', $current_id)
                ->update(array(
                    'quant' => $item_quant
                )
            );
    }

    public function remove_item($item_id)
    {
        $is_last = self::is_last_item($item_id);


        $cartremove = \App\Model\CartArticle::where('item_id', $item_id)
                ->delete();
        return [true, $is_last];
    }

    public function is_last_item($item_id)
    {
        $cart_id = self::getCartIdfromItem($item_id);
        $cart_items = \App\Model\CartArticle::where('cart_id', '=', $cart_id)
                ->count();
        if($cart_items == 1){
            return true;
        }else{
            return false;
        }
    }

    public function getCartIdfromItem($item_id)
    {
        $cart = \App\Model\CartArticle::where('item_id', '=', $item_id)
                ->get();

        return $cart[0]->cart_id;
    }

    public function checkPromo($promo, $user, $amount)
    {
        $is_valid = \App\Model\Promocode::where('code', '=', $promo)
            ->where('locale', \App\Service\Utils::getLocale())
            ->first();

        if (!$is_valid) {
            return [false, $is_valid];
        }

        $currentdate = date("Y-m-d H:i:s");
        $status = $is_valid->status;
        $start = $is_valid->start_date;
        $end = $is_valid->end_date;
        $userid = $is_valid->user_id;
        $buy = $is_valid->min_buy;
        $userlimit = $is_valid->user_limit;

        if($status == 'ACTIVE' && $currentdate < $end && $currentdate > $start){
            if(($userid == '0' || $userid == $user) && ($buy == '0' || $buy <= $amount )){
                if($user && $userlimit != '0'){
                    $checkuser = self::checkUserLimit($promo, $user);
                    if($userlimit == $checkuser){
                        return [false, $is_valid];
                    }
                }   
                return [true, $is_valid];
            }else{
                return [false, $is_valid];
            }  
        }else{
            return [false, $is_valid];
        }

    }

    public function checkUserLimit($promo, $user)
    {
        $uses = \App\Model\PromoUse::where('promocode', $promo)
            ->where('user_id', $user)
            ->count();

        return $uses;
    }

    public function calcTotals($cart_id)
    {
        $cartitems = \App\Model\CartArticle::with('option')
            ->where('cart_articles.cart_id', '=', $cart_id)
            ->get();

        $total = 0;
        foreach($cartitems as $item){
                $total+= ($item->option->price * $item->quant);           
        }

        return $total;
    }

    public function shipCosts($cart_id, $shipping)
    {
        $cartitems = \App\Model\CartArticle::with('option.article')
            ->where('cart_articles.cart_id', '=', $cart_id)
            ->get();

        $total = 0;
        foreach($cartitems as $item){
            if($shipping == 1){
                $total+= ($item->option->article->ship_mainland * $item->quant); 
            }elseif($shipping == 2){
                $total+= ($item->option->article->ship_islands * $item->quant);  
            }

        }

        return $total;
    }

    public function updateCart($cartsession, $userid, $promocode, $shipping, $del_addr_id, $user_inv_addr_id, $paymethod, $ga_visitor_id)
    {
        // Get cart object from session hash
        $cart = self::getCartfromSession($cartsession);
        // Always calc totals just to be sure
        // Calc total of cart without shipping costs
        $total = self::calcTotals($cart->id);
        // Calculate shipping cost
        $shipcost = self::shipCosts($cart->id, $shipping);
        $ord_total = $total + $shipcost;

        if($promocode){
            $promo = self::checkPromo($promocode, $userid, $ord_total);
            $promo_type = $promo[1]->type;
            $promo_val = $promo[1]->value;
            if($promo[0]){
                //Mark Promo as Used
                $used = self::promoUsed($promo[1]->id, $promocode, $userid, $cart->id);
                if($promo_type == 'VALUE'){
                    if($promo_val > $ord_total){
                        $discount = $ord_total;
                    }else{
                        $discount = $promo_val;
                    }
                }else{
                    $discount_calc = (($ord_total * $promo_val) / 100);
                    $discount = number_format((float)$discount_calc, 2, '.', '');
                }
            }else{
               $discount = '0';
            }
        }else{
            $discount = '0';
        }

        $cartupdate = \App\Model\Cart::where('id', $cart->id)
                ->update(array(
                    'date_modified' => date("Y-m-d H:i:s"),
                    'user_id'       => $userid,
                    'status'        => 'PENDING',
                    'promocode'     => $promocode,
                    'cart_total'    => $total,
                    'cart_discount'     => $discount,
                    'dellivery_addr_id' => $del_addr_id,
                    'invoice_addr_id'   => $user_inv_addr_id,
                    'pay_status'        => 'UNPAID',
                    'pay_type'          => $paymethod,
                    'shipping_option'   => $shipping,
                    'cart_shipping'     => $shipcost,
                    'ga_visitor_id'     => $ga_visitor_id
                )
            );
    }

    public function itemstoOrders($cartsession)
    {
        $cart = self::getCartfromSession($cartsession);
        $cartid = $cart->id;
        $shipping = $cart->shipping_option;
        $discount = $cart->cart_discount;
        $cart_total = $cart->cart_total;
        $cart_shipping = $cart->cart_shipping;

        $cartitems = \App\Model\CartArticle::with('option.article')
            ->where('cart_articles.cart_id', '=', $cartid)
            ->get();

        foreach($cartitems as $item){
            if($shipping == 1){
                $total_shipping = ($item->option->article->ship_mainland * $item->quant);
            }else{
                if ($item->ship_islands < 0) { return false; }
                $total_shipping = ($item->option->article->ship_islands * $item->quant);  
            }
            $item_total = ($item->option->price * $item->quant);
            $partial_discount = (($item_total + $total_shipping) * $discount )/($cart_total + $cart_shipping);
            $order_discount = number_format((float)$partial_discount, 2, '.', '');
            $update = \App\Model\CartArticle::where('item_id', $item->item_id)
                ->update(array(
                    'item_status' => 'ORDER',
                    'ord_unit_cost' => $item->option->price,
                    'ord_tot_cost'  => ($item->option->price * $item->quant),
                    'ord_shipping' => $total_shipping,
                    'ord_discount' => $order_discount,
                    'ord_total'    => ($item_total + $total_shipping - $order_discount)
                )
            );
        }

        return true;
    }

    public function updateStocks($cartsession)
    {
        $cart = self::getCartfromSession($cartsession);

        $cart_items = \App\Model\CartArticle::where('cart_id', $cart->id)
                ->get();

        foreach($cart_items as $item){
            $article_id = $item->article_id;
            $quant = $item->quant;

            $updater = self::consumeStock($article_id, $quant);
        }
    }

    public function consumeStock($article, $quant)
    {
        $item = \App\Model\ArticleOption::where('id', $article)
            ->first();

        $newstock = ($item->stock - $quant);

        $update = \App\Model\ArticleOption::where('id', $article)
            ->update(array(
                'stock' => $newstock
            )
        );
    }

    public static function releaseStock($article, $quant)
    {
        $item = \App\Model\ArticleOption::where('id', $article)
            ->first();

        $newstock = ($item->stock + $quant);

        $update = \App\Model\ArticleOption::where('id', $article)
            ->update(array(
                'stock' => $newstock
            )
        );

    }

    // DEPRECATED now using updatePaymentReference
    public function updatePayData($cartsession, $pay_data)
    {
        $cart = self::getCartfromSession($cartsession);

        $update = \App\Model\Cart::where('id', $cart->id)
                ->update(array(
                    'payment_reference' => $pay_data['reference']
                )
            );
    }

    public function updatePaymentReference($reference) {

        if ($this->data) {
            $this->data->update(['reference' => $reference]);
        }
    }

    public function getCartfromID($cartid)
    {
        $cart = \App\Model\Cart::where('id', $cartid)
            ->get();

        return $cart[0];
    }

    public function promoUsed($promoid, $promocode, $userid, $cartid)
    {
        $used = \App\Model\PromoUse::insert(
                array(
                    'promo_id'  => $promoid,
                    'promocode' => $promocode,
                    'user_id'   => $userid,
                    'cart_id'   => $cartid
                )
            );

        $addused = \App\Model\Promocode::where('id', $promoid)
            ->increment('uses');

        $updater = self::updatePromo($promoid);

    }

    public function updatePromo($promoid)
    {
        $promo = \App\Model\Promocode::where('id', $promoid)
            ->first();

        if($promo->limit_total != '0' && $promo->limit_total == $promo->uses){
            $update = \App\Model\Promocode::where('id', $promoid)
                ->update(array(
                    'status' => 'INACTIVE'
                )
            );
        }
    }

    public function getAbandonedList()
    {
        $timeago = date('Y-m-d H:i:s', strtotime('-1 hour'));

        $abandoned = \App\Model\Cart::where('status', 'NEW')
            ->where('abandonment_notify', '0')
            ->where('date_created', '<', $timeago)
            ->get();

        return $abandoned;
    }


    public static function getCartArticlesInfoFromSession($cartsession){
        $cart = self::getCartfromSession($cartsession);
        return self::getCartArticlesInfoFromCart($cart);;
    }

    public static function getCartArticlesInfoFromCart($cart){
        if (!$cart || !$cart->articles) { return []; }

        $out = array();
        $cartArticles = $cart->articles;

        foreach($cartArticles as $cartArticle){
            $out[] = array(
                'item_id'=> $cartArticle->item_id,
                'name'=> $cartArticle->option->article->title,
                'id'=> $cartArticle->option->id,
                'price'=> $cartArticle->option->price,
                'quantity'=> $cartArticle->quant,
                'ship_mainland'=> $cartArticle->option->article->ship_mainland,
                'ship_islands'=> $cartArticle->option->article->ship_islands
            );
        }

        return $out;
    }

    public function referencesExist()
    {
        if($this->data->payment_reference){
            // If yes return success screen with existing reference
            if($cart->payment_total == $cart_total){
                return true;
            }

            $this->logger->warn('Cart total ammount differs from MB details, regenerating');
        }
        return false;
    }

    public function get() {
         return $this->data->toArray();
    }

    public static function getArticleSum($cart_session)
    {
        $cart = self::getCartfromSession($cart_session);
        if (!$cart) { return 0; }
        return $cart->articles->sum('quant');
    }


}

?>
