<?php

namespace App\Service;

use \App\Model\User;
use \App\Traits\CurrentUserTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Users
{

    use CurrentUserTrait;

    public $logger;
    public $session;
    private $data;

    public function __construct(LoggerInterface $logger, SessionInterface $session) {
        $this->logger = $logger;
        $this->session = $session;
    }

    public function __get($key)
    {
        return $this->data->$key;
    }

    public function __set($key, $value)
    {
        return $this->data->$key = $value;
    }

    public function loadById($id, $with = null)
    {
        if ($id) {
            $user_query = User::where('id', '=', $id);
        if (!empty($with)) {
            $user_query->with($with);
        }
            $this->data = $user_query->first();
        }
    }


    public function migrar_Users_NL()
    {
         $user_newsletters = \App\Model\UserNewsletter::get();

        $users = \App\Model\User::get();

        foreach ($users as $user){
            $users_emails[] = $user->email;
        }

        foreach ($user_newsletters as $user){
            if (!in_array($user->username, $users_emails)) {
                self::create($name = "", $user->username, $password = "");
              }
        }

        return true;
    }



    public function login($username, $password)
    {

        $username = trim($username);
        $password = trim($password);


        $user = User::where([
                        ['state',    '=', 'Active'],
                        ['email',    '=', $username],
                        ['password', '=', md5($password)]
                    ])
                    ->first();

        return $this->forceLogin($user);

    }

    public function existsAndIsActive($email)
    {
        $user = $this->getByEmail($email);
        return !empty($user) && $user->count();
    }

    public function getByID($userid)
    {
        $user = User::find($userid);

        if (empty($user) || !$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function getByEmail($email)
    {
        $user = User::where( [
                    ['state', '=', 'Active'],
                    [ 'email', '=', $email ]
                 ])->first();

        if (empty($user)) {
            return false;
        } else {
            return $user;
        }
    }

    public static function getBySSON($code)
    {
        $user = User::where(
                [
                    ['state', '=', 'Active'],
                    ['sson', '=', $code ]
                ])->first();

        if (empty($user)) {
            return false;
        } else {
            return $user;
        }
    }

    public function create($name, $email, $password)
    {

        $password = trim($password);
        if($password == ''){
            $password = self::GenerateHash(6);
        }
        $sson =  time() . self::GenerateHash(50);

        $newuser = User::create(
                array(
                    'email'         => $email,
                    'password'      => md5($password),
                    'name'          => $name,
                    'date_created'  => date("Y-m-d H:i:s"),
                    'state'         => 'Active',
                    'rgpd'          => date("Y-m-d H:i:s"),
                    'sson'          => sha1($sson)
                )
            );

        $newaddress = \App\Model\Address::insert(
                array(
                    'user_id' => $newuser->id,
                    'addr_name' => $newuser->name,
                    'is_main' => 'yes'
                )
            );

        //Esta função está relacionada com
        $newbilling = \App\Model\Address::insert(
                array(
                    'user_id' => $newuser->id,
                    'addr_name' => $newuser->name,
                    'is_main' => 'no'
                )
            );

        /*
        $newprefs = \App\Model\Address::insert(
                array(
                    'user_id' => $newuser->id
                )
            );
        */

        $newprefs = \App\Model\UserPref::insert(
                array(
                    'user_id' => $newuser->id,
                )
            );

        return [
            'status' => true,
            'sson_hash' => sha1($sson),
            'user_id' => $newuser->id,
            'password' => $password
            ];
    }

    public static function GenerateHash ($ln = 10)
    {
        $lp = '';
        $salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        while ($i < $ln) {
            $lp = $lp . substr($salt, rand() % 33, 1);
            $i++;
        }
        return $lp;
    }

    public function tryAutologin($code)
    {

        if (empty($code)) { return false; }

        $code = urldecode($code);
        $code = preg_replace('/[^a-z0-9]/', '', $code);

        $user = self::getBySSON($code);
        if (empty($user) || !$user ) {
            return false;
        } else {
            return $this->forceLogin($user);
        }

    }

    public function resetPassword($email)
    {

        $user = $this->getByEmail($email);

        if (empty($user) || !$user) {
            return false;
        }

        $password   =   self::GenerateHash(6);
        $sson       =   time() . self::GenerateHash(50);

        // Updating member password
        // Update logins for user
        $user->password = md5($password);
        $user->sson = sha1($sson);
        $user->save();

        return [$user, $password];
    }

    public function getUserData($userid)
    {
        $user = User::find($userid);
        return $user->toArray();
    }

    public function getUserPrefs($userid)
    {
        $prefs = \App\Model\UserPref::where('user_id', '=', $userid)
            ->first();

        if ($prefs) { return $prefs->toArray(); }
        return false;
    }

    public function getUserAddress($userid = null)
    {
        if (!$userid) { $userid = $this->data->id; }
        $address = \App\Model\Address::where('user_id', '=', $userid)
            ->where('is_main', '=', 'yes')
            ->first();

        if ($address) { return $address->toArray(); }
        return false;
    }

    public function getUserBilling($userid)
    {
        $address = \App\Model\Address::where('user_id', '=', $userid)
            ->where('is_main', '=', 'no')
            ->first();

        if ($address) { return $address->toArray(); }
        return false;
    }

    public function getUserCarts($userid)
    {
        $carts = \App\Model\Cart::where('user_id', '=', $userid)
            ->where('status', '<>', 'NEW')
            ->orderBy('date_modified', 'DESC')
            ->get();

        return $carts;
    }

    public function updateDetails($userid, $name, $usermorada, $usermorada2, $usercp1, $usercp2, $userlocal, $userphone, $usernif)
    {
        $user = User::find($userid);
        $user->name = $name;
        $user->save();

        $addressupdate = \App\Model\Address::where('user_id', $userid)
                ->where('is_main', 'yes')
                ->update(array(
                    'addr_name'        => $name,
                    'addr_street'      => $usermorada,
                    'addr_street2'      => $usermorada2,
                    'addr_city'        => $userlocal,
                    'addr_cp1'         => $usercp1,
                    'addr_cp2'         => $usercp2,
                    'addr_country'     => 'Portugal',
                    'addr_phone'       => $userphone,
                    'addr_nif'         => $usernif
                )
            );

        return [
            'status' => true
            ];

    }

    public function updateBilling($userid, $usernome, $usermorada, $usermorada2, $usercp1, $usercp2, $userlocal, $userphone, $usernif)
    {

        $addressupdate = \App\Model\Address::where('user_id', $userid)
                ->where('is_main', 'no')
                ->update(array(
                    'addr_name'        => $usernome,
                    'addr_street'      => $usermorada,
                    'addr_street2'      => $usermorada2,
                    'addr_city'        => $userlocal,
                    'addr_cp1'         => $usercp1,
                    'addr_cp2'         => $usercp2,
                    'addr_country'     => 'Portugal',
                    'addr_phone'       => $userphone,
                    'addr_nif'         => $usernif
                )
            );

        return [
            'status' => true
            ];

    }

    public function updatePass($userid, $newpass)
    {

        $user = User::find($userid);

        $sson = time() . self::GenerateHash(50);

        $user->password = md5($newpass);
        $user->sson     = sha1($sson);
        $user->save();

        return [
            'status' => true
            ];

    }

    public function updateNews($userid, $news)
    {
        $newsupdate = \App\Model\UserPref::where('user_id', $userid)
                ->update(array(
                    'newsletter' => $news
                )
            );

        return [
            'status' => true
            ];
    }

    public function updateSMS($userid, $sms)
    {
        $smsupdate = \App\Model\UserPref::where('user_id', $userid)
                ->update(array(
                    'sms' => $sms
                )
            );

        return [
            'status' => true
            ];
    }

    public function updatePartners($userid, $partners)
    {
        $partnersupdate = \App\Model\UserPref::where('user_id', $userid)
                ->update(array(
                    'partners' => $partners
                )
            );

        return [
            'status' => true
            ];
    }

    public function register_from_facebook($userid, $facebookId)
    {
        $newfacebookuser = \App\Model\UsersFacebook::insert(
                array(
                    'user_id' => $userid,
                    'facebook_id' => $facebookId
                )
            );

    }

    public function getUserAddrId($userid, $main_address)
    {
        $address = \App\Model\Address::where('user_id', '=', $userid)
            ->where('is_main', '=', $main_address)
            ->first();

        return $address->id;
    }

    public function getInform($url) {
        $ch = curl_init();
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResp = curl_exec($ch);
        $curlErro = curl_error($ch);

        curl_close($ch);

        if($curlResp !== false) {
            $jResp = json_decode($curlResp);
            if(is_array($jResp)) {
                return array(true, $jResp);
            }
        }

        return array(false);
}

    public function updateRGPD($userid)
    {
        $update = \App\Model\User::where('id', "=", $userid)
                ->update(array(
                    'rgpd' => date('Y-m-d H:i:s')
                )
            );

        return true;

    }

    public function forgetUser($userid)
    {
        $user = \App\Model\User::where('id', '=', $userid)
            ->first();


        $removedUser = \App\Model\UsersRemoved::insert(
                array(
                      'user_id'  => $userid,
                      'email' => $user->email,
                     'password' => $user->password,
                      'name' => $user->name,
                      'gender' => $user->gender,
                      'birth' => $user->birth,
                      'phone' => $user->phone,
                      'date_created' => $user->date_created,
                      'province' => $user->province,
                      'user_prefs' => $user->user_prefs,
                      'state' => $user->state,
                      'last_login' => $user->last_login,
                      'sson' => $user->sson,
                      'rgpd' => $user->rgpd
                )
            );


        if($removedUser){
            $update = \App\Model\User::where('id', "=", $userid)
                ->update(array(
                      'email' =>  'esquecido',
                     'password' =>  '',
                      'name' =>  '',
                      'gender' =>  '',
                      'birth' =>  date('Y-m-d H:i:s'),
                      'phone' =>  "fbshbv",
                      'date_created' =>  date('Y-m-d H:i:s'),
                      'province' =>   "jdvhsjfddv",
                      'user_prefs' =>   '',
                      'state' =>  '',
                      'last_login' =>   date('Y-m-d H:i:s'),
                      'sson' =>  '',
                      'rgpd' =>  date('Y-m-d H:i:s')
                )
            );
            return true;
        }

        return false;


    }




}

?>
