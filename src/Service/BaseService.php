<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

abstract class BaseService
{
    protected $logger;
    protected $request;
    protected $session;

    public function __construct(LoggerInterface $logger, RequestStack $request_stack, SessionInterface $session)
    {
        $this->logger = $logger;
        $this->request = $request_stack->getCurrentRequest();
        $this->session = $session;
    }
}
