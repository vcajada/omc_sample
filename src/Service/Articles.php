<?php

namespace App\Service;

use \App\Model\Article;
use \App\Service\Characteristics;
use \App\Service\BaseService;

class Articles extends BaseService
{

    private $characteristics;

    private static $budgets = array(
        1 => 350,
        2 => 500,
        3 => 750,
        4 => 1000,
        5 => 1500,
        6 => ''
    );

    public static $widths = array(
        '67' => 'icons_largura80.svg.twig',
        '75' => 'icons_largura80.svg.twig',
        '80' => 'icons_largura80.svg.twig',
        '85' => 'icons_largura80.svg.twig',
        '88' => 'icons_largura80.svg.twig',
        '90' => 'icons_largura80.svg.twig',
        '95' => 'icons_largura80.svg.twig',
        '98' => 'icons_largura80.svg.twig',
        '100' => 'icons_largura100.svg.twig',
        '105' => 'icons_largura100.svg.twig',
        '110' => 'icons_largura135.svg.twig',
        '118' => 'icons_largura135.svg.twig',
        '120' => 'icons_largura135.svg.twig',
        '123' => 'icons_largura135.svg.twig',
        '128' => 'icons_largura135.svg.twig',
        '133' => 'icons_largura135.svg.twig',
        '135' => 'icons_largura135.svg.twig',
        '140' => 'icons_largura135.svg.twig',
        '145' => 'icons_largura135.svg.twig',
        '150' => 'icons_largura150.svg.twig',
        '155' => 'icons_largura150.svg.twig',
        '160' => 'icons_largura150.svg.twig',
        '180' => 'icons_largura180.svg.twig',
        '200' => 'icons_largura180.svg.twig',
    );

    public static $heights = array(
        '180' => 'icons_comp180.svg.twig',
        '183' => 'icons_comp183.svg.twig',
        '185' => 'icons_comp183.svg.twig',
        '190' => 'icons_comp190.svg.twig',
        '195' => 'icons_comp190.svg.twig',
        '200' => 'icons_comp200.svg.twig'
    );

    public static $types = [
        'hr' => ['label' => 'Alta resiliência'],
        'latex' => ['label' => 'Látex'],
        'molas' => ['label' => 'Molas'],
        'molas_ensacadas' => ['label' => 'Molas Ensacadas'],
        'molas_bonnel' => ['label' => 'Molas Bonnel'],
        'visco_elastico' => ['label' => 'Viscoelástico'],
    ];

    public function setCharacteristics(Characteristics $characteristics)
    {
        $this->characteristics = $characteristics;
    }

    // TODO - Determine these from the DB
    public static function getWidths()
    {
        return self::$widths;
    }

    public static function getHeights()
    {
        return self::$heights;
    }

    public static function getFirmnesses($translator)
    {
        return [
            '1' => array( 'label' => $translator->trans('Pouco Firme'), 'image' => 'icons_pouco_firme.svg.twig'),
            '2' => array( 'label' => $translator->trans('Médio'), 'image' => 'icons_medio_firme.svg.twig'),
            '3' => array( 'label' => $translator->trans('Firme'), 'image' => 'icons_firme.svg.twig'),
            '4' => array( 'label' => $translator->trans('Muito Firme'), 'image' => 'icons_muito_firme.svg.twig'),
        ];
    }

    public static function getBudgets()
    {
        return self::$budgets;
    }

    public static function getTypes()
    {
        return self::$types;
    }

    public function getRelatedProducts($mattress_type, $mattress_height, $product_id) {

        $products = Article::with('images')
            ->with(['options' => function($q) {$q->where('stock', '>', 0);}])
            ->where([
                [ 'active', '=', '1' ],
                [ 'mattress_type', '=', $mattress_type ],
                [ 'level', '=', $mattress_height ],
                [ 'id', '!=', $product_id ]
            ])
            ->whereHas('options')
            ->orderBy('id', 'desc')->get();

        $products = $products->toArray();

        $result_pretty = array();
        foreach ($products as $product) {
            $result_pretty[] = $this->prettifyProduct($product);
        }
        return $result_pretty;
    }

    public function getHomepagePromos()
    {

        $products = Article::where(
                [
                    [ 'active', '=', '1'],
                    ['is_promo', '=', '1' ]
                ]
            )
            ->whereHas('options')
            ->with('images')
            ->with(['options' => function($q) {$q->where('stock', '>', 0);}])
            ->limit(3)
            ->orderBy('id', 'desc')
            ->get();

        $products = $products->toArray();

        $result_pretty = array();
        foreach ($products as $product) {
            $result_pretty[] = $this->prettifyProduct($product);
        }
        return $result_pretty;
    }

    public function getHomepageTops()
    {
        $products = Article::where(
                [
                    [ 'active', '=', '1'],
                    ['is_promo', '=', '1' ]
                ]
            )
            ->whereHas('options')
            ->with('images')->with(['options' => function($q) {$q->where('stock', '>', 0);}])->with('translations')->with('options.translations')->limit(12)->orderBy('is_top_seller', 'desc')->orderBy('id', 'desc')->get();

        $products = $products->toArray();

        $result_pretty = array();
        foreach ($products as $product) {
            $result_pretty[] = $this->prettifyProduct($product);
        }
        return $result_pretty;
    }

    public function getStock($product_id = false)
    {
        $query = \App\Model\ArticleOption::where('stock', '>', '0');

        if ($product_id) {
            $query->where('article_id', '=', 4);
        }

        $article = $query->orderBy('height','ASC')
                         ->orderBy('width','ASC')
                         ->get();

        return $article->toArray();
    }

    public function getProduct($url_title)
    {
        $url_title = str_replace("_"," ",$url_title);

        $article = Article::where(
                [   [ 'active', '=', '1'],
                    [ 'url_title', '=', $url_title]
                    ]
                )->with(['images' => function($query) {
                    $query->orderBy('image_url', 'ASC');
                }])->with(['options' => function($q) {$q->where('stock', '>', 0);}])->whereHas('options')->get();

        return $article;
    }

    public function getBaseSearch ($height=null, $width=null, $firmness=null, $budget_type=null, $deep=null, $brand=null, $type=null) {
        $articles = Article::where('active','=',1)
                        ->with('images')->with(['options' => function($q) {$q->where('stock', '>', 0);}],'translations');

        if($height || $width){
            $articles = $this->addSizeFilter($articles,$height,$width);
        }

        if($firmness){
            $articles = $articles->where('level','=',$firmness);
        }

        if($budget_type != '' && $budget_type != 6)
        {
            $budget = self::$budgets[$budget_type];
            $articles = $this->addPriceFilter($articles,$budget);

        }

        if($deep){
            $articles = $articles->where('deep', '=', $deep);
        }

        if($brand){
            $articles = $articles->where('brand', '=', $brand);
        }

        if($type){
            $articles = $articles->where('mattress_type', '=', $type);
        }
        $articles->whereHas('options')->with('options.translations');


        return $articles;
    }

    public function getPromoResults($height, $width, $firmness, $budget_type, $deep, $brand, $type) {

        $result = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type)
                       ->where('is_promo','=',1)
                       ->orderBy('id', 'desc')
                       ->get();

        $result_pretty = array();
        foreach ($result as $product) {
            $result_pretty[] = $this->prettifyProduct($product);
        }
        return $result_pretty;
    }

    public function getTopResults($height, $width, $firmness, $budget_type, $deep, $brand, $type) {
        $result = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type)
                       ->orderBy('is_top_seller', 'desc')
                       ->orderBy('id', 'desc')
                       ->get();

        $result_pretty = array();
        foreach ($result as $product) {
            $result_pretty[] = $this->prettifyProduct($product);
        }
        return $result_pretty;
    }

    public function getSearchList($height, $width, $firmness, $budget_type=null, $deep=null, $brand=null, $type=null)
    {
        $query = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $result = $query->orderBy('id', 'desc')->get();

        $result = $result->toArray();

        $result_pretty = array();

        //var_dump($result->toArray());die();
        foreach ($result as $product) {
            if (empty($product['options'])) { continue; }
            $result_pretty[] = $this->prettifyProduct($product);
        }

        return $result_pretty;
    }

    public function prettifyProduct($product, $option_id = NULL)
    {
        $product = $this->calcPrices($product, $option_id);
        $product = $this->setMainImage($product);
        $product = $this->setCacheBuster($product);
        if($product['characteristics'] != null) {
            $product['characteristics'] = array_map('trim', explode(',', $product['characteristics']));
        }

        return $product;
    }

    private function setCacheBuster($product) {

        foreach ($product['images'] as $key => $item) {
            $product['images'][$key]['cache_buster'] = strtotime($item['last_modified']);
        }
        if (isset($product['main_image']) && !empty($product['main_image'])) {
            $product['main_image']['cache_buster'] = strtotime($product['last_modified']);
        }
        return $product;
    }

    private function setMainImage($product)
    {
        if (empty($product['images'])) {
            $product['images'] = [
                [
                    'image_url' => '',
                    'is_main' => 1,
                    'last_modified' => '1970-01-01 00:00:00'
                ]
            ];
        }
        foreach ($product['images'] as $key => $image) {
            if ($image['is_main']) {
                $product['main_image'] = $image;
            }
        }

        return $product;
    }

    private function calcPrices($product, $option_id)
    {
        $product['start_price'] = -1;
        foreach($product['options'] as $option) {
            //comentei o if abaixo e condensei-o aqui em cima pois ambos os ifs faziam a mesma coisa
            if (($option_id && $option['id'] == $option_id) || ($option['price'] < $product['start_price'] || $product['start_price'] < 0)) {
                $product['start_price']    = $option['price'];
                $product['original_price'] = $option['original_price'];
                //acrescentei este if porque dava erro de divisão por 0
                //Alguns dos produtos têm o original_price a 0
                if($option['original_price'] != 0) {
                    $product['discount']       = round(($option['price']/ $option['original_price']-1)*100*-1);
                } else {
                    $product['discount'] = 0;
                }

                //return $product;
            }

            /*if ($option['price'] < $product['start_price'] || $product['start_price'] < 0) {
                $product['start_price']    = $option['price'];
                $product['original_price'] = $option['original_price'];
                $product['discount']       = round(($option['price']/ $option['original_price']-1)*100*-1);
            }*/
        }
        return $product;
    }

    public function getSearchDepts($height, $width, $firmness, $budget_type, $deep, $brand, $type)
    {
        $deep = null;
        $articles = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        return $articles->groupBy('deep')->get();
    }

    public function getSearchBrands($height, $width, $firmness, $budget_type, $deep, $brand, $type)
    {
        $brand = null;
        $articles = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        return $articles->groupBy('brand')->get();
    }

    public function getSearchTypes($height, $width, $firmness, $budget_type, $deep, $brand, $type)
    {
        $type = null;
        $articles = $this->getBaseSearch($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        return $articles->groupBy('mattress_type')->get();
    }

    public function getPromoList($size, $firmness)
    {
        list($width,$height) = self::widthAndHeightFromSize($size);
        $articles = $this->getBaseSearch($height,$width,$firmness,null,null,null,null);
        $articles = $articles->where('is_promo','=',1);
        return $articles->get();
    }

    public function getTopList($size, $firmness)
    {
        list($width,$height) = self::widthAndHeightFromSize($size);
        $articles = $this->getBaseSearch($height,$width,$firmness,null,null,null,null);
        return $articles->get();
    }

    private function addSizeFilter($articles,$height,$width){


        if($width && $height){
            $articles = $articles->join('article_options','article_options.article_id','=','articles.id')
                                  ->where('article_options.height','=',$height)
                                  ->where('article_options.width','=',$width)
                                  ->addSelect(
                                    'articles.*',
                                    'article_options.id as option_id',
                                    'article_options.width as option_width',
                                    'article_options.height as option_height',
                                    'article_options.price as option_price',
                                    'article_options.original_price as option_original_price');

            return $articles->with(['options' => function($q) {$q->where('stock', '>', 0);}]);
        }

        $articles = $articles->whereHas('options', function($query) use ($width,$height){
            if($height){
                $query = $query->where('height','=',$height);
            }
            if($width){
                $query = $query->where('width','=',$width);
            }
        });
        return $articles->with(['options' => function($q) {$q->where('stock', '>', 0);}]);
    }

    private function addPriceFilter($articles,$price){

        $articles = $articles->whereHas('options', function($query) use ($price){

            $query = $query->where('price','<=',$price);
        });

        return $articles->with(['options' => function($q) {$q->where('stock', '>', 0);}]);



       /*  return $articles->with(['options' => function($query) use ($price){
            $query->where('price','<=',$price);
        }]); */

    }

    public static function widthAndHeightFromSize($size){
            $sizes = explode('x',$size);
            $width = $sizes[0];
            $height = $sizes[1];

            return array($width,$height);
    }

    public function getCharacteristics($product, $show_only_4_icons = false) {
        $base_cha = $this->characteristics->getBaseCharacteristics();
        $characteristics = $this->characteristics->getCharacteristics();

        /* rubbish - vlopes
        $type = [
            'molas' => 'molas',
            'HR' => 'hr',
            'Molas Ensacadas' => 'molas_ensacadas',
            'Látex' => 'latex',
            'Molas Spa' => 'molas_bonnel',
            'Molas Bonnel' => 'molas_bonnel',
            'Visco Elástico' => 'visco_elastico',
            'Viscoelástico' => 'visco_elastico',
            'viscoelastico' => 'visco_elastico',
            'Viscogel' => 'viscogel',
            'Molas SSW' => 'molas_bonnel'
        ];
        */

        $ch = [
            $base_cha['altura'],
            $base_cha['firmeza_'.$product['level']],
        ];
        if (isset($base_cha[$product['mattress_type']])) {
            $ch[] = $base_cha[$product['mattress_type']];
        }

        if($product['characteristics'] != null) {
            foreach($product['characteristics'] as $cha) {
                $ch[] = $characteristics[$cha];
                if($show_only_4_icons) {
                    break;
                }
            }
        }

        return $ch;
    }

}
