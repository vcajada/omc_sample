<?php
namespace App\Service;

class Cigest
{
    public static function generateExportFile($order_list,$type=null)
    {

        //Generate Spreadsheet
    
        $headers = array(
            'Nome',
            'Morada',
            'Morada2',
            'C.Postal',
            'Localidade',
            'Pais',
            'Telefone',
            'E-mail',
            'NIF',
            'Nome Entrega',
            'Morada Entrega',
            'Morada2 Entrega',
            'C.Postal Entrega',
            'Localidade Entrega',
            'Pais Entrega',
            'Telefone Entrega',
            'Zona',
            'Order ID',
            'Referencia',
            'Descricao1',
            'Descricao2',
            'Código Iva',
            'Grupo Contabilístico',
            'Conta POC',
            'Quantidade',
            'Valor Unit',
            'Portes',
            'Facturado'
        );
    
        $file = tempnam(sys_get_temp_dir(), "Cigest_export");


        $output = fopen($file, "w");

        //BOM
        //fwrite($output, chr(239) . chr(187) . chr(191));

        fputcsv($output, $headers, ';', ' ');
        foreach ($order_list as $order) {
            $rows = array(
                substr(strtoupper(trim($order->name)), 0, 50), //Nome                           0
                substr(strtoupper(trim($order->addr_street)), 0, 50), //Morada
                substr(strtoupper(trim($order->addr_street2)), 0, 50), //Morada2
                substr($order->addr_cp1.'-'.$order->addr_cp2, 0, 10), // Cpostal
                substr($order->addr_city, 0, 40), // Localidade
                'PT', //$order->addr_country, // Pais
                substr($order->addr_phone, 0, 20), //  Telefone
                substr($order->email, 0, 70), // Email
                ($order->addr_nif ? substr($order->addr_nif, 0, 18) : '999999990') ,  // NIF
                substr(strtoupper($order->addr_name), 0, 50), // Nome Entrega
                substr(strtoupper(trim($order->addr_street)), 0, 50), //Morada Entrega          10
                substr(strtoupper(trim($order->addr_street2)), 0, 50), //Morada Entrega 2
                substr($order->addr_cp1.'-'.$order->addr_cp2, 0, 10), // CPostal Entrega
                substr($order->addr_city, 0, 40), // Localidade Entrega
                'PT', //$order->addr_country, // Pais Entrega
                substr($order->addr_phone, 0, 20), // Telefone Entrega
                '', // Zona
                'COLCH'.$order->item_id, // Order ID
                $order->stk, // Referencia
                substr($order->title, 0, 30),  // Desc1
                substr($order->width.'x'.$order->height, 0, 255), // Desc2                      20
                '13', // Codigo IVA
                '102', // Grupo Contabilistico
                '211171', // Conta POC
                $order->quant, // Quant
                self::formatCurrencyValue($order->ord_unit_cost), // Valor Unit
                self::formatCurrencyValue($order->ord_shipping), // Portes
                '0' // Facturado                                                                27
            );

            $rows = array_map(function ($in) {
                return utf8_decode($in);
            }, $rows);
            
            fputcsv($output, $rows, ';', '"');

            // If order has a promocode, output a line for it
            if (bccomp($order->cart_discount, 0) ===1 ){

                // Skip devpromocodes for standard export
                if($type == 'cigest' && strpos($order->promocode,'DEV') === 0) {
                    continue;
                }

                $values_disc = $rows;
                $values_disc[19] = 'DESCONTO';
                $values_disc[20] = 'Promocode: '.$order->promocode;
                $values_disc[24] = -1;
                $values_disc[25] = Cigest::formatCurrencyValue($order->cart_discount);
                $values_disc[26] = Cigest::formatCurrencyValue(0);

                fputcsv($output, $values_disc, ';', '"');
            }
        }
        fclose($output);

        return $file;
    }

    private static function formatCurrencyValue($val)
    {
        return str_replace('.', ',', sprintf('%.2f', $val));
    }
}
