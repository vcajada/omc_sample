<?php

namespace App\Service;

class Promocodes extends BaseService
{

    public function getPromoList($code, $mbrid, $type, $used, $locale)
    {
        $cacheKey = 'getPromoList';

        $promos = \App\Model\Promocode::whereNotNull('code');
        if($code){
            $promos->where('code', $code);
        }
        if($mbrid){
            $promos->where('user_id', $mbrid);
        }
        if($type){
            $promos->where('type', $type);
        }
        if($used){
            if($used == 'yes'){
                $promos->where('uses', '>', '0');
            }else{
                $promos->where('uses', '=', '0');
            }
        }
        if ($locale) {
            $promos->where('locale', $locale);
        }

        $promoquery = $promos->get();

        return $promoquery;
    }

    public function loadPromo($code)
    {
        $promo = \App\Model\Promocode::where('code', $code)
            ->get();

        return $promo;
    }

    public function updatePromo($id, $code, $limit, $usr_limit, $start, $end, $type, $value, $userid, $minbuy, $status, $locale)
    {
        $promo = \App\Model\Promocode::where('id', $id)
                ->update(array(
                    'code'          => $code,
                    'limit_total'   => $limit,
                    'user_limit'    => $usr_limit,
                    'start_date'    => $start,
                    'end_date'      => $end,
                    'type'          => $type,
                    'date_modified' => date("Y-m-d H:i:s"),
                    'user_id'       => $userid,
                    'value'         => $value,
                    'min_buy'       => $minbuy,
                    'status'        => $status,
                    'locale'        => $locale

                )
            );

        return $promo;
    }

    public function createPromo($code, $limit, $usr_limit, $start, $end, $type, $value, $userid, $minbuy, $status, $locale)
    {
        $promo = \App\Model\Promocode::insert(
                array(
                    'code'          => $code,
                    'limit_total'   => $limit ? $limit : 0,
                    'user_limit'    => $usr_limit ? $usr_limit : 0,
                    'start_date'    => $start,
                    'end_date'      => $end,
                    'type'          => $type,
                    'date_created'  => date("Y-m-d H:i:s"),
                    'date_modified' => date("Y-m-d H:i:s"),
                    'user_id'       => $userid,
                    'value'         => $value,
                    'min_buy'       => $minbuy,
                    'status'        => $status,
                    'locale'        => $locale,
                    'uses'          => 0,
                    'root_id'       => 0
                )
            );

        return $promo;
    }

}

?>
