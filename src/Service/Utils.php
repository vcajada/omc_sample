<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\Session;

class Utils {

    public static function emptyObj($obj) {
        foreach ($obj as $item) {
            return false;
        }
        return true;
    }
    /**
     * Get locale from session if in BO from request locale if not in bo
     */
    public static function getLocale()
    {
        $session = new Session();
        $request = (isset($GLOBALS['request']) ? $GLOBALS['request'] : null); // Temporary workaround for command line
        $locale = isset($GLOBALS['in_bo']) && $GLOBALS['in_bo'] && $session->has('locale') ?
            $session->get('locale') :
            $request->getLocale();
        return $locale;
    }
}

?>
