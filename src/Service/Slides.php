<?php
namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

class Slides
{

    public function getSlideShow()
    {
            $slide = \App\Model\Slide::orderby('orderby','ASC')
                ->where('locale', \App\Service\Utils::getLocale())
                ->get();

        return $slide;
    }

    public function getSlideDesktop()
    {
            $slide = \App\Model\Slide::where('tipo','desktop')
                ->where('locale', \App\Service\Utils::getLocale())
                ->first();

        return $slide;
    }

    public function getSlideMobile()
    {
            $slide = \App\Model\Slide::where('tipo','mobile')
                ->where('locale', \App\Service\Utils::getLocale())
                ->first();

        return $slide;
    }

       public function removeSlide($id)
    {
            $slide = \App\Model\Slide::delete($id);

    }

    public function addSlide($filename, $link, $type, $locale){

        $fs = new Filesystem();
        $path = '/images/Slides/';
        $img_path = $_SERVER['DOCUMENT_ROOT'].$path;

        /*$fs->mkdir($img_path, 0700);*/

            $file_ext = $filename->getClientOriginalExtension();
            $file = 'slide'.date("Y-m-d::h:i:sa").'.'.$file_ext;
            $filename->move($img_path,$file);

            $img_url = $path.$file;
            $set_image = self::setImage($img_url, $link, $type, $locale);

    }



     public function updateSlide($args){

        foreach($args['id'] as $key => $item){

           $opt_args = [
                'id' => $args['id'][$key],
                'orderby' => $args['order'][$key]
            ];

            $set_option = self::updateOrderSlide($opt_args);
        }



    }

    public function updateOrderSlide($args){

        $update = \App\Model\Slide::where('id', $args['id'])
                ->update(
                    array(
                    'orderby' => $args['orderby']
                )
            );



    }

    public function setImage($img_url, $link, $type, $locale)
    {
        $images = \App\Model\Slide::insert(
                array(

                    'image_url'     => $img_url,
                    'link' => $link,
                    'tipo' => $type,
                    'locale' => $locale
                )
            );
    }

}

?>
