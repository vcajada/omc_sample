<?php

namespace App\Service;

use Symfony\Component\Translation\TranslatorInterface;

class EmailTree
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function get()
    {
        return [
            'default_from' => $this->translator->trans('geral@omeucolchao.pt'),
            'templates' => [
                'user_register' => [
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('Bem vindo {{ name }}'),
                    'template' => $this->translator->trans('emails/user_register.twig'),
                    'title' => $this->translator->trans('Bem vindo.'),
                ],
                'recover_pass' => [
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('O meu colchão - Recuperação de password'),
                    'template' => $this->translator->trans('emails/recover_pass.twig'),
                    'title' => $this->translator->trans('Recuperação de password'),
                ],
                'mb_payment_details' => [
                    ## Params: reference, entity, amount, cart
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('O meu colchão - Dados de pagamento Multibanco'),
                    'template' => $this->translator->trans('emails/mb_payment_details.twig'),
                    'title' => $this->translator->trans('Dados de pagamento Multibanco'),
                ],
                'payment_received' => [
                    ## Params: cart
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('O meu colchão - Confirmação de pagamento recebido'),
                    'template' => $this->translator->trans('emails/payment_received.twig'),
                    'title' => $this->translator->trans('Confirmação de pagamento recebido'),
                ],
                'cart_reminder' => [
                    ## Params: cart (a CartModel) , username - nome do utilizador
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('Não se esqueceu de nada?'),
                    'template' => $this->translator->trans('emails/cart_reminder.twig'),
                    'title' => $this->translator->trans('Deixou o seu carrinho para trás...'),
                ],
                'mb_reminder' => [
                    ## Params: cart (a CartModel) , username - nome do utilizador, paydata - dados de pagamento
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('A sua encomenda está prestes a expirar!'),
                    'template' => $this->translator->trans('emails/mb_reminder.twig'),
                ],
                'order_expired' => [
                    ## Params: cart (a CartModel) , username - nome do utilizador
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('Lamentamos mas o prazo de pagamento da sua encomenda terminou.'),
                    'template' => $this->translator->trans('emails/order_expired.twig'),
                ],
                'call_me' => [
                    ## Params: to, from_phone, from_email, message
                    'from' => [
                        'geral@omeucolchao.pt' => $this->translator->trans('O meu colchão'),
                    ],
                    'subject' => $this->translator->trans('Pedido de Contacto'),
                    'template' => $this->translator->trans('emails/call_me.twig'),
                ]
            ]
        ];
    }
}
