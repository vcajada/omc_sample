<?php

namespace App\Service;

use \App\Model\Article;
use \App\Model\ArticleOption;
use \App\Model\ArticleImage;
use \App\Characteristics;
use \App\Service\Utils;

use Symfony\Component\Filesystem\Filesystem;

class Products extends BaseService
{

    private static $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-',
        '/[’‘‹›‚`]/u'   =>   '',
        '/[“”«»„]/u'    =>   '',
        '/ /'           =>   '',
        '/®/'           =>   ''
    );

    private static $response_success = [
        'ok' => true,
        'response' => ''
    ];

    public function setFilesystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function getLast()
    {
        $products = \App\Model\ArticleOption::with(['article.images' => function($query) {
            $query->where('is_main', 1);
        }])
            ->orderBY('article_id', 'DESC')
            ->limit('3')
            ->get();

        return $products;
    }

    public function getProductList($parent, $prodid, $stk, $title)
    {
        $cacheKey = 'getProductList';

        $products = Article::with(['options' => function($query) {
            $query->groupBy('article_id')->orderBy('id', 'desc');
        }]);

        if($parent){
            $products->where('articles.id', "=", $parent);
        }
        if($prodid){
            $products->where('articles.id',"=", $prodid);
        }
        if($stk){
            $products->where('articles.main_stk',"=", $stk);
        }
        if($title){
            $products->where('articles.title',"=", $title);
        }

        return $products->get();
    }

    public function loadProductId($article_id)
    {
        $product = \App\Model\Article::find($article_id);

        if ($product) {
            $product = $product->toArray();
        }
        return $product;
    }

    public function loadProductStk($main_stk)
    {
        $product = Article::where('main_stk',$main_stk)->first();

        if ($product) {
            $product = $product->toArray();
        }
        return $product;
    }

    public function loadImage($img_id)
    {
        $image = \App\Model\ArticleImage::where('img_id', $img_id)
            ->get();

        return $image;
    }

    public function loadProductOptions($parent)
    {
        $options = \App\Model\ArticleOption::where('article_id', $parent)
            ->get();

        if ($options) {
            $options = $options->toArray();
        }
        return $options;
    }

    public function loadProductImages($parent)
    {
        $images = \App\Model\ArticleImage::where('article_id', $parent)
            ->get();

        foreach ($images as $key => $item) {
            $images[$key]->cache_buster = strtotime($item->last_modified);
        }

        return $images;
    }

    public function loadOption($stk, $main_stk = null)
    {
        $option = ArticleOption::where('stk','=',$stk);
        if($main_stk){
            $option = $option->whereHas('article',function($q) use ($main_stk){return $q->where('main_stk','=',$main_stk);});
        }
        $option = $option->first();

        if ($option) {
            $option = $option->toArray();
        }
        return $option;
    }

    public function createArticle(array $args)
    {

        $article = \App\Model\Article::create(
                array(
                    'main_stk'        => $args['main_stk'],
                    'brand'           => $args['brand'],
                    'title'           => $args['title'],
                    'url_title'       => preg_replace(array_keys(self::$utf8), array_values(self::$utf8), $args['title']),
                    'deep'            => $args['deep'],
                    'level'           => $args['level'],
                    'ship_mainland'   => $args['shipping_mainland'],
                    'ship_islands'    => $args['shipping_islands'],
                    'expedition_time' => $args['expedition_time'],
                    'is_promo'        => $args['is_promo'],
                    'is_ext'          => $args['is_ext'],
                    'article_type'    => $args['article_type'],
                    'active'          => $args['active'],
                    'mattress_type'   => $args['mattress_type'],
                    'description'     => $args['description'],
                    'conditions'      => $args['conditions'],
                )
            );
        return DB::getPdo()->lastInsertId();
    }

    public function saveArticle($args)
    {

        $article = \App\Model\Article::where('main_stk', $args['main_stk'])->first();
        $article->fill(array(
                'main_stk'        => $args['main_stk'],
                'brand'           => $args['brand'],
                'title'           => $args['title'],
                'url_title'       => $args['url_title'],
                'deep'            => $args['deep'],
                'level'           => $args['level'],
                'article_type'    => $args['article_type'],
                'ship_mainland'   => $args['shipping_mainland'],
                'ship_islands'    => $args['shipping_islands'],
                'expedition_time' => $args['expedition_time'],
                'is_promo'        => $args['is_promo'],
                'is_ext'          => $args['is_ext'],
                'active'          => $args['active'],
                'mattress_type'   => $args['mattress_type'],
                'characteristics' => $args['characteristics'],
                'description'     => $args['description'],
                'conditions'      => $args['conditions'],
            )
        )->save();
    }

    public function saveImage($img_id, $stk, $img_url, $is_main)
    {
        $url = '/images/articles/'.$stk.'/'.$img_url;

        $images = \App\Model\ArticleImage::where('img_id', $img_id)
                ->update(array(
                    'article_id'   => self::getIDfromSTK($stk),
                    'image_url'    => $url,
                    'is_main'      => $is_main,
                    'last_modified'=> date('Y-m-d H:i:s')
                )
            );
    }

    public function createImage($img_id, $stk, $img_url, $is_main)
    {
        $url = '/images/articles/'.$stk.'/'.$img_url;

        $images = \App\Model\ArticleImage::insert(
                array(
                    'img_id'        => $img_id,
                    'article_id'   => self::getIDfromSTK($stk),
                    'image_url'    => $url,
                    'is_main'      => $is_main
                )
            );
    }

    public function getIDfromSTK($stk)
    {
        $article = \App\Model\Article::where('main_stk', $stk)
            ->first();

        return $article->id;
    }

    public function importCSV($p_csv)
    {
        $this->logger->info("Starting CSV import");

        $utf8_file_data = file_get_contents($p_csv);
        if(!mb_detect_encoding($utf8_file_data, 'UTF-8', true)) {
                $utf8_file_data = utf8_encode($file_data);
        }

        $path = '/uploads/';
        $csv_path = $_SERVER['DOCUMENT_ROOT'].$path;

        $new_csv = $csv_path.'upload.csv';
        file_put_contents($new_csv , $utf8_file_data );

        //open uploaded csv file with read only mode
        $csvFile = fopen($new_csv, 'r');

        //skip first line
        fgetcsv($csvFile);

        $last_main_stk = '';

        //parse data from csv file line by line
        while(($line = fgetcsv($csvFile, 10000, ";")) !== FALSE){

            // Give names to things so that you can go home and take your brain with you
            $args = [
                'main_stk'          => $line[0],
                'stk'               => $line[1],
                'brand'             => $line[2],
                'mattress_type'     => $line[3],
                'title'             => $line[4],
                'original_price'    => floatval($line[5]),
                'deep'              => $line[6],
                'level'             => $line[7],
                'article_type'      => $line[8],
                'shipping_mainland' => $line[9],
                'shipping_islands'  => $line[10],
                'expedition_time'   => $line[11],
                'is_promo'          => $line[12],
                'is_top_seller'     => $line[13],
                'is_ext'            => $line[14],
                'active'            => $line[15],
                'width'             => $line[16],
                'height'            => $line[17],
                'price'             => floatval($line[18]),
                'cost'              => floatval($line[19]),
                'stock'             => $line[20],
                'description'       => $line[21],
                'conditions'        => $line[22],
                'characteristics'   => ($line[23] ? $line[23] : ''),
                'to_delete'         => isset($line[24]) ? $line[24] : 0
            ];

            $base_chas = (new \App\Service\Characteristics())->getBaseCharacteristics();
            if(!array_key_exists($args['mattress_type'], $base_chas)) {
                return [
                    'ok' => false,
                    'response' => 'Oo colchão com o tipo "'.$args['mattress_type'].'" não foi inserido. Tipo de colchão inválido.'.$args['stk'].$args['main_stk'].$args['title']
                ];
            }

            $chas = array_map('trim', explode(',', $args['characteristics']));
            $characteristics = (new \App\Service\Characteristics())->getCharacteristics();

            if($chas[0] != '') {
                foreach($chas as $cha) {
                    if(!array_key_exists($cha, $characteristics)) {
                        return [
                            'ok' => false,
                            'response' => 'O colchão com a característica "'.$cha.'" não foi inserido. Característica inválida.'
                        ];
                    }
                }
            }

            //if (!isset($args['stk']) || empty($args['stk']) || $args['stk'] === 0) {
            if (isset($args['main_stk']) && $args['main_stk'] !== $last_main_stk){
                $last_main_stk = $args['main_stk'];

                $this->logger->info("Processing line as Article: " . var_export($args,true) );
                $handle_article_result = self::handleArticle($args);
                if (!$handle_article_result['ok']) {
                    return $handle_article_result;
                }
            }

            $this->logger->info("Processing line as Option: " . var_export($args,true) );
            $handle_option_result = self::handleOption($args);

            if (!$handle_option_result['ok']) {
                return $handle_option_result;
            }
        }
        return [
            'ok' => true, 
            'response' => 'Csv importado com sucesso!'
        ];
        //close opened csv file
        fclose($csvFile);

        return true;
    }

    private function handleArticle($args) {
        $this->logger->info("[handleArticle] Processing article " . $args['main_stk']);

        //check whether article already exists in database with same id
        $article = self::loadProductStk($args['main_stk']);
        if($article){
            $args['article_id'] = $article['id'];
            $args['url_title'] = $this->session->get('locale') === 'pt' ? preg_replace(array_keys(self::$utf8), array_values(self::$utf8), $args['title']) : $article['url_title'];
            $this->logger->info("[handleArticle] updating article " . $args['main_stk']);

            //update article data
            self::saveArticle($args);
            return self::$response_success;
        }

        $this->logger->info("[handleArticle] creating new article " . $args['main_stk']);

        // if locale is not default abort and inform that it shoud be created in default first.
        if (Utils::getLocale() !== $default_locale  = $this->request->getDefaultLocale()) {
            return [
                'ok' => false,
                'response' => 'O colchão com o stk "'.$args['main_stk'].'" não foi inserido. Colchão não existente em tradução default: "' . $default_locale . '".'
            ];
        }

        //insert article data into database
        $article_id = self::createArticle($args);
        $args['article_id'] = $article_id;

        $this->logger->info("[handleArticle] new article ID is " . $args['article_id']);

        $args;

        return self::$response_success;
    }

    private function handleOption($args) {
        $this->logger->info("[handleOption] Processing option " . json_encode($args));

        //delete option if field to delete is true
        if ($args['to_delete'] == 1) {
            //delete option
            self::deleteOption($args['stk']);

            return self::$response_success;
        }
        //check whether article_option already exists in database with same id
        $option = self::loadOption($args['stk']);
        if($option){
            $this->logger->info("[handleOption] Option loaded with success");
            //update article_option data
            $args['option_id'] = $option['id'];
            self::updateOption($args);

            return self::$response_success;
        }

        // if locale is not default abort and inform that it shoud be created in default first.
        $default_locale = $this->request->getDefaultLocale();
        if (Utils::getLocale() !== $default_locale) {
            return [
                'ok' => false,
                'response' => 'A opção com o stk "'.$args['stk'].'" não foi inserida. Opção não existente em tradução default: "' . $default_locale . '".'
            ];
        }

        // New option. Load the article to determine its ID
        $article = self::loadProductStk($args['main_stk']);
        $args['article_id'] = $article['id'];

        //insert article_option data into database
        self::setOption($args);

        return self::$response_success;
    }

    public function process_image_dir($stk, $path){

        $this->logger->info("Processing $path for STK $stk");

        $article = Article::where('main_stk','=',$stk)->first();
        if(!$article){
            $this->logger->info("Couldn't find article for {$stk}");
            return;
        }
        $article_id = $article->id;

        ArticleImage::where('article_id','=',$article_id)->delete();


        if ($dir = scandir($path, 1)) {
            foreach ($dir as $filename) {

                if (strpos($filename,".") === 0) { continue; }; // Skip . and ..
                $is_main = (strpos($filename,"img_main") === 0 ? 1 : 0 ); // Determine if this is the main image

                $this->logger->info("Saving image $filename with is_main:$is_main for STK $stk");

                $image = new ArticleImage();
                $image->image_url = '/images/articles/'.$stk.'/'.$filename;
                $image->article_id = $article->id;
                $image->is_main = $is_main;

                $image->save();
            }
        }

        return true;
    }

    private function move_files_recursive($old_path, $new_path) {
        if (!is_dir($new_path)) {
            mkdir($new_path);
        }

        $this->logger->info('copying '.$old_path.' to '.$new_path);
        if ($dir = opendir($old_path)) {
            while (($filename = readdir($dir)) !== false) {
                if (strpos($filename,".") === 0) { continue; }; // Skip . and ..

                $this->logger->info('copying '.$filename.' from '.$old_path.' to '.$new_path);

                if (!copy($old_path.'/'.$filename, $new_path.'/'.$filename)) {
                    $this->logger->info('unable to copy '.$old_path.'/'.$filename);
                }

                $this->logger->info('unlinking '.$old_path.'/'.$filename);
                if (!unlink($old_path.'/'.$filename)) {
                    $this->logger->info('unable to unlink '.$old_path.'/'.$filename);
                }
            }
        }
        $this->logger->info('done move_files_recursive');
    }

    public function importZIP($p_zip)
    {
        $this->logger->info("Starting ZIP import");

        $file_data = file_get_contents($p_zip);

        $path = '/uploads/';
        $zip_path = $_SERVER['DOCUMENT_ROOT'].$path;

        $new_zip = $zip_path.'upload.zip';
        file_put_contents($new_zip , $file_data );

        $tmp_dir_name = 'tmp_imgs_'.time().'/';
        $tmp_csv_path = '/tmp/'.$tmp_dir_name;
        mkdir($tmp_csv_path);
        $csv_path = $_SERVER['DOCUMENT_ROOT']. '/images/articles/';

        $zip = new \ZipArchive;
        if ($zip->open($new_zip) === TRUE) {
            $zip->extractTo($tmp_csv_path);
            $zip->close();
        }


        $this->logger->info("Inspecting $tmp_csv_path");
        if ($dir = opendir($tmp_csv_path)) {
            while (($filename = readdir($dir)) !== false) {
                $this->logger->info("Found $filename");

                //Only process STK directories
                if(strpos($filename,"STK") !== 0) { continue; }

                $this->logger->info("Going to process $filename");

                if(is_dir($tmp_csv_path.$filename)){
                    $this->process_image_dir($filename, $tmp_csv_path.$filename.'/');
                    $this->move_files_recursive($tmp_csv_path.$filename, $csv_path.$filename);
                    #rmdir($tmp_csv_path.$filename);
                }
            }
            #rmdir($tmp_csv_path);
        }


        return true;
    }

    public function getExport()
    {
        $list = \App\Model\ArticleOption::join('articles', 'article_options.article_id', '=', 'articles.id')
            ->join('article_fields', 'article_options.article_id', '=', 'article_fields.id')
            ->select('articles.*', 'article_fields.*', 'article_options.*')
            ->get();

        return $list;
    }

    public function createProduct($p_type, $main_stk, $p_mattress, $p_brand, $p_title, $p_mainland, $p_islands, $p_expedition_time, $p_promo, $p_top_seller, $p_is_ext, $low_price, $original_price, $discount, $p_deep, $p_level, $p_status, $p_description, $p_conditions, $p_details, $p_stk, $p_height, $p_width, $p_cost, $p_price, $p_stock, $p_img_main, $p_img_2, $p_img_3, $p_img_4, $p_img_5, $p_img_6, $p_original_price)
    {

        $data = array(
            'main_stk'      => $main_stk,
            'brand'         => $p_brand,
            'title'         => $p_title,
            'url_title'     => preg_replace(array_keys(self::$utf8), array_values(self::$utf8), $p_title),
            'deep'          => $p_deep,
            'level'         => $p_level,
            'ship_mainland' => $p_mainland,
            'ship_islands'  => $p_islands,
            'expedition_time' => $p_expedition_time,
            'is_promo'      => $p_promo,
            'is_top_seller' => $p_top_seller,
            'is_ext'        => $p_is_ext,
            'article_type'  => $p_type,
            'active'        => $p_status,
            'mattress_type' => $p_mattress
        );

        $article = \App\Model\Article::create($data);

        $newid = $article->id;
        $description   = str_replace( $p_description, "\n", "<br/>\n");
        $conditions   = str_replace( $p_conditions, "\n", "<br/>\n");

        $path = '/images/articles/'.$newid.'/';
        $img_path = $_SERVER['DOCUMENT_ROOT'].$path;

        $this->filesystem->mkdir($img_path, 0755);


        if($p_img_main){
            $file_ext = $p_img_main->getClientOriginalExtension();
            $filename = 'img_main.'.$file_ext;
            $this->logger->info("Filename: " . $filename);
            $p_img_main->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '1');

        }
        if($p_img_2){
            $file_ext = $p_img_2->getClientOriginalExtension();
            $filename = 'img_2.'.$file_ext;
            $p_img_2->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '0');
        }
        if($p_img_3){
            $file_ext = $p_img_3->getClientOriginalExtension();
            $filename = 'img_3.'.$file_ext;
            $p_img_3->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '0');
        }
        if($p_img_4){
            $file_ext = $p_img_4->getClientOriginalExtension();
            $filename = 'img_4.'.$file_ext;
            $p_img_4->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '0');
        }
        if($p_img_5){
            $file_ext = $p_img_5->getClientOriginalExtension();
            $filename = 'img_5.'.$file_ext;
            $p_img_5->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '0');
        }
        if($p_img_6){
            $file_ext = $p_img_6->getClientOriginalExtension();
            $filename = 'img_6.'.$file_ext;
            $p_img_6->move($img_path,$filename);

            $img_url = $path.$filename;
            $set_image = self::setImage($newid, $img_url, '0');
        }

        foreach(array_keys($p_height) as $key){
            $args = [
                'article_id'     => $newid,
                'stk'            => $p_stk[$key],
                'height'         => $p_height[$key],
                'width'          => $p_width[$key],
                'cost'           => $p_cost[$key],
                'price'          => $p_price[$key],
                'stock'          => $p_stock[$key],
                'original_price' => $p_original_price[$key]
            ];

            $set_option = self::setOption($args);
        }

        return true;
    }

    public function setImage($id, $img_url, $is_main)
    {
        $images = \App\Model\ArticleImage::insert(
                array(
                    'article_id'    => $id,
                    'image_url'     => $img_url,
                    'is_main'       => $is_main
                )
            );
    }

    public function setOption($args)
    {
        $this->logger->info('[setOption] Came to save new option with args: ' . var_export($args, true));

        $options = new \App\Model\ArticleOption();
        $options->article_id = $args['article_id'];
        $options->size = $args['width'].'x'.$args['height'];
        $options->price = $args['price'];
        $options->original_price = $args['original_price'];
        $options->width = $args['width'];
        $options->height = $args['height'];
        $options->stock = $args['stock'];
        $options->stk = $args['stk'];
        $options->cost = $args['cost'];
        $result = $options->save();

        $this->logger->info('[setOption] Save option with result ' . var_export($result, true));
    }

    public function deleteOption($stk)
    {
        ArticleOption::where('stk',$stk)->update(['stock'=>'-1']);
    }

    private function deleteArticle($stk)
    {
        $article = Article::where('main_stk',$stk)->first();

        $article->options->delete();
        $article->images->delete();
        $article->delete();

        return;
    }

    public function editProduct($args)
    {

        $locale = Utils::getLocale();
        $default_locale = $this->request->getDefaultLocale();
        
        $article = \App\Model\Article::find($args['p_id']);
        $article->main_stk = $args['main_stk'];
        $article->brand = $args['p_brand'];
        $article->title = $args['p_title'];

        // Only update url title if is default language. If not it will insert the converted title of the language in the default translation
        if ($locale === $default_locale) {
            $article->url_title = preg_replace(array_keys(self::$utf8), array_values(self::$utf8), $args['p_title']);
        }

        $article->deep = $args['p_deep'];
        $article->level = $args['p_level'];
        $article->article_type = $args['p_type'];
        $article->ship_mainland = $args['p_ship_mainland'];
        $article->ship_islands = $args['p_ship_islands'];
        $article->expedition_time = $args['expedition_time'];
        $article->is_promo = $args['p_is_promo'];
        $article->is_ext = $args['p_is_ext'];
        $article->active = $args['p_status'];
        $article->mattress_type = $args['p_mattress'];
        $article->is_top_seller = $args['p_is_top_seller'];
        $article->description = $args['p_description'];
        $article->conditions = $args['p_conditions'];
        $article->details = $args['p_details'];
        $article->save();

        $path = '/images/articles/'.$args['p_id'].'/';
        $img_path = $_SERVER['DOCUMENT_ROOT'].$path;

        $p_img_id = $args['img_id'];

        if($p_img_main = $args['p_img_main']){
            $file_ext = $p_img_main->getClientOriginalExtension();
            $filename = 'img_main.'.$file_ext;
            $p_img_main->move($img_path,$filename);
            $img_id = $p_img_id[0];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);

        }
        if($p_img_2 = $args['p_img_2']){
            $file_ext = $p_img_2->getClientOriginalExtension();
            $filename = 'img_2.'.$file_ext;
            $p_img_2->move($img_path,$filename);
            $img_id = $p_img_id[1];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);
        }
        if($p_img_3 = $args['p_img_3']){
            $file_ext = $p_img_3->getClientOriginalExtension();
            $filename = 'img_3.'.$file_ext;
            $p_img_3->move($img_path,$filename);
            $img_id = $p_img_id[2];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);
        }
        if($p_img_4 = $args['p_img_4']){
            $file_ext = $p_img_4->getClientOriginalExtension();
            $filename = 'img_4.'.$file_ext;
            $p_img_4->move($img_path,$filename);
            $img_id = $p_img_id[3];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);
        }
        if($p_img_5 = $args['p_img_5']){
            $file_ext = $p_img_5->getClientOriginalExtension();
            $filename = 'img_5.'.$file_ext;
            $p_img_5->move($img_path,$filename);
            $img_id = $p_img_id[4];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);
        }
        if($p_img_6 = $args['p_img_6']){
            $file_ext = $p_img_6->getClientOriginalExtension();
            $filename = 'img_6.'.$file_ext;
            $p_img_6->move($img_path,$filename);
            $img_id = $p_img_id[5];

            $img_url = $path.$filename;
            $update_image = self::updateImage($img_id, $img_url);
        }
        $existing_options = $this->loadProductOptions($args['p_id']);
        foreach($existing_options as $option) {
            $existing_opt_ids[] = $option['id'];
        }

        foreach($args['p_height'] as $key => $item){

            $opt_args = [
                'option_id' => $args['opt_id'][$key],
                'article_id' => $args['p_id'],
                'stk' => $args['p_stk'][$key],
                'height' => $args['p_height'][$key],
                'width' => $args['p_width'][$key],
                'cost' => $args['p_cost'][$key],
                'price' => $args['p_price'][$key],
                'stock' => $args['p_stock'][$key],
                'original_price' => $args['p_original_price'][$key]
            ];

            if(isset($existing_opt_ids[$key])) {
                if(!in_array($existing_opt_ids[$key], $args['opt_id'])) {

                    self::deleteOption($existing_options[$key]['stk']);
                }
            }

            if (isset($args['opt_id'][$key]) || !empty($args['opt_id'][$key])) {

                $update_option = self::updateOption($opt_args);
            } else {
                $set_option = self::setOption($opt_args);
            }

        }

        return true;
    }

    public function updateImage($id, $img_url)
    {
        $images = \App\Model\ArticleImage::where('img_id', $id)
                ->update(array(
                    'image_url'   => $img_url,
                    'last_modified' => date('Y-m-d H:i:s')
                )
            );

    }

    public function updateOption($args)
    {
        $options = \App\Model\ArticleOption::find($args['option_id']);
        $options->size = $args['width'].'x'.$args['height'];
        $options->price = $args['price'];
        $options->original_price = $args['original_price'];
        $options->width = $args['width'];
        $options->height = $args['height'];
        $options->stock = $args['stock'];
        $options->stk = $args['stk'];
        $options->cost = $args['cost'];
        $options->save();

    }

}

?>
