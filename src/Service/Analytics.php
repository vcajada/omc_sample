<?php

namespace App\Service;

class Analytics
{

    private static $dataLayer = [];
    private $property_id = '';

    public function __construct($args=[])
    {
        if(isset($args['property_id'])){
            $this->property_id = $args['property_id'];
        }
    }

    public static function getDataLayer()
    {
        return self::$dataLayer;
    }

    public static function setDataLayer( $dataLayer )
    {
        self::$dataLayer = $dataLayer;
    }

    public static function clearDataLayer( )
    {
        self::$dataLayer = [];
    }



    public function trackProductList(){
        //Implemented in the view /article_box.twig
        return;
    }

    public function trackSignup(){
        self::$dataLayer[] =  array(
            'event' => 'signup',
            'data' => array(
                'category' => 'Signup',
                'action' => 'Signup'
            )
        );
        return;
    }

    public function trackAddToCart(){
        //die("Implemented in JS");
        return;
    }


    public function trackOrder($args){
        self::$dataLayer[] =  array(
            'event' => 'order',
            'data' => array(
                'category' => 'Order',
                'action' => $args['type'],
                'value' => $args['total']
            )
        );
    }

    public function trackPurchase($args){
        $cart = $args['cart'];

        if (!$cart) { return;  }

        if( $cart->pay_type === "PAY-MB" ){
            return $this->trackDelayedPurchase($args);
        }

        $data = $this->createDataFromCart($cart);

        self::$dataLayer[] = $data;
        return;
    }

    public function createDataFromCart($cart){
        $articles = \App\Service\Carts::getCartArticlesInfoFromCart($cart);

        $out =  array(
            'event' => 'purchase',
            'ecommerce' => array(
                'purchase' => array(
                    'actionField' => array(
                        'id' => $cart->id,
                        'revenue' => $cart->cart_total,         // Total transaction value (incl. tax and shipping)
                        'shipping' => $cart->cart_shipping,
                        'coupon' => $cart->promocode
                    ),
                    'products' => $articles
                )
            ),
        );

        return $out;
    }

    public function trackDelayedPurchase($args){
        $cart = $args['cart'];
        $property_id = $this->property_id;

        $client_id = $cart->ga_visitor_id;

        $params = array(
        'v'     => 1,                               //Version
        'tid'   => $property_id,
        'cid'   => $client_id,
        't'     => 'event',
        'ec'    => 'Ecommerce',
        'ea'    => 'Purchase',
        'ti'    => $cart->id,
        'ta'    => 'O meu colchao',
        'tr'    => $cart->cart_total,
        'ts'    => $cart->cart_shipping,
        'tcc'   => $cart->promocode,
        'pa'    => 'purchase',
        'cu'    => 'EUR',
        'ni'    => '0',
        );


        $products = $this->buildDelayedPurchaseProducts($cart,$args);
        $params = $params + $products;

        if(isset($args['dry_run']) && $args['dry_run']) {
            $response = 'dry_run selected';
        } else {
            $response = $this->notifyDelayed($params,$args);
        }

        return array ( 'query' => $params, 'response' => $response  );
    }


    public static function buildDelayedPurchaseProducts($oCart,$args){
        $cart = $args['cart'];

        $out = array();

        $idx = 1;
        foreach($cart->articles as $item){
            $option = $item->option;

            // Base title from the ARTICLE
            $title = $option->article->title;
            $sku = $option->id;

            $out[ 'pr'.$idx.'id'  ] = $sku;
            $out[ 'pr'.$idx.'nm'  ] = self::sanitize($title);
            $out[ 'pr'.$idx.'pr'  ] = $option->price;
            $out[ 'pr'.$idx.'qt'  ] = $item->quant;
            $out[ 'pr'.$idx.'ps'  ] = $idx;
            $out[ 'pr'.$idx.'ca'  ] = 'Category';

            $idx++;
        }

        return $out;
    }

    public function notifyDelayed($params,$options)
    {

        #$baseURL    = 'https://www.google-analytics.com/debug/collect';
        $baseURL    = 'https://www.google-analytics.com/collect';
        $propertyID = $this->property_id;

        if (!isset($params['t'])) {
            return (object) array('error' => 'missing Hit Type (t)');
        }

        if (!isset($params['cid'])) {
            return (object) array('error' => 'missing Client ID (cid)');
        }

        return $this->cURLPost($baseURL, http_build_query($params));
    }


    public function cURLPost($url, $fields) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResp = curl_exec($ch);
        $curlError = curl_error($ch);

        curl_close($ch);

        return (object)array('response' => $curlResp, 'error' => $curlError);
    }

    public static function sanitize($str) {
        return html_entity_decode($str,ENT_COMPAT|ENT_HTML401,'UTF-8');
    }

    public function trackTest(){
        self::$dataLayer[] =  array(
            'event' => 'test',
            'data' => array(
                'value' => time()
            )
        );
    }



}

?>
