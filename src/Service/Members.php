<?php

namespace App\Service;

class Members
{

    public function getTotal()
    {
        $members = \App\Model\User::count();
        
        return $members;
    }
    
    public function getMemberList($mbrid, $mbrname)
    {
        $cacheKey = 'getMemberList';
        
        $members = \App\Model\User::join('addresses', 'users.id', '=', 'addresses.user_id')
            ->select('users.*', 'addresses.addr_city','addresses.is_main')
            ->where('addresses.is_main', '=', 'yes');
        if($mbrid){
            $members->where('users.id', $mbrid);
        }
        if($mbrname){
            $members->where('users.name', $mbrname);
        }
        
        $mbrquery = $members->get();
                
        return $mbrquery;
    }
    
    public function getMemberData($member_id)
    {
        $member = \App\Model\User::where('id', $member_id)
            ->get();
        
        return $member;
    }
    
    public function getMemberPrefs($member_id)
    {
        $member = \App\Model\UserPref::where('user_id', $member_id)
            ->get();
        
        return $member;
    }
    
    public function getMemberAddr($member_id)
    {
        $member = \App\Model\Address::where('user_id', $member_id)
            ->where('is_main', 'yes')
            ->get();
        
        return $member;
    }
    
    public function getMemberOrders($member_id)
    {
        $member = \App\Model\CartArticle::join('carts', 'cart_articles.cart_id', '=', 'carts.id')
            ->join('article_options', 'cart_articles.article_id', '=', 'article_options.id')
            ->select('cart_articles.*', 'carts.*', 'article_options.*')
            ->where('cart_articles.item_status', '=', 'ORDER')
            ->where('carts.user_id', $member_id)
            ->orderBy('carts.date_modified','DESC')
            ->get();
        
        return $member;
    }
    
    public function getMemberPromos($member_id)
    {
        $member = \App\Model\Promocode::where('user_id', $member_id)
            ->get();
        
        return $member;
    }
    
}

?>
