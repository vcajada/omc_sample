<?php

namespace App\Service\Payments;

class Adyen extends \App\Service\Payments
{
    private $username;
    private $password;
    private $url;
    private $url3DSecure;
    private $urlCheckoutSetup; // Used for checkout API
    private $urlCheckoutVerify; // Used for checkout API
    private $merchantAccount;
    private $reference;
    private $amount;
    private $additionalData;
    private $billingAddress;
    private $deliveryAddress;
    private $shopperInfo;
    private $threeDSecure;
    private $md;
    private $paResponse;
    private $checkoutChannel;
    private $checkoutToken;
    private $checkoutUsername;
    private $checkoutPassword;
    private $checkoutApiKey;

    const AUTHORIZED        = 'Authorised';
    const ERROR             = 'Error';
    const CANCELLED         = 'Cancelled';
    const RECEIVED          = 'Received';
    const REDIRECTSHOPPER   = 'RedirectShopper';

    public function __construct($config)
    {
        if (!$config['username'] || !$config['password'] || !$config['url'] || !$config['merchantAccount']) {
            return false;
        }
        $this->username     = $config['username'];
        $this->password     = $config['password'];
        $this->url          = $config['url'];
        $this->url3DSecure  = $config['url3DSecure'];
        $this->merchantAccount = $config['merchantAccount'];
        $this->urlCheckoutSetup = $config['checkoutSetupUrl'];
        $this->urlCheckoutVerify = $config['checkoutVerifyUrl'];
        $this->checkoutUsername = $config['checkoutUsername'];
        $this->checkoutPassword = $config['checkoutPassword'];
        $this->checkoutApiKey = $config['checkoutApiKey'];
    }

    public function reference($reference = null)
    {
        if ($reference) {
            $this->reference = $reference;
        }

        return $reference;
    }

    public function amount($amount = null)
    {
        if ($amount) {
            $this->amount = $amount;
        }

        return $amount;
    }

    public function additionalData($additionalData = null)
    {
        if ($additionalData) {
            $this->additionalData = $additionalData;
        }
        return $additionalData;
    }

    public function billingAddress($address = array())
    {
        if ($address) {
            $this->billingAddress = $address;
        };
        return $this->billingAddress;
    }

    public function deliveryAddress($address = array())
    {
        if ($address) {
            $this->deliveryAddress = $address;
        };
        return $this->deliveryAddress;
    }

    public function shopperInfo($info = array())
    {
        if ($info) {
            $this->shopperInfo = $info;
        };
        return $this->shopperInfo;
    }

    public function threeDSecure($info = array())
    {
        if ($info) {
            $this->threeDSecure = $info;
        };
        return $this->threeDSecure;
    }

    public function md($md = null)
    {
        if ($md) {
            $this->md = $md;
        }
        return $this->md;
    }

    public function paRes($paRes = null)
    {
        if ($paRes) {
            $this->paRes = $paRes;
        };
        return $this->paRes;
    }

    public function checkoutChannel($checkoutChannel = null)
    {
        if ($checkoutChannel) {
            $this->checkoutChannel = $checkoutChannel;
        }

        return $checkoutChannel;
    }

    public function checkoutToken($checkoutToken = null)
    {
        if ($checkoutToken) {
            $this->checkoutToken = $checkoutToken;
        }

        return $checkoutToken;
    }


    public function validate()
    {
        $params = array(
            'additionalData'    => $this->additionalData,
            'amount'            => array(
                                        'value' => intval($this->amount),
                                        'currency' => 'EUR'
                                    ),
            'reference'         => $this->reference,
            'merchantAccount'   => $this->merchantAccount,
            'billingAddress'    => $this->billingAddress,
            'deliveryAddress'   => $this->deliveryAddress,
            'shopperLocale'     => 'pt_PT',
            'shopperEmail'      => $this->shopperInfo['email'],
            'shopperIP'         => $this->shopperInfo['ip']
            );

        if ($this->threeDSecure) {
            $params['browserInfo'] = array(
                'userAgent' => $this->threeDSecure['userAgent'],
                'acceptHeader' => $this->threeDSecure['acceptHeader']
            );
        }
        $response = $this->curlPost(
            array(
                'url' => $this->url,
                'params' => $params,
                'contentType' => 'json',
                'username' => $this->username,
                'password' => $this->password )
        );


        return $this->validateCurlResponse($response);
    }

    public function validate3D()
    {
        $params = array(
            'merchantAccount'   => $this->merchantAccount,
            'browserInfo'       => array(
                    'userAgent' => $this->threeDSecure['userAgent'],
                    'acceptHeader' => $this->threeDSecure['acceptHeader']
                                    ),
            'md'                => $this->md,
            'paResponse'        => $this->paRes,
            'shopperIP'         => $this->shopperInfo['ip'],
            'shopperLocale'     => 'pt_PT',
            );

        $response = $this->curlPost(
            array(
                'url' => $this->url3DSecure,
                'params' => $params,
                'contentType' => 'json',
                'username' => $this->username,
                'password' => $this->password
            )
        );


        return $this->validateCurlResponse($response);
    }

    public function checkoutSetup()
    {
        $params = array(
            'amount'            => array(
                                        'value' => intval($this->amount),
                                        'currency' => 'EUR'
                                    ),
            'reference'         => $this->reference,
            'merchantAccount'   => $this->merchantAccount,
            'billingAddress'    => $this->billingAddress,
            'channel'           => $this->checkoutChannel,
            'returnUrl'         => 'clubefashion://',
            'shopperLocale'     => 'pt_PT',
            'countryCode'       => 'PT',
            'shopperReference'  => $this->shopperInfo['email'],
            'token'             => $this->checkoutToken
        );

        $response = $this->curlPost(
            array(
                'url' => $this->urlCheckoutSetup,
                'headers' => [ 'X-API-Key: ' . $this->checkoutApiKey ],
                'params' => $params,
                'contentType' => 'json',
                'username' => $this->checkoutUsername,
                'password' => $this->checkoutPassword )
        );

        return $response['response'];
    }

    // Returns JSON
    public function checkoutVerify($payload)
    {
        $params = array( 'payload' => $payload);
        $response = $this->curlPost(
            array(
                'url' => $this->urlCheckoutVerify,
                'headers' => [ 'X-API-Key: ' . $this->checkoutApiKey ],
                'params' => $params,
                'contentType' => 'json',
                'username' => $this->checkoutUsername,
                'password' => $this->checkoutPassword )
        );

        return $response['response'];
    }

    private function validateCurlResponse($response)
    {
        if (!$response || !isset($response['response']) || !$response['response'] || $response['error']) {
            return array('error' => true, 'ok' => false, 'response' => $response );
        }

        $adyenResponse = json_decode($response['response']);


        if (!isset($adyenResponse->resultCode)) {
            return array('error' => true, 'ok' => false, 'response' => $adyenResponse );
        }

        /* Documentation says
            When a payment authorisation response includes resultCode and refusalReason, the transaction failed or was declined.
            Check their values for further details about any issues that affected the operation.
        */

        if (
            ($adyenResponse->resultCode == self::AUTHORIZED || $adyenResponse->resultCode == self::REDIRECTSHOPPER) &&
                !isset($adyenResponse->refusalReason)
            ) {
            return array('error' => false, 'ok' => true, 'response' => $adyenResponse );
        }

        return array('error' => false, 'ok' => false, 'response' => $adyenResponse );
    }
}
