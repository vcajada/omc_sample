<?php

namespace App\Service\Payments;

use AESGCM\AESGCM;
use \App\Service\Payments;

class Dps extends Payments
{
    private $user_id;
    private $password;
    private $url;
    private $entity_id;
    private $entity;
    private $validity;
    private $amount;
    private $store_name;
    private $env;

    private $user = array();

    public function setConfig($user_id, $password, $base_url, $entity_id, $entity, $validity, $secret, $store_name, $env)
    {
        if (!$user_id || !$password || !$base_url || !$entity_id || !$entity || !$validity) {
            return false;
        }
        $this->user_id      = $user_id;
        $this->password     = $password;
        $this->url          = $base_url;
        $this->entity_id    = $entity_id;
        $this->entity       = $entity;
        $this->validity     = $validity;
        $this->secret       = $secret;
        $this->store_name   = $store_name;
        $this->env          = $env;
    }

    public function user($key, $value)
    {
        if ($value) {
            $this->user[$key] = $value;
        }

        return $value;
    }

    public function amount($amount = null)
    {
        if ($amount) {
            $this->amount = $amount;
        }

        return $amount;
    }

    public function reference($reference = null)
    {
        if ($reference) {
            $this->reference = $reference;
        }

        return $reference;
    }

    public function validity($validity = null)
    {
        if ($validity) {
            $this->validity = $validity;
        }

        return $validity;
    }

    public function getMbReferences()
    {

        $start_date = new \DateTime();
        $start_date->setTimeZone(new \DateTimeZone("Europe/Lisbon"));

        $end_date = new \DateTime();
        $end_date->setTimeZone(new \DateTimeZone("Europe/Lisbon"));

        $interval = new \DateInterval('PT' . $this->validity . 'S');
        date_add($end_date, $interval);

        $start_date_fixed = str_replace('+', '.001+', $start_date->format(\DateTime::ATOM));
        $end_date_fixed = str_replace('+', '.001+', $end_date->format(\DateTime::ATOM));

        $params = [
            'authentication.userId'                         => $this->user_id,
            'authentication.password'                       => $this->password,
            'authentication.entityId'                       => $this->entity_id,
            'amount'                                        => sprintf("%.2f", $this->amount),
            'currency'                                      => 'EUR',
            'paymentBrand'                                  => 'SIBS_MULTIBANCO',
            'paymentType'                                   => 'PA',
            'billing.country'                               => 'PT',
            'merchantTransactionId'                         => $this->reference,
            'customParameters[storeName]'                   => $this->store_name,
            'customParameters[SIBSMULTIBANCO_PtmntEntty]'   => $this->entity,
            'customParameters[SIBSMULTIBANCO_RefIntlDtTm]'  => $start_date_fixed,
            'customParameters[SIBSMULTIBANCO_RefLmtDtTm]'   => $end_date_fixed, //'2017-11-01T19:25:00.001+01:00',
        ];


        if ($this->env !== 'prod') {
            $params['testMode'] = 'EXTERNAL';
            $params['customParameters[SIBS_ENV]'] = 'QLY';
        }

        $response = $this->curlPost(
            array(
                'url' => $this->url.'/v1/payments',
                'params' => $params,
            )
        );

        if (!$response || !isset($response['response']) || !$response['response'] || $response['error']) {
            return array('error' => true, 'response' => $response );
        }

        $response = json_decode($response['response']);
        $success_regex = '/^(000\.000\.|000\.100\.1|000\.[36])/';
        if (isset($response->result) && isset($response->result->code) && preg_match($success_regex, $response->result->code)) {
            $amount = $response->resultDetails->amount;
            $entity = $response->resultDetails->ptmntEntty;
            $reference = $response->resultDetails->pmtRef;
            $reference = preg_replace('/(\d{3})(\d{3})(\d{3})/', '$1 $2 $3', $reference); // Pretty print

            return [
                'error' => false,
                'reference' => $reference,
                'entity' => $entity,
                'amount' => $amount,
            ];

        }

        return array('error' => true, 'response' => $response );
    }

    public function processResponse($params)
    {
        if (!isset($params['encryptedBody']) ||
            !isset($params['initializationVector']) ||
            !isset($params['authenticationTag'])
        ) {
            return array('error' => true, 'ok' => false, 'response' => 'Missing params' );
        }

        $encryptedBody          = hex2bin($params['encryptedBody']);
        $initializationVector   = hex2bin($params['initializationVector']);
        $authenticationTag      = hex2bin($params['authenticationTag']);
        $secret                 = hex2bin($this->secret);

        try {
            $result = json_decode(AESGCM::decrypt($secret, $initializationVector, $encryptedBody, null, $authenticationTag));
        } catch (\Exception $e) {
            return array('error' => true, 'ok' => false, 'response' => 'Caught exception ' . $e->getMessage() );
        }

        return array('error' => false, 'ok' => true, 'response' => $result);
    }

    /*Mb way*/
    // Not used Should check difference from payments
    public function processCheckout($cart)
    {
        $url = $this->url."/v1/checkouts";
        $user_id = $this->user_id;
        $password = $this->password;
        $entity_id = $this->entity_id;
        $amount = sprintf("%.2f", $cart->getTotal());

        $data = array(
            "authentication.userId" => $user_id,
            "authentication.password" => $password,
            "authentication.entityId" => $entity_id,
            "amount" => $amount,
            "merchantTransactionId" => $cart->id,
            "currency" => "EUR",
            "paymentType" => "DB",
            "customParameters[storeName]" => $this->store_name,
        );

        if ($this->app['env'] !== 'prod') {
            $data['testMode'] = 'EXTERNAL';
            $data['customParameters[SIBS_ENV]'] = 'QLY';
        }

        $response = $this->curlPost(
            array(
                'url' => $url,
                'params' => $data
            )
        );

        return $response;
    }


    public function mbwayPayment()
    {

        $params = [
            'authentication.userId'         => $this->user_id,
            'authentication.password'       => $this->password,
            'authentication.entityId'       => $this->entity_id,
            'amount'                        => sprintf("%.2f", $this->amount),
            'currency'                      => 'EUR',
            'paymentBrand'                  => 'MBWAY',
            'paymentType'                   => 'DB',
            'billing.country'               => 'PT',
            'merchantTransactionId'         => $this->reference,
            "virtualAccount.accountId"      => '351#'. $this->user['mbway_phone'],
            'customParameters[storeName]'   => $this->store_name,
            "customParameters[mbr_id]"      => $this->user['id'],
            #"shopperResultUrl"              => $SITE_URL.getMenuLink('payment_mbway')."oID=".$_GET['oID']."&processing=1",
        ];

        if ($this->env !== 'prod') {
            $params['testMode'] = 'EXTERNAL';
            $params['customParameters[SIBS_ENV]'] = 'QLY';
        }

        $this->logger->info('DPSLOG - '. var_export(array(
                'url' => $this->url.'/v1/payments',
                'params' => $params,
                'timeout' => 10
            ), true
        ));
        $json_response = $this->curlPost(
            array(
                'url' => $this->url.'/v1/payments',
                'params' => $params,
                'timeout' => 10
            )
        );

        $to_log = array(
            'cart_id'  => $this->reference,
            'response'      => $json_response['response'],
        );

        if (!$json_response || !isset($json_response['response']) || !$json_response['response'] || $json_response['error']) {
            $this->logger->info('DPSLOG - ' . var_export($to_log, true));
            //DPSLog::create($toLog);
            return array('error' => true, 'ok' => false, 'response' => $json_response );
        }

        $response = json_decode($json_response['response']);

        // Add resultCode to DPSlog
        $result_code = ( isset($response->result->code) ? $response->result->code : 'UNKNOWN');
        $to_log['result_code'] = $result_code;
        $this->logger->info('DPSLOG - ' . var_export($to_log, true));
        //DPSLog::create($toLog);


        $success_regex = '/^(000\.200|800\.400\.5|100\.400\.500)/';

        if (isset($response->result) && isset($response->result->code) && preg_match($success_regex, $response->result->code)) {
            return ['error' => false,
                    'ok' => true,
                    'response' => $response
                 ];
        }

        return array('error' => true, 'ok' => false, 'response' => $response );
    }
}
