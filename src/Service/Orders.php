<?php

namespace App\Service;

class Orders extends BaseService
{

    public function getTotal()
    {
        $orders = \App\Model\CartArticle::join('carts', 'cart_articles.cart_id', '=', 'carts.id')
            ->join('article_options', 'cart_articles.article_id', '=', 'article_options.id')
            ->select('cart_articles.*', 'carts.*', 'article_options.*')
            ->where('cart_articles.item_status', '=', 'ORDER')
            ->count();

        return $orders;
    }

    public function totalBilled()
    {
        $paid = \App\Model\CartArticle::sum('ord_paid');

        return $paid;
    }

    public function totalDiscounts()
    {

        $discounts = \App\Model\Cart::where('pay_status','=','PAID')
            ->sum('cart_discount');

        return $discounts;
    }

    public function totalDevDiscounts()
    {

        $discounts = \App\Model\Cart::where('pay_status','=','PAID')
            ->whereRaw('LEFT(promocode,3) = "DEV"')
            ->sum('cart_discount');

        return $discounts;
    }


    public function getLast()
    {
        $orders = $this->getBaseQuery()
            ->orderBy('carts.date_modified','DESC')
            ->limit('5')
            ->get();

        return $this->handleLegacyTranslation($orders);
    }

    public function getOrderList($cartid, $orderid, $prodid, $mbrid, $paytype, $paystat, $ordstat, $email, $locale, $date_paid = NULL)
    {
        $cacheKey = 'getOrderList';

        $orders = $this->getBaseQuery()
            ->join('users', 'carts.user_id', '=', 'users.id')
            ->join('addresses', 'carts.dellivery_addr_id', '=', 'addresses.id')
            ->addSelect('users.*', 'addresses.*')
            ->where('cart_articles.item_status', '=', 'ORDER');
        if($cartid){
            $orders->where('cart_articles.cart_id', $cartid);
        }
        if($orderid){
            $orders->where('cart_articles.item_id', $orderid);
        }
        if($prodid){
            $orders->where('article_id', $prodid);
        }
        if($mbrid){
            $orders->where('carts.user_id', $mbrid);
        }
        if($paytype){
            $orders->where('carts.pay_type', $paytype);
        }
        if($paystat){
            $orders->where('carts.pay_status', $paystat);
        }
        if($ordstat){
            $orders->where('carts.status', $ordstat);
        }
        if($email){
            $orders->where('users.email', $email);
        }
        if(!is_null($date_paid)) {
            $orders->whereRaw('DATE(carts.date_paid) = ?', array($date_paid));
        }
        if($locale) {
            $orders->where('carts.locale', $locale);
        }
        $orders->orderBy('carts.date_modified','DESC');

        $ordersquery = $orders->get();

        return $this->handleLegacyTranslation($ordersquery);
    }

    public function getOrdersByDatePaid($date_paid) {
        return $this->getOrderList('', '', '', '', '', 'PAID', 'DONE', '', $date_paid);
    }

    public function getMonths()
    {
        $months = array();
        // vlopes - ok for trads only used for BO
        setlocale(LC_TIME, "");
        setlocale(LC_TIME, "pt_PT");

        for ($i = 7; $i >= 0; $i--) {
            $month = strftime("%B", strtotime(date( 'Y-m-01' )." -$i months"));
            array_push($months, $month);
        }

        /*$en = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $pt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

        $months = str_replace($en, $pt, $months);*/

        return $months;
    }

    public function getDates($time)
    {
        $date = date("F, Y", strtotime($time));

        $en = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $pt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

        $date = str_replace($en, $pt, $date);

        return $date;
    }

    public function getChartData(){

        $data = \App\Model\Cart::groupBy(DB::raw('year(carts.date_paid)'),DB::raw('month(carts.date_paid)'))
                    ->selectRaw('sum(cart_total) as prods')
                    ->selectRaw('sum(cart_shipping) as ships')
                    ->selectRaw('sum(case when left(promocode,3)="DEV" then cart_discount else 0 End) as dev_promocodes')
                    ->selectRaw('sum(case when left(promocode,3)<>"DEV" then cart_discount else 0 End) as mkt_promocodes')
                    ->whereRaw('carts.pay_status="PAID"')
                    ->orderByRaw('year(carts.date_paid) DESC')
                    ->orderByRaw('month(carts.date_paid) DESC')
                    ->limit(8)->get()
                    ->toArray();

        $data = array_reverse($data);

        return $data;
    }

    /*
    public function getMonthlySales()
    {
        $sales = \OMC\Models\Cart::groupBy(DB::raw('year(date_paid)'),Capsule::raw('month(date_paid)'))
                    ->selectRaw('sum(cart_total) as sum')
                    ->orderByRaw('year(date_paid) DESC')
                    ->orderByRaw('month(date_paid) DESC')
                    ->limit(8)->get()
                    ->pluck('sum')
                    ->toArray();

        $sales = array_reverse($sales);

        return $sales;
    }

    public function getMonthlyShips()
    {
        $ships = \OMC\Models\Cart::groupBy(DB::raw('year(date_paid)'),Capsule::raw('month(date_paid)'))
                    ->selectRaw('sum(cart_shipping) as sum')
                    ->orderByRaw('year(date_paid) DESC')
                    ->orderByRaw('month(date_paid) DESC')
                    ->limit(8)->get()
                    ->pluck('sum')
                    ->toArray();

        $ships = array_reverse($ships);

        return $ships;
    }
    */

    public function getMonthlyCosts()
    {

        // select sum(AO.cost),month(C.date_paid) FROM carts AS C LEFT JOIN cart_articles AS CA ON CA.cart_id=C.id LEFT JOIN article_options AS AO ON CA.article_id= AO.id GROUP BY year(C.date_paid) desc, month(C.date_paid) desc  order by year(C.date_paid), month(C.date_paid);

        $costs = \App\Model\Cart::groupBy(DB::raw('year(carts.date_paid)'),DB::raw('month(carts.date_paid)'))
                    //->selectRaw('sum(article_options.cost) as sum')
                    ->join('cart_articles','cart_articles.cart_id','=','carts.id')
                    ->join('article_options','cart_articles.article_id','=','article_options.id')
                    ->orderByRaw('year(carts.date_paid) DESC')
                    ->orderByRaw('month(carts.date_paid) DESC')
                    ->limit(8)->get()
                    ->pluck('sum')
                    ->toArray();

        $costs = array_reverse($costs);
        return $costs;
    }

    public function getAccounting($args)
    {
        $cartid     = $args['cartid'];
        $orderid    = $args['orderid'];
        $prodid      = $args['prodid'];
        $dates      = $args['dates'];
        $validated  = $args['validated'];
        $is_ext     = $args['is_ext'];
        $locale     = $args['locale'];

        $list = $this->getBaseQuery()
            ->join('users', 'carts.user_id', '=', 'users.id')
            ->join('addresses', 'carts.dellivery_addr_id', '=', 'addresses.id')
            ->addSelect('users.*', 'addresses.*')
            ->where('carts.status', '=', 'DONE');

        if($dates){
            $start = substr($dates, 0, -13);
            $end = substr($dates, 13);
#            $start_ary = explode("/", $start);
#            $end_ary = explode("/", $end);
#
#            $start = $start_ary[2] . "-" . $start_ary[0] . "-". $start_ary[1];
#            $end = $end_ary[2] . "-" . $end_ary[0] . "-". $end_ary[1];


            $start .= ' 00:00:00';
            $end .= ' 23:59:59';
            $list->whereDate('carts.date_paid', '>=', $start);
            $list->whereDate('carts.date_paid', '<=', $end);
        }else{
            // Don't filter if no dates set
            #$list->whereDate('carts.date_paid', '=', date('Y-m-d'));
        }
        if($cartid){
            $list->where('carts.id', '=', $cartid);
        }
        if($orderid){
            $list->where('cart_articles.item_id', '=', $orderid);
        }
        if($prodid){
            $list->where('article_id', $prodid);
        }

        if(isset($validated)){
            if($validated=="validated"){
                $list->where('cart_articles.ord_validated','=','YES');
            }
            if($validated=="nonvalidated"){
                $list->where('cart_articles.ord_validated','=','NO');
            }
        }

        if($is_ext != ''){
            $list->where('is_ext', $is_ext);
        }

        if ($locale) {
            $list->where('carts.locale', $locale);
        }

        $list->orderBy('carts.date_paid', 'DESC');

        $orderlist = $list->get();

        return $this->handleLegacyTranslation($orderlist);
    }

    public function getCartExport()
    {
        $time = date('Y-m-d H:i:s',time() - 1 * 60);

        $list = \App\Model\Cart::where('status', '=', 'DONE')
            ->where('pay_status', '=', 'PAID')
            ->where('exported', '=', '0')
            ->where('date_modified', '<=', $time)
            ->orderBy('id', 'DESC')
            ->get();

        return $list;
    }

    public function getautoExport($cartid)
    {
        $list = $this->getBaseQuery()
            ->join('users', 'carts.user_id', '=', 'users.id')
            ->join('addresses', 'carts.dellivery_addr_id', '=', 'addresses.id')
            ->addSelect('users.*', 'addresses.*')
            ->where('articles.is_ext', '=', '0')
            ->where('cart_articles.cart_id', '=', $cartid)
            ->get();

        return $this->handleLegacyTranslation($list);
    }

    public function setCartAsExported($cartid)
    {
        $exported = \App\Model\Cart::where('id', $cartid)
                ->update(array(
                    'exported'   => '1'
                )
            );
    }

    public function setOrderValidated($orderid)
    {
        $validated = \App\Model\CartArticle::where('item_id', $orderid)
            ->update(array(
                'ord_validated' => 'YES'
            )
        );
    }

    public function getListtoExpire()
    {
        $days_ago = date("Y-m-d", strtotime("-2 day"));

        $to_expire = \App\Model\Cart::where('status', 'PENDING')
            ->where('date_modified', '<', $days_ago)
            ->get();

        return $to_expire;

    }

    public function releaseStock($cartid)
    {
        $orders = \App\Model\CartArticle::where('cart_id', $cartid)
            ->get();

        foreach($orders as $order){
            $release = \App\Carts::releaseStock($order->article_id, $order->quant);
        }

    }

    public function getPendingMB()
    {
        $days_ago = date("Y-m-d", strtotime("-1 day"));

        $pending = \App\Model\Cart::where('status', 'PENDING')
            ->where('pay_type', 'PAY-MB')
            ->where('pay_status', 'UNPAID')
            ->where('mb_reminder', '0')
            ->where('date_modified', '=', $days_ago)
            ->get();

        return $pending;

    }

    private function getBaseQuery()
    {

        return \App\Model\CartArticle::join('carts', 'carts.id', '=', 'cart_id')
            ->join('article_options', 'cart_articles.article_id', '=', 'article_options.id')
            ->join('articles', 'article_options.article_id', '=', 'articles.id')
            ->leftJoin('article_translations', function($join) {
                $join->on('article_translations.article_id', '=', 'articles.id')
                    ->on('article_translations.locale', '=', 'carts.locale');
            })
            ->leftJoin('article_option_translations', function($join) {
                $join->on('article_option_translations.article_option_id', '=', 'article_options.id')
                    ->on('article_option_translations.locale', '=', 'carts.locale');
            })
            ->select(
                'cart_articles.*',
                'articles.*',
                'article_options.*',
                'carts.*',
                'article_translations.title as translated_title',
                'article_translations.ship_mainland as translated_ship_mainland',
                'article_translations.ship_islands as translated_ship_islands',
                'article_translations.expedition_time as translated_expedition_time',
                'article_option_translations.price as translated_price',
                'article_option_translations.cost as translated_cost',
                'article_option_translations.original_price as translated_original_price'
            );
    }

    private function handleLegacyTranslation($orders)
    {
        foreach ($orders as $order) {
            if ($order->translated_title !== null) {
                $order->title = $order->translated_title;
            }
            if ($order->translated_ship_mainland !== null) {
                $order->title = $order->translated_ship_mainland;
            }
            if ($order->translated_ship_islands !== null) {
                $order->title = $order->translated_ship_islands;
            }
            if ($order->translated_expedition_time!== null) {
                $order->title = $order->translated_expedition_time;
            }
            if ($order->translated_price !== null) {
                $order->title = $order->translated_price;
            }
            if ($order->translated_cost !== null) {
                $order->title = $order->translated_cost;
            }
            if ($order->translated_original_price !== null) {
                $order->title = $order->translated_original_price;
            }
        }
        return $orders;
    }

}

?>
