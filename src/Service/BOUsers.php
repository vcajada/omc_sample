<?php

namespace App\Service;

use App\Model\BoUser;

class BOUsers
{

    public function forceLogin($user, $session)
    {
        if ($user) {
            $roles = $user->acls->map(function ($a) {
                return $a->role->role;
            })->toArray();

            // build user session
            $session->set('omc_bo_user', array(
            'id_user' => $user->id,
            'nome' => $user->nome,
            'job'   => $user->job,
            'roles' => $roles
        ));

            // Update logins for user
            $userupdate = \App\Model\BoUser::where('id', $user->id)
            ->update(array('last_login' => date("Y-m-d H:i:s")));

            return true;
        } else {
            return false;
        }
    }
    
    public function login($username, $password, $session)
    {
        $username = trim($username);
        $password = trim($password);

        $user = BoUser::where([
            ['state','=','ACTIVE'],
            ['username','=',$username],
            ['password','=',md5($password)],
        ])->first();


        return $this->forceLogin($user, $session);
    }

    public function roles($session)
    {
        $bo_user = $session->get('omc_bo_user');
        $out =  $bo_user['roles'];
        return $out;
    }
    
    public function existsAndIsActive($username)
    {
        $user = \App\Model\BoUser::where('state', '=', 'ACTIVE')
            ->where('username', '=', $username)
            ->get();

        return !empty($user) && count($user)>0;
    }
    
    public function getByUsername($username)
    {
        $user = \App\Model\BoUser::where('state', '=', 'ACTIVE')
            ->where('username', '=', $username)
            ->get();

        if (empty($user[0])) {
            return false;
        } else {
            return $user[0];
        }
    }
    
    public static function GenerateHash($ln = 10)
    {
        $lp = '';
        $salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        while ($i < $ln) {
            $lp = $lp . substr($salt, rand() % 33, 1);
            $i++;
        }
        return $lp;
    }
    
    public function resetPassword($username)
    {
        $user = \App\Model\BoUser::where('state', '=', 'ACTIVE')
            ->where('username', '=', $username)
            ->get();

        if (empty($user)) {
            return false;
        } else {
            $password   =   self::GenerateHash(6);
            
            // Updating member password
            $userupdate = \App\Model\BoUser::where('username', $username)
                ->update(
                    array(
                    'password' => md5($password),
                )
            );
            
            return [$user[0], $password];
        }
    }
    
    public function getUserData($userid)
    {
        $user = \App\Model\BoUser::where('id', '=', $userid)
            ->get();
        
        return $user[0];
    }
    
    public function updatePass($userid, $newpass)
    {
        $userpassupdate = \App\Model\BoUser::where('id', $userid)
                ->update(
                    array(
                    'password' => md5($newpass)
                )
            );
        
        return [
            'status' => true
            ];
    }
}
