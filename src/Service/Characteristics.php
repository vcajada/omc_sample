<?php

namespace App\Service;

use Symfony\Component\Translation\TranslatorInterface;

class Characteristics
{
	
    private $translator;

    function __construct(TranslatorInterface $translator) // TODO use dependency injection
    {
        $request = $GLOBALS['request'];
        $this->translator = $translator;
    }

    public function getBaseCharacteristics() {
    
        $translated_base_characteristics = [
            'altura' => [
                'title' => $this->translator->trans('Altura'),
                'description' => $this->translator->trans('A altura do colchão é medida do centro do colchão. Segundo a norma europeia EN 1334:1996, as medidas dos colchões podem variar +/- 1cm na altura.'),
                'image' => 'cm.svg'
            ],
            'molas' => [
                'title' => $this->translator->trans('Molas'),
                'description' => $this->translator->trans('Os colchões de molas oferecem um ótimo apoio, distribuindo uniformemente o peso do corpo. Também não vai sentir demasiado calor, pois a construção de molas abertas permite que o ar circule.'),
                'image' => 'molas.svg'
            ],
            'molas_ensacadas' => [
                'title' => $this->translator->trans('Molas ensacadas'),
                'description' => $this->translator->trans('Proporcionam um bom grau de firmeza e um comportamento independente, visto ter mais molas por metro quadrado. Adequado para casais e pessoas de grande porte.'),
                'image' => 'molas_ensacadas.svg'
            ],
            'molas_bonnel' => [
                'title' => $this->translator->trans('Molas Bonnel'),
                'description' => $this->translator->trans('Caracterizadas por terem maior flexibilidade e firmeza para o apoio da coluna, tendo uma resistência progressiva. Estas molas são entrelaçadas e feitas em aço, tendo uma excelente durabilidade.'),
                'image' => 'molas_bonnel.svg'
            ],
            'hr' => [
                'title' => $this->translator->trans('HR (espuma)'),
                'description' => $this->translator->trans('Colchões com elevado nível de conforto e uma boa adaptabilidade. Quanto maior a densidade da espuma, maior será a sua firmeza. Adequado para casais, por não haver repercussão de movimentos.'),
                'image' => 'espuma.svg'
            ],
            'latex' => [
                'title' => $this->translator->trans('Látex'),
                'description' => $this->translator->trans('Colchões com bom nível de conforto e uma boa adaptabilidade. Firmeza inferior a um colchão de molas ou de espuma HR. Tem um bom isolamento térmico, sendo uma mais-valia no inverno, mas pode tornar-se muito quente no verão.'),
                'image' => 'latex.svg'
            ],
            'visco_elastico' => [
                'title' => $this->translator->trans('Visco elástico'),
                'description' => $this->translator->trans('Esta camada de conforto molda-se ao corpo em função da temperatura e da pressão exercida, tendo como ponto forte a característica da espuma de memória. Nos pontos de maior pressão (ombros para os homens e peito e ancas para as mulheres) o visco elástico molda-se mais, aliviando a pressão. Nos pontos de menor pressão, o visco elástico resiste mais.'),
                'image' => 'visco_elastico.svg'
            ],
            'viscogel' => [
                'title' => $this->translator->trans('Viscogel'),
                'description' => $this->translator->trans('Esta camada produz menos calor que o viscoelástico, devido às partículas de gel. Molda-se ao corpo em função da temperatura e da pressão exercida. Tem, também, uma maior densidade e uma maior respirabilidade.'),
                'image' => 'viscogel.svg'
            ],
            'firmeza_1' => [
                'title' => $this->translator->trans('Pouco firme'),
                'description' => $this->translator->trans('Colchões com pouca firmeza, onde o suporte não é forte. Elevados níveis de adaptabilidade, não aconselháveis para casais.'),
                'image' => 'firmeza_1.svg'
            ],
            'firmeza_2' => [
                'title' => $this->translator->trans('Médio'),
                'description' => $this->translator->trans('Colchões com firmeza média, sendo o mais indicado para a população em geral.'),
                'image' => 'firmeza_2.svg'
            ],
            'firmeza_3' => [
                'title' => $this->translator->trans('Firme'),
                'description' => $this->translator->trans('Colchões firmes, mas sempre com um bom nível de adaptabilidade.'),
                'image' => 'firmeza_3.svg'
            ],
            'firmeza_4' => [
                'title' => $this->translator->trans('Muito firme'),
                'description' => $this->translator->trans('Colchões muito firmes, considerados muito rijos mas têm sempre uma camada de conforto associada.'),
                'image' => 'firmeza_4.svg'
            ]
        ];

        return $translated_base_characteristics;
    }

    public function getCharacteristics()
    {
        $translated_characteristics = [
            'algodao' => [
                'title' => $this->translator->trans('Algodão'),
                'description' => $this->translator->trans('Fibra natural, sendo um excelente regulador térmico. Não acumula electricidade estática, possui grande elasticidade e é muito resistente.'),
                'image' => 'algodao.svg'
            ],
            'bambu' => [
                'title' => $this->translator->trans('Bambu'),
                'description' => $this->translator->trans('As fibras de bambu absorvem mais humidade e não a retém por ser um tecido com maior respirabilidade. Material 100% natural e ecológico, tem uma maior resistência e é antialérgico.'),
                'image' => 'bambu.svg'
            ],
            'aloevera' => [
                'title' => $this->translator->trans('Aloé vera'),
                'description' => $this->translator->trans('Material 100% natural e ecológico. É antialérgico e tem uma melhor capacidade para dissipar o calor.'),
                'image' => 'aloevera.svg' ],
            'antiacaros' => [
                'title' => $this->translator->trans('Anti-ácaros'),
                'description' => $this->translator->trans('Não acumula ácaros, sendo uma mais valia para a qualidade da respiração da pessoa.'),
                'image' => 'antiacaros.svg'
            ],
            'antialergico' => [
                'title' => $this->translator->trans('Antialérgico'),
                'description' => $this->translator->trans('Materiais e características antialérgicas, oferecendo uma noite de sono tranquila.'),
                'image' => 'antialergico.svg'
            ],
            'respiravel' => [
                'title' => $this->translator->trans('Respirável'),
                'description' => $this->translator->trans('Feito para ter uma maior respirabilidade, o que evita acumular humidade no colchão.'),
                'image' => 'respiravel.svg'
            ],
            'termo_adaptavel' => [
                'title' => $this->translator->trans('Termo adaptável'),
                'description' => $this->translator->trans('Regula a temperatura consoante a pressão exercida no colchão. Mais agradável no inverno e não aquece tanto no verão.'),
                'image' => 'termo_adaptavel.svg'
            ],
            'camas_articuladas' => [
                'title' => $this->translator->trans('Camas articuladas'),
                'description' => $this->translator->trans('Indicado para estrados articulados.'),
                'image' => 'camas_articuladas.svg'
            ],
            'comportamento_independente' => [
                'title' => $this->translator->trans('Comportamento independente'),
                'description' => $this->translator->trans('O movimento de uma pessoa não afeta a pessoa do outro lado. Ideal para casais.'),
                'image' => 'comportamento_independente.svg'
            ],
            'fecho_zip' => [
                'title' => $this->translator->trans('Fecho zip'),
                'description' => $this->translator->trans('Camada de conforto removível, o que permite arejar ou lavar sem prejudicar o colchão.'),
                'image' => 'fecho_zip.svg'
            ],
            'box' => [
                'title' => $this->translator->trans('Box'),
                'description' => $this->translator->trans('Reforço de segurança à volta de colchão, aumentando em 10% a zona de descanso.'),
                'image' => 'box.svg'
            ],
        ];

        return $translated_characteristics;
    }
}


?>
