<?php

/*

CREATE TABLE `article_option_translations` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`article_option_id` int(10) unsigned DEFAULT NULL,
`locale` varchar(2) DEFAULT NULL,
`price` decimal(10,2) DEFAULT NULL,
`cost` decimal(10,2) DEFAULT NULL,
`original_price` decimal(10,2) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `locale` (`locale`,`article_option_id`),
KEY `idx_loc` (`locale`)


) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

*/

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableTrait;

/**
 * @property int $id
 * @property int $article_id
 * @property mixed $size
 * @property mixed $price
 * @property mixed $width
 * @property mixed $height
 * @property int $stock
 * @property mixed $stk
 */
class ArticleOption extends Model
{
    use TranslatableTrait;
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    private $translation_foreign_key = 'article_option_id';
    private $translated_attributes = ['price', 'cost', 'original_price'];

    /**
     * @var array
     */
    protected $fillable = ['article_id', 'size', 'price', 'width', 'height', 'stock','stk','original_price'];

    public function article()
    {
        return $this->belongsTo('\App\Model\Article');
    }


}
