<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property mixed $addr_name
 * @property mixed $addr_street
 * @property mixed $addr_city
 * @property mixed $addr_cp1
 * @property mixed $addr_cp2
 * @property mixed $addr_country
 * @property mixed $addr_phone
 * @property mixed $addr_nif
 * @property mixed $is_main
 */
class Address extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'addr_name', 'addr_street', 'addr_city', 'addr_cp1', 'addr_cp2', 'addr_country', 'addr_phone', 'addr_nif', 'is_main'];
    
     public function user()
    {
        return $this->hasOne('\App\Model\User','id','user_id');
    }
    
    public function carts()
    {
        return $this->hasMany('\App\Model\Cart','dellivery_addr_id','id');
    }

}
