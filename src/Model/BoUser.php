<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BoUser extends Model
{
    public $table = 'bo_users';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['username','password','nome','job','last_login','state'];


    public function acls()
    {
        return $this->hasMany('\App\Model\BoAcl', 'user_id');
    }
}
