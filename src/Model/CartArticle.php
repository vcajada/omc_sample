<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $item_id
 * @property int $cart_id
 * @property int $article_id
 * @property int $quant
 * @property mixed $item_status
 * @property mixed $processed_status
 */
class CartArticle extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $primaryKey = 'item_id';

    /**
     * @var array
     */
    protected $fillable = ['cart_id', 'article_id', 'quant', 'item_status', 'processed_status','ord_unit_cost', 'ord_tot_cost', 'ord_shipping', 'ord_discount', 'ord_devolution', 'ord_total', 'ord_paid', 'ord_validated', 'ord_invoiced' ];

    public function option()
    {
        return $this->hasOne('\App\Model\ArticleOption','id','article_id');
    }

    public function cart()
    {
        return $this->belongsTo('\App\Model\Cart');
    }
}
