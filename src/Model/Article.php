<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TranslatableTrait;

/**
 * @property int $id
 * @property mixed $title
 * @property mixed $url_title
 * @property mixed $discount
 * @property mixed $start_price
 * @property mixed $level
 * @property mixed $article_type
 * @property mixed $ship_mainland
 * @property mixed $ship_islands
 * @property int $active
 * @property int $is_promo
 * @property mixed $brand
 * @property mixed $original_price
 * @property int $is_ext
 */
class Article extends Model
{
    use TranslatableTrait;

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = ['id', 'created', 'last_modified', 'characteristics'];
    private $translation_foreign_key = 'article_id';
    private $translated_attributes = ['title', 'ship_mainland', 'ship_islands', 'expedition_time', 'description', 'conditions', 'details'];

    public function images()
    {
        return $this->hasMany('\App\Model\ArticleImage','article_id');
    }

    public function options()
    {
        return $this->hasMany('\App\Model\ArticleOption','article_id');
    }

    // vlopes - use url_title from db from url because of traductions
    /*public function getUrlTitleAttribute()
    {

        $title = str_replace(" ","_",$this->title);
        return $title;
    }*/

    public function mainImage(){
        return ArticleImage::where('article_id','=',$this->id)->where('is_main','=',1)->first();
        return $this->images()->where('is_main','=',1)->first();
    }

     public function scopeActive($query)
     {
        return $query->where('active', '=', 1);
     }

}
