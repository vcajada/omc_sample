<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $img_id
 * @property int $article_id
 * @property mixed $image_url
 * @property int $is_main
 */
class ArticleImage extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    protected $dates = ['last_modified'];

    /**
     * @var array
     */
    protected $fillable = ['article_id', 'image_url', 'is_main'];

    protected $primaryKey = 'img_id';

    public function scopeIsMain($query)
    {
        return $query->where('is_main', 1);
    }

}
