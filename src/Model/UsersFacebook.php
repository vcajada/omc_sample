<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersFacebook extends Model
{
    public $timestamps = false;
    public $primaryKey = 'pref_id';
}
