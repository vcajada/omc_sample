<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property mixed $code
 * @property string $start_date
 * @property string $end_date
 * @property mixed $type
 * @property mixed $value
 * @property int $user_id
 * @property mixed $date_created
 * @property mixed $date_modified
 * @property mixed $status
 * @property int $uses
 * @property int $limit_total
 * @property int $user_limit
 * @property int $root_id
 */
class Promocode extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['code', 'start_date', 'end_date', 'type', 'value', 'user_id', 'date_created', 'date_modified', 'status', 'uses', 'limit_total', 'user_limit', 'root_id'];

}
