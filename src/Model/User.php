<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property mixed $email
 * @property mixed $password
 * @property mixed $name
 * @property mixed $gender
 * @property mixed $birth
 * @property mixed $phone
 * @property mixed $date_created
 * @property mixed $province
 * @property mixed $user_prefs
 * @property mixed $state
 * @property mixed $last_login
 * @property mixed $sson
 */
class User extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['email', 'password', 'name', 'gender', 'birth', 'phone', 'date_created', 'province', 'user_prefs', 'state', 'last_login', 'sson', 'rgpd'];
    
    public function addresses()
     {
        return $this->hasMany('\App\Model\Address','user_id','id');
     }

    public function address()
    {
        return $this->addresses()->where('is_main', '=', 'yes');
    }

    public function user_prefs(){
        return $this->hasOne('\App\Model\UserPref','user_id','id');
    }

}
