<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BoAcl extends Model
{
    public $timestamps = false;
    protected $fillable = ['user_id','role_id'];


    public function user()
    {
        return $this->hasOne('\App\Model\BoUser', 'id', 'user_id');
    }

    public function role()
    {
        return $this->hasOne('\App\Model\BoRole', 'id', 'role_id');
    }
}
