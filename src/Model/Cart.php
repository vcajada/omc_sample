<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property mixed $date_created
 * @property mixed $date_modified
 * @property mixed $user_ip
 * @property int $user_id
 * @property mixed $cart_hash
 * @property mixed $status
 * @property mixed $cart_articles
 * @property mixed $articles_amount
 * @property mixed $promocode
 * @property mixed $cart_total
 * @property mixed $cart_shipping
 * @property mixed $orders_ref
 * @property int $dellivery_addr_id
 * @property int $invoice_addr_id
 * @property mixed $pay_status
 * @property mixed $pay_type
 * @property mixed $date_paid
 * @property mixed $value_paid
 * @property mixed $cart_discount
 * @property mixed $shipping_option
 * @property mixed $payment_reference
 * @property mixed $payment_entity
 * @property mixed $payment_total
 * @property int $abandonment_notify
 * @property int $exported
 */
class Cart extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['date_created', 'date_modified', 'user_ip', 'user_id', 'cart_hash', 'status', 'cart_articles', 'articles_amount', 'promocode', 'cart_total', 'cart_shipping', 'orders_ref', 'dellivery_addr_id', 'invoice_addr_id', 'pay_status', 'pay_type', 'date_paid', 'value_paid', 'cart_discount', 'shipping_option', 'payment_reference', 'abandonment_notify', 'exported', 'ga_visitor_id', 'payment_entity', 'payment_total'];

     public function articles()
     {
        return $this->hasMany('\App\Model\CartArticle','cart_id','id');
     }

     public function user()
     {
        return $this->hasOne('\App\Model\User','id','user_id');
     }
    
    public function addresses()
     {
        return $this->hasOne('\App\Model\Address','id','dellivery_addr_id');
     }
    public function bill_addresses()
    {
       return $this->hasOne('\App\Model\Address','id','invoice_addr_id');
    }


}
