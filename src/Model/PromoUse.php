<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoUse extends Model
{
    public $timestamps = false;
    protected $table = "promo_uses";
}
