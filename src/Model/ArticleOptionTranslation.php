<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ArticleOptionTranslation extends Model
{
    public $timestamps = false;
}
