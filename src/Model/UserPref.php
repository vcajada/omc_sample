<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPref extends Model
{
    public $timestamps = false;
    public $primaryKey = 'pref_id';
}
