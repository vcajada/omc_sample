<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BoRole extends Model
{
    public $timestamps = false;
    protected $fillable = ['role'];
}
