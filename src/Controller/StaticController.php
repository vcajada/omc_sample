<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use \App\Controller\BaseController;
use \App\Service\Users;
use \App\Service\Faq;


class StaticController extends BaseController {

    public function quemSomosAction() {

         $contents = [

            ];

        return $this->getRenderedContent('quem_somos.twig', $contents);

        }

    public function faqsAction(Faq $faq) {

        $contents = [
            'faqs' => $faq->getTranslatedFaqs()
        ];

        return $this->getRenderedContent('faq.twig', $contents);

        }

    public function termosAction() {

         $contents = [ ];

        return $this->getRenderedContent('termos.twig', $contents);

        }

        public function devolutionsAction() {

            $contents = [ ];

            return $this->getRenderedContent('politica_devolucoes.twig', $contents);

        }

    public function contactosAction(Users $user) {

        $usersession = $this->session->get('omc_user');;
        if($usersession){
            $user_data = $user->getUserData($usersession['id_user']);
            $user_address = $user->getUserAddress($usersession['id_user']);

            $url = $this->getParameter('intgest.url');
            $key = $this->getParameter('intgest.key');
            $ticketList = $user->getInform($url. $key . "/tickets/list/" . $user_data['id']);

             $contents = [
                    'ContactSelected' => 'selected',
                    'userdata'        => $user_data,
                    'useraddr'        => $user_address,
                    'ticketList'      => isset($ticketList[1]) ? $ticketList[1] : ''
            ];
        }else{
            $contents = [
                'ContactSelected' => 'selected'
            ];
        }

        return $this->getRenderedContent('contactos.twig', $contents);

    }

    public function vendasB2BAction() {

        $contents = [];

        return $this->getRenderedContent('vendas_b2b.twig', $contents);

    }

    public function ticketAction(Users $user) {

            $usersession = $this->session->get('omc_user');

            $oUser = \App\Model\User::find($usersession['id_user']);

            $url = $this->getParameter('intgest.url');
            $key = $this->getParameter('intgest.key');


            $fields = [
                'Telefone'  => $this->request->get("telefone"),
                'Assunto'   => $this->request->get("assunto"),
                'Mensagem'  => $this->request->get("mensagem")
            ];

            $toSend['mbr_id'] = $oUser->id;
            $toSend['autologin'] = $oUser->sson;
            $toSend['name'] = $oUser->name;
            $toSend['email'] = $oUser->email;
            $toSend['phone'] = $fields['Telefone'];
            $toSend['subject_id'] = $fields['Assunto'];
            $toSend['message'] = $fields['Mensagem'];
            
            $ticket = self::cURLPost($url.$key."/tickets/add", $toSend);
        
            if($ticket){
                return new JsonResponse(['status'=>true]);
            }else{
                return new JsonResponse(['status'=>false]);
            }
        
    }

    public function ticketEmailAction(LoggerInterface $logger) {

            $nome = $this->request->get("nome");
            $opts = array();

            if(isset($nome) && $nome != '') {
                $opts['subject'] = 'Vendas B2B - '.$nome;
            }
            
            $telefone  = $this->request->get("telefone");
            if(!$telefone){ $telefone = '-'; }
            $email = $this->request->get("email");
            if(!$email){ $email = '-'; }
            $assunto = $this->request->get("assunto");
            $mensagem = nl2br($this->request->get("mensagem"));

            $opts['to'] = 'geral@omeucolchao.pt';
            $opts['from_email'] = $email;
            $opts['from_phone'] = $telefone;
            $opts['message'] = $mensagem;

            $result = $this->send_email('email_contact',$opts);


            $logger->debug('ticketEmailAction result: ' . json_encode($result));


            if( isset($result['success']) && $result['success'] === true ){
                return new JsonResponse(['status'=>true]);
            }

            return new JsonResponse(['status'=>false]);

    }
    
}

?>
