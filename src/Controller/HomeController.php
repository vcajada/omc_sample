<?php

namespace App\Controller;

use \App\Controller\BaseController;
use \App\Service\Articles;
use \App\Service\Slides;

class HomeController extends BaseController
{

    public function indexStart(Articles $article, Slides $slides)
    {
        $tops = $article->getHomepageTops();
        for($i = 0; $i < count($tops); $i++) {
            $tops[$i]['characteristics'] = $article->getCharacteristics($tops[$i], true);
        }

        $slide_desktop = $slides->getSlideDesktop();
        $slide_mobile = $slides->getSlideMobile();

        $contents = [
            'tops'      => $tops,
            'HomeSelected' => 'selected',
            'slideDesktop' => $slide_desktop,
            'slideMobile' => $slide_mobile
        ];

        return $this->getRenderedContent('home/index.html.twig', $contents);
    }
}
