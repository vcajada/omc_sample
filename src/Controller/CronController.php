<?php

namespace App\Controller;

use \App\Controller\BaseController;
use \App\Service\Carts;
use \App\Service\Orders;
use \App\Service\Payments;
 
class CronController extends BaseBoController {

    public function ordersExpire(Orders $orders, Carts $carts) {
        
        $expire = $orders->getListtoExpire();
        
        foreach($expire as $item){
            $cartModel = $carts->getCartModelfromId($item->id);
            $cartModel->articles->load('option','option.article','option.article.images');
            $user = $cartModel->user;
            
            $contents = array(
                             'to' => array($user->email => $user->name),
                             'username' => $user->name,
                             'cart'     => $cartModel
                         );

            $result = $this->send_email('order_expired', $contents);
            
            $cartModel->status = 'EXPIRED';
            $cartModel->save();
            
            $orders->releaseStock($item->id);
            
            $this->logger->debug("Sending order_expired email of cart {$item->id} to {$user->name} with the email {$user->email}") ;
            $this->logger->debug("Setting cart {$item->id} to EXPIRED") ;
        }
        
        return true;

    }
        
    public function cartReminder(Carts $carts) {
        
        $abandoned = $carts->getAbandonedList();
        
        foreach($abandoned as $remind){
            $cartModel = $carts->getCartModelfromId($remind->id);
            $cartModel->articles->load('option','option.article','option.article.images');
            $user = $cartModel->user;
            
            $contents = array(
                             'to' => array($user->email => $user->name),
                             'username' => $user->name,
                             'cart'     => $cartModel
                         );

            $result = $this->send_email('cart_reminder', $contents);
            
            $cartModel->abandonment_notify = '1';
            $cartModel->save();
            
            $this->logger->debug("Sending cart_reminder email of cart {$remind->id} to {$user->name} with the email {$user->email}") ;
        }
        
        return true;
        
    }

    public function mbReminder(Orders $order, Carts $carts, Payments $payment) {
        
        $pending = $orders->getPendingMB();
        
        foreach($pending as $remind){
            $cartModel = $carts->getCartModelfromId($remind->id);
            $cartModel->articles->load('option','option.article','option.article.images');
            $user = $cartModel->user;
            $paydata = $payment->getMBData($remind->cart_hash);
            
            $contents = array(
                             'to' => array($user->email => $user->name),
                             'username' => $user->name,
                             'cart'     => $cartModel,
                             'paydata'  => $paydata
                         );

            $result = $this->send_email('mb_reminder', $contents);
            
            $cartModel->mb_reminder = '1';
            $cartModel->save();
            
            $this->logger->debug("Sending mb_reminder email of cart {$remind->id} to {$user->name} with the email {$user->email}") ;
        }
        
        return true;
        
        }
        
    public function autoExport() {
        
        $this->logger->debug("[auto_export] BEGIN Processing =============================================") ;
        // FTP DATA
        $ftp_server = $this->getParameter('ftpdata.server');
        $ftp_user_name = $this->getParameter('ftpdata.username');
        $ftp_user_pass = $this->getParameter('ftpdata.pass');
            
        // START FTP SESSION
        // Connect to FTP Server
        if (!$conn_id = ftp_connect($ftp_server)) {
            $this->logger->debug("[auto_export] Cannot connect to ftp server") ;
            die();
        }
        // Login to FTP Server
        if (!$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)) {
            $this->logger->debug("[auto_export] Cannot login to ftp server") ;
            die();
        }

        // Enable passive mode
        ftp_pasv($conn_id, true);
        
        $orders = $this->getOrders();
        $cart_list = $orders->getCartExport();
        
        $this->logger->debug("[auto_export] Exporting a total of ".count($cart_list)." carts") ;
        
        foreach($cart_list as $cart){
            
            $this->logger->debug("[auto_export] Processing... [cart_id={$cart->id}]") ;
            
            $order_list = $orders->getautoExport($cart->id);
            
            if(count($order_list) === 0){
                $this->logger->debug("[auto_export] cart has no exportable orders. Moving to next cart.") ;
                $exported = $orders->setCartAsExported($cart->id);
                continue;
            }

            $this->logger->debug("[auto_export] cart contains a total of ".count($order_list)." exportable orders") ;
            
            $this->logger->debug("[auto_export] Building csv file for [cart_id={$cart->id}]") ;


            $file = \App\Service\Cigest::generateExportFile($order_list);
            $filename = '49_'. date('Y-m-d').'@'.date('H.i.s') . '_OMC_'.$cart->id.'.csv';

            foreach($order_list as $order){
                $validated = $orders->setOrderValidated($order->item_id);
            }
            
            $output = fopen($file, 'r+');
            $this->logger->debug("[auto_export] Finished the build of {$file} for [cart_id={$cart->id}]") ;

            $this->logger->debug("[auto_export] Exporting {$file} to FTP") ;

            // upload the file
            if (ftp_fput($conn_id, $filename, $output, FTP_ASCII)) {
                $this->logger->debug("[auto_export] Finished export of {$file} to FTP") ;
            } else {
                $this->logger->debug("[auto_export] An error ocurred while exporting {$file} to FTP") ;
            }
            
            $exported = $orders->setCartAsExported($cart->id);
            unlink($file);
        }
        // close the connection
        ftp_close($conn_id);
        //ENDED FTP SESSION
        $this->logger->debug("[auto_export] No more carts to export") ;

        return true;
        
    }
    
}

?>
