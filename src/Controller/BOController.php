<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Service\Orders;
use App\Service\Products;
use App\Service\Promocodes;

class BOController extends BaseBoController implements EnsureBoAccessController
{

    public function backOfficeAction(Orders $orders, Products $products)
    {

        $user = $this->getUsers();

        $member = $this->getMembers();

        $total_orders = $orders->getTotal();
        $total_billed = $orders->totalBilled();
        $total_devolution = $orders->totalDevDiscounts();
        $total_members = $member->getTotal();
        $last_orders = $orders->getLast();
        $last_products = $products->getLast();
        $last7months = $orders->getMonths();
        $first = $orders->getDates("-7 months");
        $current = $orders->getDates("-1 months");
        //$monthly_sales = $orders->getMonthlySales();
        //$monthly_ships = $orders->getMonthlyShips();

        $monthly_costs = $orders->getMonthlyCosts();

        $chartdata = $orders->getChartData();

        for ($i=0; $i < count($chartdata); $i++) {
            $chartdata[$i]['y']     = $last7months[$i];
            $chartdata[$i]['costs'] = isset($monthly_costs[$i]) ? $monthly_costs[$i] : 0;
        }

        $chartdata2 = array();
        $margin_perc = 0;
        foreach (array_keys($last7months) as $key) {
            //criei este if porque quando o monthly_sales[key] é 0, dá erro de divisão por 0
            /* monthly sales is not defined... vlopes
            if ($monthly_sales[$key]!=0) {
                $margin_perc = number_format((($monthly_sales[$key] - $monthly_costs[$key]) / $monthly_sales[$key]) * 100, 1) * 100;
            }*/

            $chartdata2[] = array(
            'y' => $last7months[$key],
            'costs' => isset($monthly_costs[$key]) ? $monthly_costs[$key] : 0,
            'sales' => isset($chartdata[$key]) ? $chartdata[$key]['prods'] : 0,
            'margin' => (isset($chartdata[$key]) ? $chartdata[$key]['prods'] : 0) - (isset($monthly_costs[$key]) ? $monthly_costs[$key] : 0),
            'margin_perc' => $margin_perc
            );
        }

        $contents = [
            'DashActive'      => 'active',
            'totalorders'     => $total_orders,
            'totalbilled'     => number_format((float)$total_billed, 2, '.', ''),
            'totalmembers'    => $total_members,
            'totaldevolution' => $total_devolution,
            'lastorders'      => $last_orders->toArray(),
            'lastproducts'    => $last_products->toArray(),
            'from'            => $first,
            'to'              => $current,
            'lastmonths'      => $last7months,
            'sales'           => [], //$monthly_sales,
            'ships'           => [], //$monthly_ships,
            'chart'           => $chartdata,
            'chart2'          => $chartdata2
        ];

        return $this->getRenderedContent('bo_dashboard.twig', $contents);
    }

    public function backOfficeMembers()
    {
        $user = $this->getUsers();
        $member = $this->getMembers();
        $mbrid = $this->request->get("member_id");
        $mbrname = $this->request->get("member_name");
        $members = $member->getMemberList($mbrid, $mbrname);

        $contents = [
            'MemberActive' => 'active',
            'members'   => $members->toArray(),
            'mbrid'     => $mbrid,
            'mbrname'   => $mbrname
            ];

        return $this->getRenderedContent('bo_members.twig', $contents);
    }

    public function backOfficeSlideShow()
    {
        $slideShow = $this->getSlides();
        $slides = $slideShow->getSlideShow();

            $contents = [
            'SlideActive' => 'active',
            'slides' => $slides
            ];

        return $this->getRenderedContent('bo_slides.twig', $contents);
    }

    public function backOfficeSlideAdd()
    {
        $filename = $this->request->files->get("slideAdd");
         $link = $this->request->get("slideLink");
         $type = $this->request->get("tipo");
         $locale = $this->request->get("locale");

        $slideShow = $this->getSlides();
        $slideShow->addSlide($filename, $link, $type, $locale);
        $slides = $slideShow->getSlideShow();

        $contents = [
        'SlideActive' => 'active',
        'slides' => $slides
        ];

        return $this->redirectToRoute('backoffice-slideshow');

    }

    public function backOfficeSlideRemove()
    {

        $id = $this->request->get("id");
        $slideShow = $this->getSlides();

        $slideShow->removeSlide($id);
        $slides = $slideShow->getSlideShow();

            $contents = [
            'SlideActive' => 'active',
            'slides' => $slides
            ];

            return $this->redirectToRoute('backoffice-slideshow');

    }



    public function backOfficeGetMember($member_id)
    {
        $user = $this->getUsers();
        $member = $this->getMembers();
        $member_data = $member->getMemberData($member_id);
        $member_prefs = $member->getMemberPrefs($member_id);
        $member_address = $member->getMemberAddr($member_id);
        $member_orders = $member->getMemberOrders($member_id);
        $member_promos = $member->getMemberPromos($member_id);
        //   $member_log = $member->getMemberLog($member_id);

        $contents = [
            'MemberActive' => 'active',
            'member'    => $member_data,
            'prefs'     => $member_prefs,
            'address'   => $member_address,
            'orders'    => $member_orders,
            'promos'    => $member_promos
            ];

        return $this->getRenderedContent('bo_edit_member.twig', $contents);
    }

    public function backOfficeSales(Orders $orders)
    {
        $user = $this->getUsers();
        $cartid = $this->request->get("cartid");
        $orderid = $this->request->get("orderid");
        $prodid = $this->request->get("prodid");
        $mbrid = $this->request->get("mbrid");
        $email = $this->request->get("email");
        $paytype = $this->request->get("paytype");
        $paystat = $this->request->get("paystat");
        $ordstat = $this->request->get("ordstat");
        $locale = $this->request->get("locale");
        $order_list = $orders->getOrderList($cartid, $orderid, $prodid, $mbrid, $paytype, $paystat, $ordstat, $email, $locale);

        $contents = [
            'SalesActive' => 'active',
            'orders'    => $order_list->toArray(),
            'cartid'    => $cartid,
            'orderid'   => $orderid,
            'prodid'    => $prodid,
            'mbrid'     => $mbrid,
            'email'     => $email,
            'paytype'   => $paytype,
            'paystat'   => $paystat,
            'ordstat'   => $ordstat
            ];

        return $this->getRenderedContent('bo_sales.twig', $contents);
    }

    public function backOfficePromos(Promocodes $promos)
    {
        $user = $this->getUsers();
        //if (!in_array('COMERCIAL', $user->roles($app['session']))) { die(); }

        $code = $this->request->get("code");
        $mbrid = $this->request->get("member_id");
        $type = $this->request->get("type");
        $used = $this->request->get("used");
        $locale = $this->request->get("locale");
        $promo_list = $promos->getPromoList($code, $mbrid, $type, $used, $locale);

        $contents = [
            'PromoActive' => 'active',
            'promos'    => $promo_list,
            'code'      => $code,
            'mbrid'     => $mbrid,
            'type'      => $type,
            'used'      => $used,
            'locale'    => $locale
            ];

        return $this->getRenderedContent('bo_promocodes.twig', $contents);
    }

    public function backOfficeEditPromo(Promocodes $promos, $code)
    {
        $user = $this->getUsers();
        if (!in_array('COMERCIAL', $user->roles($this->session))) {
            die();
        }

        $promo = $promos->loadPromo($code);

        $contents = [
            'PromoActive' => 'active',
            'promo'       => $promo
            ];

        return $this->getRenderedContent('bo_edit_promo.twig', $contents);
    }

    public function backOfficeSavePromo(Promocodes $promos)
    {
        $user = $this->getUsers();
        if (!in_array('COMERCIAL', $user->roles($this->session))) {
            die();
        }

        $id =  $this->request->get("id");
        $code =  $this->request->get("code");
        $limit =  $this->request->get("limit_total");
        $usr_limit =  $this->request->get("user_limit");
        $start =  $this->request->get("start_date");
        $end =  $this->request->get("end_date");
        $type =  $this->request->get("type");
        $value =  $this->request->get("value");
        $userid =  $this->request->get("user_id") ? $this->request->get("user_id") : 0;
        $minbuy =  $this->request->get("min_buy");
        $status =  $this->request->get("status");
        $locale =  $this->request->get("locale");
        $update = $promos->updatePromo($id, $code, $limit, $usr_limit, $start, $end, $type, $value, $userid, $minbuy, $status, $locale);

        if ($update) {
            return new JsonResponse(['status'=>true]);
        } else {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro a atualizar o Promocode']);
        }
    }

    public function backOfficeCreatePromo(Promocodes $promos)
    {
        $user = $this->getUsers();
        if (!in_array('COMERCIAL', $user->roles($this->session))) {
            die();
        }

        $code =  $this->request->get("code");
        $limit =  $this->request->get("limit_total");
        $usr_limit =  $this->request->get("user_limit");
        $start =  $this->request->get("start_date");
        $end =  $this->request->get("end_date");
        $type =  $this->request->get("type");
        $value =  $this->request->get("value");
        $userid =  $this->request->get("user_id") ? $this->request->get("user_id") : 0;
        $minbuy =  $this->request->get("min_buy");
        $status =  $this->request->get("status");
        $locale =  $this->request->get("locale");
        $new = $promos->createPromo($code, $limit, $usr_limit, $start, $end, $type, $value, $userid, $minbuy, $status, $locale);

        if ($new) {
            return new JsonResponse(['status'=>true]);
        } else {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro a atualizar o Promocode']);
        }
    }

    public function backOfficeNewPromo()
    {
        $user = $this->getUsers();
        if (!in_array('COMERCIAL', $user->roles($this->session))) {
            die();
        }

        $contents = [
            'PromoActive' => 'active'
            ];

        return $this->getRenderedContent('bo_new_promo.twig', $contents);
    }


    public function backOfficeSessionLocale()
    {
        $locale = $this->request->get('p_locale');
        $this->session->set('locale', $locale);
        return $this->redirect($this->request->get('redirect_url'));
    }



    public function backOfficeProdutos(Products $products)
    {
        $user = $this->getUsers();
        $parent = $this->request->get("parent_id");
        $prodid = $this->request->get("product_id");
        $stk = $this->request->get("stk");
        $title = $this->request->get("p_title");
        $product_list = $products->getProductList($parent, $prodid, $stk, $title);

        $contents = [
            'ProductActive' => 'active',
            'products'      => $product_list,
            'parent'        => $parent,
            'prodid'        => $prodid,
            'stk'           => $stk,
            'title'         => $title
        ];

        return $this->getRenderedContent('bo_products.twig', $contents);
    }




    public function backOfficeNewProduto()
    {
        $user = $this->getUsers();

        $contents = [
            'ProductActive' => 'active'
            ];

        return $this->getRenderedContent('bo_new_product.twig', $contents);
    }


    public function backOfficeEditProduto(Products $products, $product)
    {
        $user = $this->getUsers();
        $product_data = $products->loadProductId($product);
        $product_options = $products->loadProductOptions($product);
        $product_images = $products->loadProductImages($product);

        $contents = [
            'ProductActive' => 'active',
            'product'       => $product_data,
            'options'       => $product_options,
            'images'        => $product_images,
        ];

        return $this->getRenderedContent('bo_edit_product.twig', $contents);
    }

    public function backOfficeDeleteOption(Products $products)
    {
        $products->deleteOption($this->request->get('option_id'));

        return new JsonResponse(['status' => true]);
    }

    public function backOfficeCreateProduct(Products $products)
    {
        $this->session->set('locale', $this->request->getDefaultLocale());

        $p_type =  $this->request->get("p_type");
        $p_brand =  $this->request->get("p_brand");
        $main_stk =  $this->request->get("main_stk");
        $p_mattress = $this->request->get("p_mattress");
        $p_title =  $this->request->get("p_title");
        $p_ship_mainland = $this->request->get("p_ship_mainland");
        $p_ship_islands = $this->request->get("p_ship_islands");
        $p_expedition_time = $this->request->get("expedition_time");
        $p_is_promo = $this->request->get("p_is_promo");
        $p_is_top_seller = $this->request->get("p_is_top_seller");
        $p_is_ext = $this->request->get("p_is_ext");
        $low_price =  $this->request->get("low_price");
        $original_price = $this->request->get("original_price");
        $discount =  $this->request->get("discount");
        $p_deep =  $this->request->get("p_deep");
        $p_level =  $this->request->get("p_level");
        $p_status = $this->request->get("p_status");
        $p_description =  $this->request->get("p_description");
        $p_conditions =  $this->request->get("p_conditions");
        $p_details =  $this->request->get("p_details");
        $p_stk =  $this->request->get("p_stk");
        $p_height =  $this->request->get("p_height");
        $p_width =  $this->request->get("p_width");
        $p_cost =  $this->request->get("p_cost");
        $p_price =  $this->request->get("p_price");
        $p_original_price = $this->request->get("p_original_price");
        $p_stock =  $this->request->get("p_stock");
        $p_img_main =  $this->request->files->get("p_img_main");
        $p_img_2 =  $this->request->files->get("p_img_2");
        $p_img_3 =  $this->request->files->get("p_img_3");
        $p_img_4 =  $this->request->files->get("p_img_4");
        $p_img_5 =  $this->request->files->get("p_img_5");
        $p_img_6 =  $this->request->files->get("p_img_6");

        $new = $products->createProduct($p_type, $main_stk, $p_mattress, $p_brand, $p_title, $p_ship_mainland, $p_ship_islands, $p_expedition_time, $p_is_promo, $p_is_top_seller, $p_is_ext, $low_price, $original_price, $discount, $p_deep, $p_level, $p_status, $p_description, $p_conditions, $p_details, $p_stk, $p_height, $p_width, $p_cost, $p_price, $p_stock, $p_img_main, $p_img_2, $p_img_3, $p_img_4, $p_img_5, $p_img_6, $p_original_price);

        return new RedirectResponse('/backoffice/produtos/');
        /*if ($new) {
            return new JsonResponse(['status'=>true]);
        } else {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro a inserir o Produto']);
        }*/
    }

    public function backOfficeImportCSV(Products $products)
    {
        $locale = $this->request->request->get('locale');

        $this->session->set('locale', $locale);
        $p_csv =  $this->request->files->get("csv_import");
        $import = $products->importCSV($p_csv);
        $flashbag = $this->session->getFlashBag();

        if (!$import['ok']) {
            $flashbag->add('error', $import['response']);
        } else {
            $flashbag->add('success', $import['response']);
        }

        return new RedirectResponse('/backoffice/produtos/');
        $flashbag->clear();
    }

    public function backOfficeImportZIP(Products $products)
    {
        $p_zip =  $this->request->files->get("zip_import");

        try {
            $import = $products->importZIP($p_zip);
        } catch (\Exception $e) {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro: ' . $e->getMessage() ]);
        }


        if ($import) {
            return new JsonResponse(['status'=>true]);
        } else {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro a carregar o ficheiro']);
        }
    }

    public function backOfficeSaveProduct(Products $products)
    {

        $args = $this->request->request->all();
        $args['p_img_main'] =  $this->request->files->get("p_img_main");
        $args['p_img_2']    =  $this->request->files->get("p_img_2");
        $args['p_img_3']    =  $this->request->files->get("p_img_3");
        $args['p_img_4']    =  $this->request->files->get("p_img_4");
        $args['p_img_5']    =  $this->request->files->get("p_img_5");
        $args['p_img_6']    =  $this->request->files->get("p_img_6");
        $args['p_is_promo'] = !isset($args['p_is_promo']) ? 0 : $args['p_is_promo'];
        $args['p_is_ext'] = !isset($args['p_is_ext']) ? 0 : $args['p_is_ext'];
        $args['p_is_top_seller'] = !isset($args['p_is_top_seller']) ? 0 : $args['p_is_top_seller'];
        $args['p_ship_islands'] = is_numeric($args['p_ship_islands']) ? $args['p_ship_islands'] : 0;
        $args['p_ship_mainland'] = is_numeric($args['p_ship_mainland']) ? $args['p_ship_mainland'] : 0;

        $new = $products->editProduct($args);

        $query_param = 0;
        if ($new) {
            $query_param = 1;
        }

        return new RedirectResponse('/backoffice/produtos/produto/'.$args['p_id'].'?ok='.$query_param);
        /*if ($new) {
            return new JsonResponse(['status'=>true, 'messsage' => '', 'product_id' => $args['p_id']]);
        } else {
            return new JsonResponse(['status'=>false, 'message'=>'Ocorreu um erro a inserir o Produto', 'product_id' => $args['p_id']]);
        }*/
    }


    public function backOfficeAccount(Orders $orders)
    {
        $user = $this->getUsers();


        $cartid = $this->request->get("cartid");
        $orderid = $this->request->get("orderid");
        $prodid = $this->request->get("prodid");
        $dates = $this->request->get("dates");
        $validated = $this->request->get("validated");
        $is_ext = $this->request->get("is_ext");
        $locale = $this->request->get("locale");

        $order_list = $orders->getAccounting(array( 'cartid' => $cartid, 'orderid' => $orderid, 'prodid' => $prodid, 'dates' => $dates, 'validated' => $validated, 'is_ext' => $is_ext, 'locale' => $locale ));

        //Acrescentei a declaração da variável aqui porque, quando não entra no if, dá erro de variável indefinida
        $dates_title = '';
        if ($dates) {
            $dates_title = 'de '.$dates;
        }

        $contents = [
                'AccountActive' => 'active',
                'cartid'        => $cartid,
                'orderid'       => $orderid,
                'prodid'        => $prodid,
                'dates'         => $dates,
                'validated'     => $validated,
                'datetitle'     => $dates_title,
                'orders'        => $order_list->toArray(),
                'is_ext'        => $is_ext
            ];

        return $this->getRenderedContent('bo_accounting.twig', $contents);
    }

    public function backOfficeExport(Orders $orders)
    {
        $cartid = $this->request->get("cartid");
        $orderid = $this->request->get("orderid");
        $prodid = $this->request->get("prodid");
        $dates = $this->request->get("dates");
        $validated = $this->request->get("validated");
        $is_ext = $this->request->get("is_ext");
        $locale = $this->request->get('locale');
        $order_list = $orders->getAccounting(array( 'cartid' => $cartid, 'orderid' => $orderid, 'prodid' => $prodid, 'dates' => $dates, 'validated' => $validated, 'is_ext' => $is_ext, 'locale' => $locale));
        $export_type = $this->request->get("export");

        $formated = str_replace(' ', '_', $dates);
        #$formated = str_replace('/', '-', $formated);

        $filename = 'CIGEST_['.$formated.']_'. date('Y-m-d').'@'.date('h.i.s') . '.csv';

        $file = \App\Service\Cigest::generateExportFile($order_list, $export_type);

        $response = new BinaryFileResponse($file, 200, array(
            'Content-type'          => 'text/csv',
            'Content-Disposition'   => 'attachment; filename='.$filename,
            'Pragma'                => 'no-cache',
            'Expires'               => '0',
        ), true);
        $response->deleteFileAfterSend(true);

        return $response;
    }

    public function backOfficeValidate(Orders $orders)
    {
        $cartid    = $this->request->get("cartid");
        $orderid   = $this->request->get("orderid");
        $prodid    = $this->request->get("prodid");
        $dates     = $this->request->get("dates");
        $is_ext    = $this->request->get("is_ext");
        $validated = $this->request->get("validated");
        $order_list = $orders->getAccounting(array('cartid'=>$cartid, 'orderid' => $orderid, 'prodid' => $prodid, 'dates' => $dates, 'validated' => $validated, 'is_ext' => $is_ext));

        foreach ($order_list as $order) {
            $validated = $orders->setOrderValidated($order->item_id);
        }

        return new JsonResponse(['status'=>true]);
    }

    public function backOfficeExportCSV(Products $products)
    {
        $product_list = $products->getExport();

        //Generate Spreadsheet

        $headers = array(
            'STK MAE',
            'STK OPCAO',
            'MARCA',
            'TIPO DE COLCHAO',
            'TITULO',
            'PVP ORIGINAL',
            'ALTURA',
            'FIRMEZA',
            'TIPO DE ARTIGO',
            'PORTES',
            'PORTES ILHAS',
            'EXPEDICAO',
            'PROMOCAO',
            'TOP SELLER',
            'MERCADORIA',
            'ATIVO',
            'LARGURA',
            'COMPRIMENTO',
            'PRECO',
            'PRECO DE CUSTO',
            'STOCK',
            'DESCRICAO',
            'CONDICOES',
            'CARACTERISTICAS'
        );

        $file = '/tmp/omc_products.csv';
        $filename = 'omc_export.csv';

        $output = fopen($file, "w");

        fwrite($output, chr(239) . chr(187) . chr(191));
        fputcsv($output, $headers, ';', '"');

        foreach ($product_list as $product) {
            $description = $product->description;
            $conditions = $product->conditions;

            $description = str_replace("\n", "", $description);
            $description = str_replace("\r", "", $description);
            $conditions  = str_replace("\n", "", $conditions);
            $conditions  = str_replace("\r", "", $conditions);

            $rows = array(
                $product->main_stk,
                $product->stk,
                $product->brand,
                $product->mattress_type,
                $product->title,
                $product->original_price,
                $product->deep,
                $product->level,
                $product->article_type,
                $product->ship_mainland,
                $product->ship_islands,
                $product->expedition_time,
                $product->is_promo,
                $product->is_top_seller,
                $product->is_ext,
                $product->active,
                $product->width,
                $product->height,
                $product->price,
                $product->cost,
                $product->stock,
                $description,
                $conditions,
                $product->characteristics,
            );

            fputcsv($output, $rows, ';', '"');
        }
        fclose($output);

        return $app->sendFile($file, 200, array('Content-type' => 'text/csv', 'Content-Disposition' => 'attachment; filename='.$filename, 'Pragma' => 'no-cache', 'Expires' => '0'));
    }

    public function backOfficeExportUsers()
    {
        ## SELECT U.email, U.name FROM users U LEFT JOIN user_prefs AS UP ON UP.user_id=U.id  WHERE U.rgpd IS NOT NULL AND UP.newsletter = 1 AND email <>''

        $users = \App\Models\User::whereNotNull('rgpd')
            ->where('email','<>','')
            ->whereHas('user_prefs',function($q){
                $q->where('newsletter','=','1');
            })
            ->get();

        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
        foreach($users as $user){
            $content = [ $user->email, $user->name ];
            fputcsv($csv, $content,';');
        }

        rewind($csv);
        $BOM = "\xEF\xBB\xBF";
        $out =  $BOM . stream_get_contents($csv);

        $response = new Response($out);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'users_'.date('Y-m-d_H:i:s').'.csv'
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }

    public function forgetUser(){

        $user = $this->getClientUsers();
        $log = $this->getLogs();
        $user_id = $request->get('id');
        $usersession = $session->get('omc_user');

        $removed = $user->forgetUser($user_id);

        if ($removed == true){
            $resultlogs = $log->insertLog($usersession['id_user'], 'Removed User');
            return $this->redirectToRoute('backoffice-membros');
        }
    }
}
