<?php

namespace App\Controller;

use \App\Controller\BaseController;
use \App\Service\Carts;

class GoogleMerchantController extends BaseController
{
    private function translateWindows1252($str)
    {
        $str = strtr($str, array(
            "\x80" => '&euro;',
            "\x94" => '&rdquo;',
            "Ô" => '&rdquo;',
            "\x96" => '&ndash;',
            "Ö" => '&ndash;',
            "\x99" => '&trade;'
        ));
        return $str;
    }

    public function feedAction($token='')
    {
        $allowed_tokens = [
            'fc8959288ee4bf8183d3be2fe5cf3718efad6d42' => 'googlemerchant',
            'e4bf8183d3be2fe5cf3718efad6d4221a3d1aa02' => 'facebookmerchant'
        ];

        $only_parents = $request->get('only_parents');


        if (empty($token) || !in_array($token, array_keys($allowed_tokens))) {
            throw $this->createNotFoundException('');
        }

        $products = \App\Models\Article::active()->get();

        $results = [];

        foreach ($products as $product) {
            $options = $product->options()->orderBy('price', 'asc')->get();
            $first = true;

            foreach ($options as $option) {

                if($only_parents){
                    $results[] = array(
                        'title' => $product->title . " ",
                        'description' => $product->fields->description,
                        'image_url' => 'https://www.omeucolchao.pt'.$product->mainImage()->image_url, // FIXME
                        'url' => $this->app->url('produtos', array('url_title' => $product->url_title)),
                        'price' => ($option->original_price ? $option->original_price : 0), // Some products have no original price
                        'sale_price' =>  $option->price,
                        'id' => $product->id,
                        'real_id' => $product->id,
                        'type' => ucfirst($product->mattress_type),
                        'google_product_category' => '2696',
                        'brand' => $product->brand,
                        'cheapest_option' => $first ? true :false,
                    );
                    // Only cheapest option
                    break;

                } else {

                    if (!$option) {
                        continue;
                    } // Skip options with no available stock

                    $results[] = array(
                        'title' => $product->title . " " . $option->size,
                        'description' => $product->fields->description,
                        'image_url' => 'https://www.omeucolchao.pt'.$product->mainImage()->image_url, // FIXME
                        'url' => $this->app->url('produtos_with_option', array('url_title' => $product->url_title, 'option_id' => $option->id )),
                        'price' => ($option->original_price ? $option->original_price : 0), // Some products have no original price
                        'sale_price' =>  $option->price,
                        'id' => $option->stk,
                        'real_id' => $option->id,
                        'type' => ucfirst($product->mattress_type),
                        'google_product_category' => '2696',
                        'brand' => $product->brand,
                        'cheapest_option' => $first ? true :false,
                    );
                    $first = false;
                };
            }
        }

        //die(dump($results));

        $contents = [
            'pubDate' => date('c'),
            'products' => $results
        ];

        return new Response(
            $this->render(
                'google_merchant.xml.twig',
                $contents
            ),
            200,
            array('Content-Type' => 'application/xml')
        );
    }
}
