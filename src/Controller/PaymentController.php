<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

use \App\Controller\BaseController;
use \App\Service\Payments\Dps;
use \App\Service\Payments;
use \App\Service\Carts;
use \App\Service\Users;
use Psr\Log\LoggerInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PaymentController extends BaseController {

    public function payFREE(Payments $payment, Carts $carts) {

        $freesession = $this->session->get('order-payments-free');

        if (empty($freesession)) {
                return $this->redirectToRoute('homepage');
        }

        $cartsession = $this->session->get('omc_cart');

        if($cartsession){

            $updateCart = $payment->setPaidCart($cartsession);
            $updateOrders = $payment->processOrders($cartsession);

            if($updateCart){

                $contents = [
                    'success' => $this->translator->trans('A sua encomenda foi processada com sucesso')
                ];

                $cart = $carts->getCartModelfromSession($cartsession);
                $analytics = new \App\Service\Analytics( array('property_id' => $this->getParameter('gtm.property_id')) );
                $this->send_payment_received_email($carts, $cart->id);
                $analytics->trackPurchase( array( 'cart' => $cart ) );


            }else{
                $contents = [
                    'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento')
                ];
            }

        }else{
            $contents = [
                'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento')
            ];
        }

        $this->clearShoppingCart();

        return $this->getRenderedContent('payments/pay_free.twig', $contents);
    }

    public function payMbDps(Users $user, Carts $cart, Dps $dps, LoggerInterface $logger) {

        // Verify if payment type in session is right -- SESSIONWEIRD
        $mbsession = $this->session->get('order-payments-mb');

        if (empty($mbsession)) {
                return $this->redirectToRoute('homepage');
        }

        // Get user & cart info -- SESSIONWEIRD
        $cart_hash_session = $this->session->get('omc_cart');
        $usersession = $this->session->get('omc_user');
        $userid = $usersession['id_user'];

        // Check if we have a cart in session -- SESSIONWEIRD
        if(!$cart_hash_session){
            return $this->renderErrorMbDps();
        }

        // Get user cart content
        $cart->loadByHash($cart_hash_session);

        // Check if we already have references for this order
        if($cart->payment_reference){
            // If yes return success screen with existing reference
            if($cart->payment_total == $cart_total){
                $pay_data = [
                    'error' => false,
                    'entity' => $cart->payment_entity,
                    'reference' => $cart->payment_reference,
                    'amount'    => $cart->payment_total
                ];

                return $this->renderSuccessMbDps($user, $cart_hash_session, $pay_data, $cart);
            }

            $this->logger->warn('Cart total ammount differs from MB details, regenerating | cart id: '.$cart->id.' , user id: '.$user->id);
        }

        // Prepare DPS object for reference gen
        $dps->reference($cart->id);

        $dps->amount($cart->getTotal());

        $user = $user->getByID( $userid );

        $dps->user('phone_number', $user->phone);
        $dps->user('email', $user->email);
        $dps->user('id', $user->id);

        // Generate references
        $pay_data = $dps->getMbReferences();

        // Save into db
        $cart->updatePaymentReference($pay_data['reference']);

        // Check if something went wrong
        if (isset($pay_data['error']) && $pay_data['error'] === true) {
            $message = '';
            if(isset($pay_data['response']->result->description)){
                $message = $pay_data['response']->result->description;
            }
            return $this->renderErrorMbDps($message);
        }

        return $this->renderSuccessMbDps($user, $cart_hash_session, $pay_data, $cart);
    }

    public function callbackDps(Carts $cart, Dps $dps)
    {

        $this->logger->info('UI!');
        $store_name = $this->getParameter('payments.dps.store_name');

        if($this->request->getContentType() != 'json'){
            $this->logger->warn("Content-type not json, ignored.");
            return new Response('ok', 200);
        }

        $data = json_decode($this->request->getContent());

        if(!is_object($data) || !isset($data->encryptedBody)){
            $this->logger->warn("Bad content, ignored.");
            return new Response('ok', 200);
        }


        $params = [
            'encryptedBody' => $data->encryptedBody,
            'initializationVector' => $this->request->headers->get('x-initialization-vector'),
            'authenticationTag' => $this->request->headers->get('x-authentication-tag')
        ];

        $this->logger->info("Callback Dps returned params: " . var_export($params, true));

        $result = $dps->processResponse($params);

        $process_result = null; // Stores the result of genericProcessOrder()

        if (!isset($result['ok']) || !isset($result['response']) || !$result['ok']) {

            $this->logger->warn("This notification was ignored:" . var_export($process_result, true));

            return new Response('ok', 200);
        }

        $response = $result['response'];

        if (!isset($response->type) || !isset($response->payload) || !isset($response->payload->paymentType) || !isset($response->payload->paymentBrand)) {
            $this->logger->debug(json_encode($response));
            if(@$response->type == 'test'){
                $this->logger->info('This is a test, returnin 200');
                return new Response('test ok0', 200);
            }

            $this->logger->error('Missing parameters on request, aborting');
            return new Response('nok1', 400);
        }


        if ($response->type !== 'PAYMENT' ||
                ($response->payload->paymentType !== 'RC' && $response->payload->paymentBrand == 'SIBS_MULTIBANCO') ||
                ($response->payload->paymentType !== 'DB' && $response->payload->paymentBrand == 'MBWAY')
                ) {
            $this->logger->warn('Not processable by us, aborting');
            return new Response('nok2', 200);
        }

        if (!isset($response->payload->customParameters->storeName)||
            ($response->payload->customParameters->storeName !== $store_name)) {
            $this->logger->warn('Sent storeName isn\'t ours. Got ' . $response->payload->customParameters->storeName. ', was expecting ' . $store_name);
            return new Response('nok2', 200);
        }

        $success_regex = '/^(000\.000\.|000\.100\.1|000\.[36])/';
        if (!isset($response->payload->result) || !isset($response->payload->result->code) || !preg_match($success_regex, $response->payload->result->code)) {
            $this->logger->warn('Payload result did not match success regex ' . var_export($response->payload, true));
            return new Response('nok3', 200);
        }

        if (!isset($response->payload->merchantTransactionId)) {
            $this->logger->warn('Response had no merchant transaction id / cart id');
            return new Response('nok3', 200);
        }

        // Set Cart Paid from id
        $cart_id = $response->payload->merchantTransactionId;
        $cart->loadById($cart_id, ['articles']);
        $cart->setPaid();

        // Generate Factura and Send to Client
        $this->send_payment_received_email($cart, $cart->id);

        // Analytics Conversion
        $analytics = new \App\Service\Analytics( array('property_id' => $this->getParameter('gtm.property_id')) );
        $output = $analytics->trackPurchase( array( 'cart' => $cart ) );

        return new Response('ok', 200);
    }

    public function payMbway(Users $user, Carts $cart) {
        // get info from session
        $cart_hash_session = $this->session->get('omc_cart');
        $user_session = $this->session->get('omc_user');

        if(!$cart_hash_session){
            return $this->redirectToRoute('homepage');
        }

        // Load user
        $user_id = $user_session['id_user'];
        $user->loadById($user_id);

        // load user cart
        $cart->loadByHash($cart_hash_session);

        if ($cart->status == 'PAID') {
            return $this->redirectToRoute('duplicated-payment', array('cart_id' => $cart->id));
        }

        $contents = array(
            'metaTitle'        => $this->translator->trans('Pagamento Encomenda'),
            'PageTitle'        => $this->translator->trans('Pagamento Encomenda'),
            'amount'           => $cart->getTotal(),
            'pending'          => false,
            'shopperResultUrl' => $this->generateUrl('process-mbway', array('cart_id' => $cart->id)),
            'phone'            => $user->getUserAddress()['addr_phone'],
        );

        return $this->getRenderedContent('payments/mbway/generic.twig', $contents);
    }

    public function processMbway(Users $user, Carts $cart, Dps $dps, $cart_id)
    {
        // get info from session
        $cart_hash_session = $this->session->get('omc_cart');
        $user_session = $this->session->get('omc_user');

        if(!$cart_hash_session){
            return $this->redirectToRoute('homepage');
        }

        // Load user
        $user_id = $user_session['id_user'];
        $user->loadById($user_id);

        // load user cart
        $cart->loadByHash($cart_hash_session);

        if ($cart->status == 'PAID') {
            return $this->redirectToRoute('duplicated-payment', array('cart_id' => $cart->id));
        }

        $dps->reference($cart->id);

        $amount = $cart->getTotal();
        $dps->amount($amount);

        $dps->user('mbway_phone', $this->request->get('phone'));
        $dps->user('id', $user->id);

        $response_data = $dps->mbwayPayment();

        $this->logger->warn('Process MB Way result> ' . var_export($response_data, true));

        if (!$response_data['ok']) {
            $this->logger->warn('Payment MB Way failed: ' . var_export($response_data['response'], true));

            $this->logger->info('VASCO - ' . var_export($response_data, true));
            $this->logger->info('VASCO - ' . var_export(is_object($response_data['response']), true));
            if (is_object($response_data['response'])) {
                $error = $response_data['response']->result->code;
                $description = $response_data['response']->result->description;
            } else {
                $error = 0;
                $description = '';
            }
            $error =
            $contents = array(
                'metatitle'    => $this->translator->trans('Pagamento Encomenda'),
                'error'        => [
                    $this->translator->trans('Ocorreu um erro no processo de pagamento da sua encomenda.'),
                    $this->translator->trans('Erro: ') . $error,
                    $this->translator->trans('Descrição: ') . $description,
                    $this->translator->trans('Verifique se o número de telemóvel inserido é o mesmo com que se encontra registado na aplicação MB WAY.'),
                    $this->translator->trans('Por favor tente novamente ou com outro método de pagamento.')
                ],
                'amount'           => $amount,
                'shopperResultUrl' => $this->generateUrl('process-mbway', array('cart_id' => $cart->id)),
                'pending'      => false
            );

            return $this->getRenderedContent('payments/mbway/generic.twig', $contents);
        }

        $contents = array(
            'metaTitle'    => 'Aguardando pagamento',
            'PageTitle'    => 'Aguardando pagamento',
            'cartId'       => $cart_id,
            'amount'           => $amount,
            'shopperResultUrl' => $this->generateUrl('process-mbway', array('cart_id' => $cart->id)),
            'pending'      => true
        );
        return $this->getRenderedContent('payments/mbway/generic.twig', $contents);
    }

    public function statusMbway(Carts $cart, $cart_id)
    {
        $cart->loadById($cart_id);

        $user_session = $this->session->get('omc_user');

        if (isset($user_session['id_user']) && ($cart->user_id == $user_session['id_user'])) {
            $estado = $cart->status;
        }

        $data = array(
            'estado' => $estado
        );

        return $this->json($data);
    }

    public function successMbway($cart_id)
    {
        $contents = array(
            'metaTitle' => 'Pagamento Encomenda',
            'PageTitle' => 'Pagamento Encomenda',
            'cartId' => $cart_id
        );

        $this->clearShoppingCart();

        return $this->getRenderedContent('payments/mbway/success.twig', $contents);
    }

    // DEPRECATED -- Now using callbackMbDps
    public function callbackMB(Payments $payment, Carts $cart) {

        $ref = $this->request->get('ref');
        $token = $this->request->get('token');

        if($token){

            // Set Cart Paid from token
            $updateCart = $payment->setPaid($token, $ref);
            $updateOrders = $payment->processOrders($token);

            // Generate Factura and Send to Client
            $this->send_payment_received_email($token);

            // Analytics Conversion
            $current_cart = $cart->getCartModelfromSession($token);

            $analytics = new \App\Service\Analytics( array('property_id' => $this->getParameter('gtm.property_id')) );
            $output = $analytics->trackPurchase( array( 'cart' => $current_cart ) );

            return true;
        }
    }

    private function send_payment_received_email($cart, $cart_id)
    {
        $cart->loadById($cart_id, ['articles.option.article.images', 'addresses', 'user']);
        $date_paid = substr($cart->date_paid, 0, 10);

        //a encomenda será entregue daqui a 15 dias úteis
        $delivery_date = $this->addWorkingDays(strtotime($date_paid), 15);

        // Only send copies if in prod
        if($this->getParameter('kernel.environment') == "prod"){
            $bcc = array('guva@goodlife.pt', 'fpbarbosa@goodlife.pt', 'geral@omeucolchao.pt');
        } else {
            $bcc = array('it@clubefashion.com');
        }


        $email_contents = array(
            'to'   => array($cart->user->email => $cart->user->name),
            'bcc'  => $bcc,
            'name' => $cart->user->name,
            'cart' => $cart->get(),

            //outras informações
            'cart_id'       => $cart->id,
            'delivery_date' => $delivery_date,
            'total_cost'    => $cart->cart_total,
            'addr_street'   => $cart->addresses->addr_street,
            'addr_street2'  => $cart->addresses->addr_street2,
            'addr_cp1'      => $cart->addresses->addr_cp1,
            'addr_cp2'      => $cart->addresses->addr_cp2,
            'addr_city'     => $cart->addresses->addr_city,
            'cart_shipping' => $cart->cart_shipping
        );

        $result = $this->send_email('payment_received', $email_contents);
    }

    private function addWorkingDays($timestamp, $days) {
        $skipdays = array("Saturday", "Sunday");

        $i = 1;
        while ($days >= $i) {
            $timestamp = strtotime("+1 day", $timestamp);
            if ( (in_array(date("l", $timestamp), $skipdays)) )
            {
                $days++;
            }
            $i++;
        }

        return date("Y-m-d",$timestamp);
    }

    public function payCcAdyen(Carts $cart) {

        // get info from session
        $cart_hash_session = $this->session->get('omc_cart');

        if(!$cart_hash_session){
            return $this->redirectToRoute('homepage');
        }

        // load user cart
        $cart->loadByHash($cart_hash_session);

        // format content for display
        $contents = $this->createAdyenContents($cart);

        return $this->getRenderedContent('payments/adyen/generic.twig', $contents);
    }

    public function processCcAdyen(Carts $cart, Users $user, $cart_id)
    {
        // load user cart
        $cart->loadById($cart_id, ['addresses', 'bill_addresses']);

        $user_session = $this->session->get('omc_user');

        // Prevent users from accessing other's orders
        if (!isset($user_session['id_user']) || ($cart->user_id != $user_session['id_user'])) {
            $this->logger->warn('User with ID '.$user_session['user_id'].' tried to pay for cart '.$cart_id);
            return $this->badAuth($cart);
        }

        if ($cart->status == 'PAID') {
            return $this->redirectToRoute('duplicated-payment', array('cart_id' => $cart->id));
        }

        // prepare adyen for payment process
        $cart_total = $cart->getTotal();
        $adyen_init = $this->adyenInit();

        $this->logger->debug('Adyen encrypted data: ' . $this->request->request->get('adyen-encrypted-data'));

        $adyen = new Payments\Adyen($adyen_init);
        $adyen->amount($cart_total*100);
        $adyen->reference($cart_id);
        $adyen->additionalData(array('card.encrypted.json' => $this->request->request->get('adyen-encrypted-data')));

        $user->loadById($user_session['id_user']);

        $billing_address = array(
            'city' => $cart->bill_addresses->addr_city,
            'country' => 'PT',
            'street' => $cart->bill_addresses->addr_street,
            'houseNumberOrName' => '',
            'postalCode' => $cart->bill_addresses->addr_cp1.'-'.$cart->bill_addresses->addr_cp2,
        );
        $adyen->billingAddress($billing_address);

        $delivery_address = array(
            'city' => $cart->addresses->addr_city,
            'country' => 'PT',
            'street' => $cart->addresses->addr_street,
            'houseNumberOrName' => '',
            'postalCode' => $cart->addresses->addr_cp1.'-'.$cart->addresses->addr_cp2,
        );
        $adyen->deliveryAddress($delivery_address);

        $shopper_info = array(
            'email' => $user->email,
            'ip' => $this->request->getClientIp(), // TODO get safer / prettier way to do this
        );
        $adyen->shopperInfo($shopper_info);

        /* For 3D Secure integration */
        $threedsecure = array(
            'acceptHeader' => $this->request->headers->get('Accept'),
            'userAgent' => $this->request->headers->get('User-Agent'),
            );
        $adyen->threeDSecure($threedsecure);

        /* Contact Adyen and validate this payement */
        $result = $adyen->validate();

        return $this->processCcAdyenResult($result, $cart);
    }

    private function processCcAdyenResult($result, $cart)
    {

        // Log result
        $psp_reference = (isset($result['response']) && isset($result['response']->pspReference)) ? $result['response']->pspReference : '';
        $to_log = (array) $result['response'];
        $to_log['cart_id'] = $cart->id_encomenda;
        $to_log['psp_reference'] = $psp_reference;
        $to_log['response'] = json_encode($result['response']);
        $this->logger->info("Process CC Addyen result: " . var_export($to_log, true));

        // Check adyen response for errors
        if (!$result['ok']) {
            $contents = $this->createAdyenContents($cart);
            $contents['error'] = [
                (isset($result['response']->refusalReason)
                    ? $this->translator->trans('A transação foi recusada com a mensagem de erro:').' '.$result['response']->refusalReason
                    : $this->translator->trans('Ocorreu um erro ao processar a transação:').' '.$result['response']->message)
            ];

            return $this->getRenderedContent('payments/adyen/generic.twig', $contents);
        }

        /* Check if we need to redirect the client ( true for 3D Secure ) */
        if ($result['response']->resultCode == Payments\Adyen::REDIRECTSHOPPER) {
            $adyen_response = $result['response'];

            $contents = array(
                'metaTitle' => 'Redireccionamento para 3DSecure',
                'pageTitle' => 'Redireccionamento para 3DSecure',
                'PaReq'     => $adyen_response->paRequest,
                'MD'        => $adyen_response->md,
                'issuerUrl' => $adyen_response->issuerUrl,
                'TermUrl'   => $this->generateUrl('process-cc-adyen-3dsecure', ['cart_id' => $cart->id], UrlGeneratorInterface::ABSOLUTE_URL)
            );

            // Store the relation between md and id_encomenda as md is the only parameter that's returned to us
            $adyenSession = array(
                'id_encomenda' => $cart->id_encomenda,
                'md' => $adyen_response->md
            );
            $this->logger->info('Process CC Adyen result adyen session: ' . var_export($adyenSession, true));


            return $this->getRenderedContent('payments/adyen/3dsecure.twig', $contents);
        }

        // Everything OK set cart paid and clear cart session
        $this->clearShoppingCart();
        $cart->setPaid();
        return $this->redirectToRoute('success-cc-adyen', array('cart_id' => $cart->id));
    }

    public function processCcAdyen3dSecure(Users $user, Carts $cart, $cart_id)
    {
        $adyen_init = $this->adyenInit();
        $adyen = new Payments\Adyen($adyen_init);

        $user_session = $this->session->get('omc_user');
        $user_id = $user_session['id_user'];
        $user->loadById($user_id);

        $shopper_info = array(
            'email' => $user->email,
            'ip' => $this->request->getClientIp(),
            );
        $adyen->shopperInfo($shopper_info);

        /* For 3D Secure integration */
        $threedsecure = array(
            'acceptHeader' => $this->request->headers->get('Accept'),
            'userAgent' => $this->request->headers->get('User-Agent'),
            );
        $adyen->threeDSecure($threedsecure);

        $adyen->md($this->request->request->get('MD'));
        $adyen->paRes($this->request->request->get('PaRes'));

        $adyen_result = $adyen->validate3D();

        $cart->loadById($cart_id);

        return $this->processCcAdyenResult($adyen_result, $cart);
    }

    public function successCcAdyen (Carts $cart, $cart_id)
    {

        $this->send_payment_received_email($cart, $cart_id);

        $contents = [
            'metaTitle' => "Compra OK",
            'pageTitle' => "Compra OK",
         ];


        $analytics = new \App\Service\Analytics( array('property_id' => $this->getParameter('gtm.property_id') ));
        $analytics->trackPurchase( array( 'cart' => $cart ) );

        return $this->getRenderedContent('payments/adyen/success.twig', $contents);
    }

    public function duplicatedPayment($cart_id)
    {
        $contents = array(
            'metaTitle' => 'Pagamento duplicado',
            'pageTitle' => 'Pagamento duplicado',
            'cart_id' => $cart_id
        );

        return $this->getRenderedContent('payments/duplicated.twig', $contents);
    }

    private function createAdyenContents($cart)
    {
        return [
            'metaTitle' => 'Pagamento Encomenda',
            'pageTitle' => 'Pagamento Encomenda',
            'orderId' => $cart->id,
            'amount' => $cart->getTotal(),
            'library_url' => $this->getParameter('payments.adyen.library_url'),
            'generationTime' => (new \DateTime('NOW'))->format('Y-m-d\TH:i:s\Z'),
            'payment_handler' => $this->generateUrl('process-cc-adyen', array('cart_id' => $cart->id)),
         ];
    }

    private function adyenInit()
    {
        return array(
            'url' => $this->getParameter('payments.adyen.payment_url'),
            'url3DSecure' => $this->getParameter('payments.adyen.payment_url_3dsecure'),
            'username' => $this->getParameter('payments.adyen.username'),
            'password' => $this->getParameter('payments.adyen.password'),
            'merchantAccount' => $this->getParameter('payments.adyen.merchant_account'),
            'checkoutSetupUrl' => $this->getParameter('payments.adyen.checkout.setup_url'),
            'checkoutVerifyUrl' => $this->getParameter('payments.adyen.checkout.verify_url'),
            'checkoutUsername' => $this->getParameter('payments.adyen.checkout.username'),
            'checkoutPassword' => $this->getParameter('payments.adyen.checkout.password'),
            'checkoutApiKey' =>  $this->getParameter('payments.adyen.checkout.api_key'),

        );
    }

    // Auxiliary function for payMbDps
    private function renderErrorMbDps($message = null) {

        $error_contents = ['error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento') . ($message ? ' - ' . $message : null)];

        return $this->getRenderedContent('payments/dps/generic.twig', $error_contents);
    }

    // Auxiliary function for payMbDps
    private function renderSuccessMbDps($user, $cart_hash, $pay_data, $cart) {

        // Send confirmation email
        // Only send copies if in prod
        if($this->getParameter('kernel.environment') == "prod"){
            $bcc = array( 'guva@goodlife.pt', 'fpbarbosa@goodlife.pt', 'geral@omeucolchao.pt');
        } else {
            $bcc = array('it@clubefashion.com');
        }

        // Instantiate cart to list cart articles in email
        $cart->loadByHash($cart_hash, ['articles.option.article.images']);
        $cart_array = $cart->get();

        $email_contents = array(
            'to' => array($user->email => $user->name),
            'bcc' => $bcc,
            'name' => $user->name,
            'paydata' => $pay_data,
            'cart' => $cart_array,
            'cart_shipping' => $cart_array['cart_shipping']
        );

        $result = $this->send_email('mb_payment_details', $email_contents);

        $this->clearShoppingCart();

        $contents = ['paydata' => $pay_data];

        return $this->getRenderedContent('payments/dps/generic.twig', $contents);
    }

    private function clearShoppingCart() {

        $this->session->remove('order-payments-mb');
        $this->session->remove('omc_cart');
    }


    // DEPRECATED -- using payCcAdyen
    public function payCC() {

        $cart_session = $this->session->get('omc_cart');
        $payment = $this->getPayments();
        $cart = $payment->getCartfromSession($cart_session);
        $total = \App\Service\Payments::getTotal($cart);

        if(!$cart_session){
            return $this->redirect('/');
        }

        $contents = [
            'metatitle' => "Pagamento Encomenda",
            'popup_url' => '/netcaixa-popup/'
         ];

        /*
        $analytics = new \OMC\Analytics;
        $analytics->trackOrder(
            array('type' => 'CC', 'total' => $total)
        );
         */

        return $this->getRenderedContent('pay_cc.twig', $contents);
    }

    // DEPRECATED
    public function paySuccess(Carts $carts, $cart_id) {

        $cart = $carts->getCartModelfromID($cart_id);

        $contents = [
            'metatitle' => "Compra OK",
            'total' => $cart->cart_total
         ];

        $analytics = new \App\Service\Analytics( array('property_id' => $this->getParameter('gtm.property_id')) );
        $analytics->trackPurchase( array( 'cart' => $cart ) );

        return $this->getRenderedContent('netcaixa/pay_success.twig', $contents);

    }

    // DEPRECATED
    public function cleanAmount($amount) {
        $out = str_replace(",",".",$amount);
        $out = sprintf("%.2f",$out);
        $out = str_replace(".",",",$out);

        return $out;
    }

    // DEPRECATED
    public function netcaixaPopup(Payments $payment) {

        $cartsession = $this->session->get('omc_cart');

        if($cartsession){

            $pay_data = $payment->getCCData($cartsession);

            $payment = self::getPayments();
            $pay_data = $payment->getCCData($cart_session);

            if($pay_data){

                $A030 = 'H3D0'; // Código Mensagem;
                $A001 = $this->getParameter('netcaixa.tpaNumber'); // Número do TPA Virtual
                $C007 = '1'.self::leftPad($pay_data['cartid'], 5); // Referência Pagamento
                $A105 = '9782'; // Moeda Operacao
                $A061 = self::cleanAmount($pay_data['amount']); // Amount uses comma as a separator
                $C046 = $this->getParameter('netcaixa.css'); // ficheiro CSS, opcional
                $C012 = $this->getParameter('netcaixa.callback'); // callback url
                $C013 = ''; //Elemento Segurança

                $contents = [
                    'A030' => $A030,
                    'A001' => $A001,
                    'C007' => $C007,
                    'A105' => $A105,
                    'A061' => $A061,
                    'C046' => $C046,
                    'C012' => $C012,
                    'C013' => $C013,
                    'form_action' => $this->getParameter('netcaixa.urlForm03')
                ];

                $confirmation = self::genForm03Confirmation($contents);

                $contents['C013'] = $confirmation;

            }else{
                $contents = [
                    'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento')
                ];
            }

        }

        return $this->getRenderedContent('netcaixa/form03.twig', $contents);

    }

    // DEPRECATED
    public function genForm03Confirmation($input)
    {
        $CSupervisor = $this->getParameter('netcaixa.CSupervisor');

        $input = array_merge(
            array(
                'A030' => '',
                'A001' => '',
                'C007' => '',
                'A105' => '',
                'A061' => '',
                'C046' => '',
                'C012' => '',
                'A077' => '',
                'A078' => '',
                ), $input
        );

        // Make sure amount uses comma as a separator
        $input['A061'] = self::cleanAmount($input['A061']);

        $confirmation[] = $input['A030'];
        $confirmation[] = ($input['A001'] ? self::leftPad($input['A001'], 9) : '');
        $confirmation[] = ($input['C007'] ? self::leftPad($input['C007'], 15) : '');
        $confirmation[] = ($input['A105'] ? self::leftPad($input['A105'], 4) : '');
        $confirmation[] = ($input['A061'] ? self::leftPad(str_replace(",", "", $input['A061']), 8) : '');
        $confirmation[] = $input['C046'];
        $confirmation[] = $input['C012'];

        if (isset($input['C003']) && isset($input['C004'])) {
            $confirmation[] = $input['C003'];
            $confirmation[] = self::leftPad($input['C004'], 6);
        }

        $confirmation[] = (isset($input['C016']) ? self::leftPad($input['C016'], 2) : '');
        $confirmation[] = $input['A077'];
        $confirmation[] = $input['A078'];

        $check = implode('', $confirmation);

        $hash = strtoupper(hash_hmac("sha1", $check, $CSupervisor));

        return $hash;
    }

    // DEPRECATED
    private function leftPad($string, $num)
    {
        return str_pad($string, $num, '0', STR_PAD_LEFT);
    }

    // DEPRECATED
    public function netcaixaCallback(Carts $carts, Payments $payment)
    {
        $req = $_REQUEST;

        $cartid = substr($req['C007'], 1);

        $order = $carts->getCartfromID($cartid);

        $confirmation = self::genForm03Confirmation($req);

        if ($confirmation != $req["C013"]) {
            return self::bad_auth();
        }


        $authorizedAmount = (isset($req['A061']) ? $req['A061'] : '');
        $total = ($order->cart_total + $order->cart_shipping - $order->cart_discount);

        // Clean up
        $authorizedAmount = self::cleanAmount($authorizedAmount);
        $total = self::cleanAmount($total);

        if ($req["C016"] != 2 or $authorizedAmount != $total) {

            $error = var_export($req, true);
            if ($req['A077'] == 'Disabled for 3DS') {

                return self::bad_auth3DS();
            }

            return self::bad_auth();
        }

        $m002_request = array(
                'A030' => 'M002',
                'A001' => $req['A001'],
                'C007' => $req['C007'],
                'A061' => $req['A061'],
                'A105' => $req['A105'],
                'A037' => ''
            );
        $m002_request['C013'] = self::genM00xConfirmation($m002_request);

        $retry_counter = 0;

        $debug_obj = array( 'M002_request' => array(), 'M002_response' => array () );
        while ($retry_counter < 5) {
            $retry_counter++;

            $response = self::localCurlPost($m002_request);

            $xml = simplexml_load_string($response->response);
            array_push($debug_obj['M002_request'], $m002_request);
            array_push($debug_obj['M002_response'], $xml);

            // Break the loop when we have a positive response
            if (isset($xml) && isset($xml->A038) && $xml->A038 == "0") {
                break;
            }
            sleep(1);
        }

        if (empty($xml) or $xml->A038 > 0 ) {
            $contents = [ 'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento' )];
            return $this->getRenderedContent('netcaixa/form03.twig', $contents);
        }

        // Set Cart Paid from token
        $updateCart = $payment->setPaidCart($order->cart_hash);
        $updateOrders = $payment->processOrders($order->cart_hash);

        $this->send_payment_received_email($carts, $order->cart_hash);


        // Clear the shopping cart
        $this->session->remove('omc_cart');

        // Analytics Conversion

        return $this->redirectToRoute('compra-ok',array('cart_id' =>  $cartid));

    }

    // DEPRECATED
    private function genM00xConfirmation($input)
    {
        $input['A061'] = str_replace(",", "", $input['A061']);

        $message =
            $input['A030'].
            self::leftPad($input['A001'], 9).
            self::leftPad($input['C007'], 15).
            self::leftPad($input['A061'], 8).
            self::leftPad($input['A105'], 4);

        $hash = strtoupper(sha1($message));
        return $hash;
    }

    // DEPRECATED
    private function localCurlPost($fields)
    {
        $urlGest = $this->getParameter('netcaixa.urlGest');
        $urlCertific = $this->getParameter('netcaixa.urlCertific');
        $certificPassw = $this->getParameter('netcaixa.certificPassw');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlGest);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, "PEM");
        curl_setopt($ch, CURLOPT_SSLCERT, $urlCertific);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $certificPassw);

        $curlResp = curl_exec($ch);
        $curlError = curl_error($ch);

        curl_close($ch);

        return (object)array('response' => $curlResp, 'error' => $curlError);
    }

    // DEPRECATED
    private function bad_auth()
    {
        die('badauth called');
        $contents = $this->createAdyenContents($cart);
        $contents['metaTitle']     = "Pagamento Encomenda";
        $contents['pageTitle']     = "Pagamento Encomenda";
        $contents['error'] = array(
            'A autorização de pagamento foi recusada.',
            'Por favor tente novamente ou opte por pagamento através de referências MultiBanco.'
        );

        return $this->getRenderedContent('netcaixa/generic.twig', $contents);
    }

    // DEPRECATED
    private function badAuth3Ds($cart)
    {
        die('badauth3ds');
        $contents = $this->createAdyenContents($cart);
        $contents['metaTitle'] = "Pagamento Encomenda";
        $contents['pageTitle'] = "Pagamento Encomenda";
        $contents['message']   = array(
            'Para segurança acrescida dos nossos clientes, os pagamentos em omeucolchao.pt exigem a utilização do sistema 3D Secure.',
            'Queira por favor contactar o seu banco para ativar este serviço.',
            'No imediato, pode completar o pagamento através da rede Multibanco.'
        );

        return $this->getRenderedContent('payments/adyen/generic.twig', $contents);
    }

    // DEPRECATED now using payMbDps
    public function payMB() {

        $mbsession = $this->session->get('order-payments-mb');

        if (empty($mbsession)) {
                return $this->redirect('/');
        }

        $cart_session = $this->session->get('omc_cart');
        $usersession = $this->session->get('omc_user');
        $userid = $usersession['id_user'];

        if($cart_session){

            $payment = $this->getPayments();
            $pay_data = $payment->getMBData($cart_session);
            if($pay_data){
                $cart = $this->getCarts();
                $updatePayData = $cart->updatePayData($cart_session, $pay_data);

                $contents = [
                    'paydata' => $pay_data
                ];


                ## Send email with MB Details

                $cartModel = $this->getCarts()->getCartModelfromSession($cart_session);
                $cartModel->articles->load('option','option.article','option.article.images');

                $user = $this->getUsers()->getByID( $userid );

                // Only send copies if in prod
                if($this->getParameter('kernel.environment') == "prod"){
                    $bcc = array( 'guva@goodlife.pt', 'fpbarbosa@goodlife.pt', 'geral@omeucolchao.pt');
                } else {
                    $bcc = array('it@clubefashion.com');
                }

                $email_contents = array(
                    'to' => array($user->email => $user->name),
                    'bcc' => $bcc,
                    'name' => $user->name,
                    'paydata' => $pay_data,
                    'cart' => $cartModel,
                    'cart_shipping' => $cartModel->cart_shipping
                );

                $result = $this->send_email('mb_payment_details', $email_contents);


            }else{
                $contents = [
                    'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento')
                ];
            }

        }else{
            $contents = [
                'error' => $this->translator->trans('Ocorreu um erro no processamento por favor tente mais tarde ou com outro método de pagamento')
            ];
        }

        /*
        $analytics = new \OMC\Analytics;
        $analytics->trackOrder(
            array('type' => 'MB', 'total' => $pay_data['amount'])
        );
        */

        $this->session->remove('order-payments-mb');
        $this->session->remove('omc_cart');

        return $this->getRenderedContent('pay_mb.twig', $contents);

    }

}

?>
