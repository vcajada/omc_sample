<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use \App\Controller\BaseController;
use \App\Service\Users;
use \App\Service\Carts;
use \App\Service\Logs;
use \App\Service\Payments;
 
class UserController extends BaseController {


    public function userStart(Users $user)
    {
        $usersession = $this->session->get('omc_user');
        
        if(!$usersession){
            return $this->redirectToRoute('homepage');
        }
            
        $user_data = $user->getUserData($usersession['id_user']);
        $user_prefs = $user->getUserPrefs($usersession['id_user']);
        $user_address = $user->getUserAddress($usersession['id_user']);
        $user_carts = $user->getUserCarts($usersession['id_user']);

        $contents = [
            'userdata'      => $user_data,
            'userprefs'     => $user_prefs,
            'useraddress'   => $user_address,
            'usercarts'     => $user_carts
        ];
        
        return $this->getRenderedContent('user.twig', $contents);
        
    }
    
    public function updateDetails(Users $user)
    {
        $usersession = $this->session->get('omc_user');
        $usernome =  $this->request->get("nome");
        $usermorada =  $this->request->get("morada");
        $usermorada2 =  $this->request->get("morada2");
        $usercp1 =  $this->request->get("cp1");
        $usercp2 =  $this->request->get("cp2");
        $userlocal =  $this->request->get("local");
        $userphone =  $this->request->get("phone");
        $usernif =  $this->request->get("nif");
        $user_update = $user->updateDetails($usersession['id_user'], $usernome, $usermorada, $usermorada2, $usercp1, $usercp2, $userlocal, $userphone, $usernif);
        
        if ($user_update['status'] == true) {
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function setPass(Users $user)
    {
        $newpass =  $this->request->get("new-pass");
        $usersession = $this->session->get('omc_user');
        $userpass_update = $user->updatePass($usersession['id_user'], $newpass);
        
        if ($userpass_update['status'] == true) {
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function updateNews(Users $user, Logs $log)
    {
        $news =  $this->request->get("news-freq");
        $usersession = $this->session->get('omc_user');
        $news_update = $user->updateNews($usersession['id_user'], $news);
        
        
       
        
        if ($news_update['status'] == true) {
        $resultlogs = $log->insertLog($usersession['id_user'], 'Newsletter changed to: '.$news);
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function updateSMS(Users $user, Logs $log)
    {
        $sms =  $this->request->get("value");
        $usersession = $this->session->get('omc_user');
        $sms_update = $user->updateSMS($usersession['id_user'], $sms);
        
        if ($sms_update['status'] == true) {
        $resultlogs = $log->insertLog($usersession['id_user'], 'SMS Ads changed to: '.$sms);
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function updatePartners(Users $user, Logs $log)
    {
        $partners =  $this->request->get("value");
        $usersession = $this->session->get('omc_user');
        $partners_update = $user->updatePartners($usersession['id_user'], $partners);
        
        if ($partners_update['status'] == true) {
        $resultlogs = $log->insertLog($usersession['id_user'], 'Partners Ads changed to: '.$partners);
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function orderDetail(Carts $cart, Payments $payment, $order_id)
    {
        $usersession = $this->session->get('omc_user');
        $cartModel = $cart->getCartModelfromId($order_id);
        
        if(!$usersession || ($usersession['id_user'] != $cartModel->user_id) ){
            return $this->redirectToRoute('homepage');
        }

        $cartModel->articles->load('option','option.article','option.article.images');
        $userid = $usersession['id_user'];
    
        $pay_data = '';
        if($cartModel->pay_type == 'PAY-MB'){
            $pay_data = $payment->getMBData($cartModel->cart_hash);
        }

        $contents = [
            'cart'      => $cartModel,
            'paydata'   => $pay_data
        ];

        return $this->getRenderedContent('order_detail.twig', $contents);
    }
    
    public function updateRGPD(Users $user, Logs $log){
        
        $usersession = $this->session->get('omc_user');
        $update = $user->updateRGPD($usersession['id_user']);
        
        $resultlogs = $log->insertLog($usersession['id_user'], 'RGPD Accepted');
        
        if ($update == true){
            return new JsonResponse(['status'=>true]);
        } else {
            return new JsonResponse(['status'=>false]);
        }
       
    }
    
    public function callMe(Users $user)
    {
        $name =  $this->request->get("name");
        $phone =  $this->request->get("phone");
        $usersession = $this->session->get('omc_user');
         
        $contents =
             array(
                'to' => array('geral@omeucolchao.pt' => $this->translator->trans('O Meu Colchão')),
                'name' => $name,
                'phone' => $phone,
                'title'=>$this->translator->trans('Pedido de Contacto')
             );

        $result = $this->send_email('call_me', $contents);
        
        
        if ($result) {
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }  
    
    public function politicaDados()
    {
        $contents =
             array(
              
             );

        return $this->getRenderedContent('politica_dados.twig', $contents);
    }
    
    public function registerNL(Users $user)
    {
        $name =  "";
        $email = $this->request->get("email");
        $password = "Newsletter!2018:".rand()."Rand".rand();
        $register = $user->create($name,$email,$password);
         
      
        if ($register) {
            return new JsonResponse(['status'=>true, 'message'=>$this->translator->trans("A partir de agora vai receber as Newsletter d'O Meu Colchão.") ]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
    
    public function migrarRegisterNL(Users $user)
    {
        $migrar = $user->migrar_Users_NL();
         
      
        if ($migrar) {
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false]);
        }
    }
}

?>
