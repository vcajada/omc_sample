<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use \App\Controller\BaseController;
use \App\Service\Articles;

class SearchController extends BaseController {

    public function searchStart(Articles $article) {

        $widths = $article->getWidths();
        $heights = $article->getHeights();
        $firmnesses = $article->getFirmnesses($this->translator);
        $typesMap = $article->getTypes();

        $contents = [
            'SearchSelected' => 'selected',
            'widths' => $widths,
            'heights' => $heights,
            'firmnesses' => $firmnesses,
            'types' => $typesMap
        ];

        return $this->getRenderedContent('search.twig', $contents);

    }

    public function searchResults(Articles $article) {

        $height =  $this->request->get("height");
        $width =  $this->request->get("width");
        $firmness =  $this->request->get("firmness");
        $budget_type = $this->request->get("budget");
        $deep = $this->request->get("deep");
        $brand = $this->request->get("brand");
        $type = $this->request->get("type");

        $results = $article->getSearchList($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        foreach($results as $result) {
            $result['characteristics'] = $article->getCharacteristics($result, true);
        }

        $depts = $article->getSearchDepts($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $brands = $article->getSearchBrands($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $types = $article->getSearchTypes($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $widths = $article->getWidths();
        $heights = $article->getHeights();
        $firmnesses = $article->getFirmnesses($this->translator);
        $typesMap = $article->getTypes();
        
        $contents = [
            'blocktitle' => 'Resultados',
            'SearchSelected' => 'selected',
            'results' => $results,
            'widths' => $widths,
            'heights' => $heights,
            'firmnesses' => $firmnesses,
            'typesMap' => $typesMap,
            'types' => $types,
            'depts' => $depts,
            'brands' => $brands,
            'heightParam'   => $height,
            'widthParam'    => $width,
            'firmnessParam' => $firmness,
            'budgetParam'   => $budget_type,
            #'budgetLabel'   => $budget,
            'firmnessParam' => $firmness,
            'deepParam'     => $deep,
            'brandParam'    => $brand,
            'typeParam'     => $type,
            'hideMenu'      => true
        ];

        return $this->getRenderedContent('results.twig', $contents);

    }

    public function searchFilters() {

        $searchParams = array(
            'height' =>  $this->request->get("height"),
            'width' =>  $this->request->get("width"),
            'firmness' =>  $this->request->get("firmness"),
            'deep' => $this->request->get("deep"),
            'brand' => $this->request->get("brand"),
            'type' => $this->request->get("type"),
        );

        //Capsule::enableQueryLog();

        $optionsQuery = \App\Model\ArticleOption::leftJoin('articles','article_options.article_id','=','articles.id');

        $articlesQuery = \App\Model\Article::leftJoin('article_options','article_options.article_id','=','articles.id');


        $heights = $this->filterSearch($optionsQuery,$searchParams,'height');
        $widths = $this->filterSearch($optionsQuery,$searchParams,'width');
        $firmness = $this->filterSearch($articlesQuery,$searchParams,'firmness');
        $deep = $this->filterSearch($articlesQuery,$searchParams,'deep');
        $brand = $this->filterSearch($articlesQuery,$searchParams,'brand');
        $type = $this->filterSearch($articlesQuery,$searchParams,'type');

        //die(var_export(Capsule::getQueryLog(),true));

        $output = array(
            'heights' => $heights,
            'widths' => $widths,
            'firmnesses' => $firmness,
            'deeps' => $deep,
            'brands' => $brand,
            'types' => $type,
        );


        return new JsonResponse($output);
    }

    private function filterSearch($baseQuery,$params,$exception){

        $select = (($exception  == "firmness") ? 'level' : $exception);
        $select = (($select  == "type") ? 'mattress_type' : $select);

        $groupBy=(in_array($select,['height','width']) ? $select : 'articles.'.$select);

        $query = $this->applyFiltersExcept($baseQuery,$params,$exception);
        $out = $query->select($select)->groupBy($groupBy)->get();
        return array_map(function($item) use ($select){ return $item[$select]; }, $out->toArray() );
    }

    private function applyFiltersExcept($searchQuery,$params,$exception){

        $s = clone($searchQuery);

        if($exception !== 'width' && $params['width'] ){
            $s->where('width','=',$params['width']);
        }

        if($exception !== 'height' && $params['height'] ){
            $s->where('height','=',$params['height']);
        }

        if($exception !== 'firmness'  && $params['firmness'] ){
            $s->where('articles.level','=',$params['firmness']);
        }

        if($exception !== 'deep' && $params['deep'] ){
            $s->where('articles.deep','=',$params['deep']);
        }

        if($exception !== 'brand' && $params['brand'] ){
            $s->where('articles.brand','=',$params['brand']);
        }

        if($exception !== 'type'  && $params['type'] ){
            $s->where('articles.mattress_type','=',$params['type']);
        }

        $s->where('width','>',0);
        $s->where('height','>',0);
        $s->where('deep','>',0);
        $s->where('active','=',1);

        return $s;
    }
}

?>
