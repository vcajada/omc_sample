<?php

namespace App\Controller;

use \App\Controller\BaseController;
use \App\Service\Articles;
use \App\Service\Faq;

class ProductController extends BaseController
{
    const sizeopt1 = '180x80';
    const sizeopt2 = '185x140';
    const sizeopt3 = '190x150';
    const sizeopt4 = '200x180';

    public function productStart(Articles $article, Faq $faq, $url_title, $option_id = null)
    {
        $article_obj = $article->getProduct($url_title);
        $article_arr = $article_obj->toArray();
        $article_pretty = $article->prettifyProduct($article_arr[0], $option_id);

        $promotions = $article->getRelatedProducts($article_arr[0]['mattress_type'], $article_arr[0]['level'], $article_pretty['id'] );
        $characteristics = $article->getCharacteristics($article_pretty);
        if ($option_id) {
            foreach ($article_pretty['options'] as $option) {
                if ($option['id'] == $option_id) { $article_pretty['selected_option'] = $option; }
            }
        }

        $contents = [
                'product' => $article_pretty,
                #'stocks' => $stocks,
                #'images' => $images,
                'promotions' => $promotions,
                'faqs' => $faq->getTranslatedFaqs(),
                'characteristics' => $characteristics
            ];

        return $this->getRenderedContent('detail.twig', $contents);
    }

    // DEPRECATED
    public function promoResultsRedirect(){
        return $this->redirectToRoute('oportunidades');
    }

    // DEPRECATED
    public function promoResults(Articles $products)
    {
        $height =  $this->request->get("height");
        $width =  $this->request->get("width");
        $firmness =  $this->request->get("firmness");
        $budget_type = $this->request->get("budget");
        $deep = $this->request->get("deep");
        $brand = $this->request->get("brand");
        $type = $this->request->get("type");

        $results = $products->getPromoResults($height, $width, $firmness, $budget_type, $deep, $brand, $type);

        $stocks = $products->getStock();
        $depts = $products->getSearchDepts($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $brands = $products->getSearchBrands($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $types = $products->getSearchTypes($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $widths = $products->getWidths();
        $heights = $products->getHeights();
        $firmnesses = $products->getFirmnesses($this->translator);
        
        $contents = [
            'PromoSelected' => 'selected',
            'blocktitle' => 'Oportunidades',
            'results' => $results,
            'widths' => $widths,
            'heights' => $heights,
            'firmnesses' => $firmnesses,
            'stocks' => $stocks,
            'depts' => $depts,
            'brands' => $brands,
            'types' => $types,
            'heightParam'   => $height,
            'widthParam'    => $width,
            'firmnessParam'     => $firmness,
            'budgetParam'    => $budget_type,
            #'budgetLabel'   => $budget,
            'firmnessParam' => $firmness,
            'deepParam'     => $deep,
            'brandParam'    => $brand,
            'typeParam'     => $type,
            'promoParam'    => 1
        ];

        return $this->getRenderedContent('results.twig', $contents);
    }

    public function topResults(Articles $products)
    {
        $height =  $this->request->get("height");
        $width =  $this->request->get("width");
        $firmness =  $this->request->get("firmness");
        $budget_type = $this->request->get("budget");
        $deep = $this->request->get("deep");
        $brand = $this->request->get("brand");
        $type = $this->request->get("type");

        $results = $products->getTopResults($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        foreach($results as $result) {
            $result->characteristics = $products->getCharacteristics($result, true);
        }

        $stocks = $products->getStock();
        $depts = $products->getSearchDepts($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $brands = $products->getSearchBrands($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $types = $products->getSearchTypes($height, $width, $firmness, $budget_type, $deep, $brand, $type);
        $widths = $products->getWidths();
        $heights = $products->getHeights();
        $firmnesses = $products->getFirmnesses($this->translator);

        $contents = [
            'TopSelected' => 'selected',
            'blocktitle' => 'Top Sellers',
            'results' => $results,
            'widths' => $widths,
            'heights' => $heights,
            'firmnesses' => $firmnesses,
            'stocks' => $stocks,
            'depts' => $depts,
            'brands' => $brands,
            'types' => $types,
            'heightParam'   => $height,
            'widthParam'    => $width,
            'firmnessParam' => $firmness,
            'budgetParam'   => $budget_type,
            #'budgetLabel'   => $budget,
            'firmnessParam' => $firmness,
            'deepParam'     => $deep,
            'brandParam'    => $brand,
            'typeParam'     => $type,
            'promoParam'    => 1,
            'hideMenu'      => true
        ];

        return $this->getRenderedContent('results.twig', $contents);
    }
}
