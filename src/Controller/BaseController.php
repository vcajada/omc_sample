<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

use \App\Traits\CurrentUserTrait;
use \App\Service\Analytics;
use \App\Service\Utils;
use \App\Service\EmailTree;
use \App\Service\Carts;

class BaseController extends AbstractController
{

    use CurrentUserTrait;

    protected $session;
    protected $logger;
    protected $request;
    protected $translator;

    public function __construct(SessionInterface $session, LoggerInterface $logger, RequestStack $request_stack, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->logger = $logger;
        $this->request = $request_stack->getCurrentRequest();
        $this->translator = $translator;

        if ($sson = $this->request->get('autologin')) {
            $this->autologin($sson);
        }
    }

    protected function getRenderedContent($template, $controllerContents) {

        $description = $this->translator->trans('Omeucolchao.pt é um site especializado em colchões que vai ajudá-lo a encontrar um colchão à sua medida e ao melhor preço. Oferecemos-lhe a melhor relação qualidade preço, à medida das suas necessidades.');

        $cart_session = $this->session->get('omc_cart');

        $env = $this->getParameter('kernel.environment');
        // Header and Footer Contents that will be displayed in every pages
        $current_year = date('Y');
        $current_flag = $this->getCurrentFlag();

        $generalContents = [
                'PageTitle' => $this->getParameter('page_title'),
                'PageDescription' => $description,
                'CurrentYear' => $current_year,
                'dataLayer' => new Analytics(array('property_id' => $this->getParameter('gtm.property_id'))),
                'cacheBust' => $this->getParameter('cache_bust'),
                'render_fullstory' => $env == 'prod' ? true : false,
                'language' => $this->request->getLocale(),
                'telephone' => $this->getParameter('telephone_number'),
                'static_path' => $this->getStaticPath(),
                'current_flag' => $current_flag,
                'cart_article_sum' => Carts::getArticleSum($cart_session),
                'active_campaign'  => $this->getActiveCampaign()
            ];

        // Merge generalContents with controllerContents
        $outputContent = array_merge($generalContents, $controllerContents);

        // Render Twig template response
        return $this->render(
            $template,
            $outputContent
        );

    }

    private function getActiveCampaign() {
        $campaigns = [
            [
                'brand'          => 'FESTIVAL DO SONO',
                'tc'             => '',
                'sticky_desktop' => '<span class="commercial-sticky-bar-main-text" style="color:#ed1c24">Oferta</span> <span class="commercial-sticky-bar-sub-text" style="color: #1c7582">100 noites de experiência, na compra de qualquer colchão. SÓ HOJE!</span>',
                'sticky_mobile'  => '<div class="row" style="font-size:1.2em"><div class="col-md-12" style="color: #1c7582"><span style="color:#ed1c24">Oferta</span> 100 noites de experiência, na compra de qualquer colchão. SÓ HOJE!</div></div>',
                'logo'           => [
                    'display' => 'show',
                    'path'    => '/images/moonfestsleep.png'
                ],
                'stamp_small'    => '/images/xmas18/stamp.png',
                'stamp_big'      => '/images/xmas18/stampbig.png',
                'date_start'     => '03-01-2019 00:00:00',
                'date_stop'      => '03-01-2019 23:59:59',
                'sticky_bg'      => [
                    'type' => 'color',
                    'images' => [
                        'right'   => '/images/xmas18/rightxmas.png?a',
                        'left'    => '/images/xmas18/leftxmas.png?a',
                        'middle'  => '/images/xmas18/middlexmas.png?a',
                    ],
                    'color' => 'fff'
                ],
                'button' => [
                    'display'    => 'none',
                    'text'       => 'Ver Ofertas >',
                    'bg_color'   => 'fff',
                    'text_color' => '8e1c01'
                ]
            ],
            [
                'brand'          => 'PROMOÇÃO NATAL',
                'tc'             => 'Campanha válida para compras dos colchões assinalados de 11/12/18 a 02/01/19. Na compra de qualquer Colchão Visco Baicent Fleur de Lavande 3D® oferta de 20% desconto directo extra e na compra do colchão Paradise Spa (medidas 90x190, 140x190 ou 160x200) oferta imediata de 10% desconto directo extra + Edredon Nórdico Hipoalergénico em branco (nas dimensões 260x240, 240x220 ou 160x220). O edredon poderá ser enviado em separado do colchão.',
                'sticky_desktop' => '<span class="commercial-sticky-bar-sub-text">PRENDAS DE NATAL ATÉ 20% DE</span> <span class="commercial-sticky-bar-main-text">DESCONTO DIRECTO E EXTRA</span>',
                'sticky_mobile'  => '<div class="row" style="font-size:1.2em"><div class="col-md-6">Prendas de natal até 20% de DESCONTO DIRECTO E EXTRA</div><div class="col-md-6"><b>>> VER OFERTAS <<</b></div></div>',
                'logo'           => [
                    'display' => 'none',
                    'path'    => ''
                ],
                'stamp_small'    => '/images/xmas18/stamp.png',
                'stamp_big'      => '/images/xmas18/stampbig.png',
                'date_start'     => '01-12-2018 00:00:00',
                'date_stop'      => '02-01-2019 23:59:59',
                'sticky_bg'      => [
                    'type' => 'images',
                    'images' => [
                        'right'   => '/images/xmas18/rightxmas.png?a',
                        'left'    => '/images/xmas18/leftxmas.png?a',
                        'middle'  => '/images/xmas18/middlexmas.png?a',
                    ],
                    'color' => 'fff'
                ],
                'button' => [
                    'text'       => 'Ver Ofertas >',
                    'bg_color'   => 'fff',
                    'text_color' => '8e1c01'
                ]
            ],
            [
                'brand'          => 'Cyber Week',
                'tc'             => 'Na compra dos colchões: Farma Ortopédico Preto e Visco Baicent Fleur de Lavande 3D (em todas as medidas) oferta de 40% de desconto extra e imediato. Campanha válida de 27/11/18 a 02/12/18. Não acumulável com outras campanhas em vigor.',
                'sticky_desktop' => '<span class="commercial-sticky-bar-sub-text">As Ofertas continuam!</span> <span class="commercial-sticky-bar-main-text">40% EXTRA.</span> <span class="commercial-sticky-bar-sub-text">Aproveite desconto directo nos colchões assinalados até 02/12/12.</span>',
                'sticky_mobile'  => 'Cyber week. As Ofertas continuam: 40% desconto directo extra até 02/12/12.',
                'cta_color'      => '35a8e0',
                'logo'           => '/images/blackfriday/logocw3.png',
                'stamp_small'    => '/images/blackfriday/stampcwsmall.png',
                'stamp_big'      => '/images/blackfriday/stampcwbig.png',
                'date_start'     => '01-12-2018 00:00:00',
                'date_stop'      => '02-12-2018 23:59:59',
                'sticky_bg'      => '0e2b4d'
            ],
            [
                'brand'          => 'Cyber Week',
                'tc'             => 'Na compra dos colchões: Farma Ortopédico Preto e Visco Baicent Fleur de Lavande 3D (em todas as medidas) oferta de 40% de desconto extra e imediato. Campanha válida de 27/11/18 a 30/11/18. Não acumulável com outras campanhas em vigor.',
                'sticky_desktop' => '<span class="commercial-sticky-bar-sub-text">As Ofertas continuam!</span> <span class="commercial-sticky-bar-main-text">40% EXTRA.</span> <span class="commercial-sticky-bar-sub-text">Aproveite desconto directo nos colchões assinalados até 30/11/12.</span>',
                'sticky_mobile'  => 'Cyber week. As Ofertas continuam: 40% desconto directo extra até 30/11/12.',
                'cta_color'      => '35a8e0',
                'logo'           => '/images/blackfriday/logocw3.png',
                'stamp_small'    => '/images/blackfriday/stampcwsmall.png',
                'stamp_big'      => '/images/blackfriday/stampcwbig.png',
                'date_start'     => '27-11-2018 00:00:00',
                'date_stop'      => '30-11-2018 23:59:59',
                'sticky_bg'      => '0e2b4d'
            ],
            [
                'brand'          => 'Cyber Monday',
                'tc'             => 'Na compra dos colchões: Farma Ortopédico Preto e Visco Baicent Fleur de Lavande 3D (em todas as medidas) oferta de 40% de desconto extra e imediato. Campanha válida durante o dia 26/11/18. Não acumulável com outras campanhas em vigor.',
                'sticky_desktop' => '<span class="commercial-sticky-bar-main-text">CYBER MONDAY</span> <span class="commercial-sticky-bar-sub-text">Prolongámos a black friday: 40% desconto directo extra.</span> <span class="commercial-sticky-bar-main-text">SÓ HOJE!</span>',
                'sticky_mobile'  => 'Prolongámos a black friday: 40% desconto directo extra. SÓ HOJE!',
                'cta_color'      => '009640',
                'logo'           => '/images/blackfriday/cybermondaylogo2.png',
                'stamp_small'    => '/images/blackfriday/stamp3.png',
                'stamp_big'      => '/images/blackfriday/barra3.png',
                'date_start'     => '26-11-2018 00:00:00',
                'date_stop'      => '26-11-2018 23:59:59',
                'sticky_bg'      => '000'
            ]
        ];

        $active_campaign = [];
        foreach ($campaigns as $campaign) {
            $now = time();
            if ($now > strtotime($campaign['date_start']) && $now < strtotime($campaign['date_stop'])) {
                $active_campaign = $campaign;
            }
            break;
        }
        return $active_campaign;
    }
    public function send_email($template, $args = array() ){
        /* Mandatory parameters: template and to */
        $email_tree = (new EmailTree($this->translator))->get();
        if(!$email_tree['templates'][$template]['from']['geral@omeucolchao.pt']){ return array('error' => 'Missing template'); }
        if(!isset($args['to'])) { return array('error' => 'Missing TO argument'); }

        $template_config = [
            'from' => [
                'geral@omeucolchao.pt' => $email_tree['templates'][$template]['from']['geral@omeucolchao.pt']
            ],
            'subject' => $email_tree['templates'][$template]['subject'],
            'template' => $email_tree['templates'][$template]['template'],
            'title' => $email_tree['templates'][$template]['title'],
            'telephone' => $this->getParameter('telephone_number'),
            'static_path' => $this->getStaticPath()
        ];

        /* Merge our given options with the defaults */
        $options = array_merge($template_config, $args);

        /* ensure to is an array */
        $to = is_array($options['to']) ? $options['to'] : array($options['to'] );

        /* render the email through twig */
        $content = $this->renderView( $options['template'], $options );

        /* poor man's templating for the subject line .... */
        $subject = $options['subject'];
        foreach(array_keys($options) as $k){
            if(is_array($options[$k])){ continue; } // Skip over the From parameter..
            $subject = str_replace('{{ ' . $k . ' }}', $options[$k] , $subject);
        }

        if(!isset($options['bcc'])) {
            $options['bcc'] = null;
        }

        $mailer = $this->get('swiftmailer.mailer');
        $message = $mailer->createMessage()
                        ->setSubject($subject)
                        ->setBcc($options['bcc'])
                        ->setFrom($options['from'])
                        ->setTo($to)
                        ->setBody($content, 'text/html');
        $mailer->send($message);

        return array('success'=>true);
    }

    public function toSEOFriendly($text){
        $text = str_replace(" ","_",$text);
        return $text;
    }

    public function cURLPost($url, $fields) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResp = curl_exec($ch);
        $curlError = curl_error($ch);

        curl_close($ch);

        return (object)array('response' => $curlResp, 'error' => $curlError);
    }

    public function hasParameter($name) {
        if (!$this->container->has('parameter_bag')) {
            throw new ServiceNotFoundException('parameter_bag', null, null, array(), sprintf('The "%s::getParameter()" method is missing a parameter bag to work properly. Did you forget to register your controller as a service subscriber? This can be fixed either by using autoconfiguration or by manually wiring a "parameter_bag" in the service locator passed to the controller.', \get_class($this)));
        }

        return $this->container->get('parameter_bag')->has($name);
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            'swiftmailer.mailer' => 'Swift_Mailer'
        ]);
    }

    public function getStaticPath()
    {
        $current_locale = Utils::getLocale();
        foreach ($this->getParameter('domains') as $locale_domain) {
            list($locale, $domain) = explode(':', $locale_domain);
            if ($locale == $current_locale) { return $domain; }
        }

        return $this->getParameter('static_path');
    }

    public function getCurrentFlag()
    {
        $current_locale = Utils::getLocale();
        $flags = $this->getParameter('flags');
        if(@$flags[$current_locale]){
            return $flags[$current_locale];
        }
        return "pt"; // Needs default_locale parameter
    }

}

?>
