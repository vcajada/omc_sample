<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Psr\Log\LoggerInterface;

use \App\Service\BOUsers;
use \App\Service\Users;
use \App\Service\Members;
use \App\Service\Promocodes;
use \App\Service\Products;
use \App\Service\Orders;
use \App\Service\Slides;
use \App\Service\Carts;
use \App\Service\Payments;
use \App\Service\Logs;


class BaseBoController extends AbstractController {

    protected $session;
    protected $logger;
    protected $request;
    protected $product;

    public function __construct(SessionInterface $session, LoggerInterface $logger, RequestStack $request_stack, Products $product) {
        $this->session = $session;
        $this->logger = $logger;
        $this->request = $request_stack->getCurrentRequest();
        $this->product = $product;
    }

    public function getUsers($previewMode = false){

        $this->user = new BOUsers($previewMode);
        return $this->user;
    }

    public function getClientUsers($previewMode = false){

        $this->client_user = new Users($previewMode);
        return $this->client_user;
    }

    public function getMembers($previewMode = false){

        $this->member = new Members($previewMode);
        return $this->member;
    }

    public function getPromos($previewMode = false){

        $this->promo = new Promocodes($previewMode);
        return $this->promo;
    }

    public function getProducts(){

        return $this->product;
    }

    public function getOrders(){

        $this->order = new Orders();
        return $this->order;
    }

    public function getSlides($previewMode = false){

        $this->slides = new Slides($previewMode);
        return $this->slides;
    }

     public function getCarts(){

        $this->cart = new Carts();
        return $this->cart;
    }

    public function getPayments($previewMode = false){

        $this->payment = new Payments($previewMode);
        return $this->payment;
    }

    public function getLogs() {

        $this->logs = new Logs();
        return $this->logs;
    }

    protected function getRenderedContent($template, $controllerContents) {

        // not doing anything with this - vlopes
        //$requestError = $app["request"]->get('error');

        // Header and Footer Contents that will be displayed in every pages
        $current_year = date('Y');
        $generalContents = [
                'PageTitle' => 'O meu Colchão | BackOffice',
                'CurrentYear' => $current_year,
                'user' => $this->session->get('omc_bo_user'),
                'locales' => $this->getParameter('locales')
            ];

        // Merge generalContents with controllerContents
        $outputContent = array_merge($generalContents, $controllerContents);

        // Render Twig template response
        return $this->render(
            $template,
            $outputContent
        );

    }

}

?>
