<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class BoLoginController extends BaseBoController
{

    public function backOfficeLoginForm()
    {
        return $this->getRenderedContent('bo_login.twig', array());
    }

    public function loginAction()
    {
        $user = $this->getUsers();
        $username =  $this->request->get("username");
        $password =  $this->request->get("password");
        $session = $this->session;

        $hasLogin = $user->login($username, $password, $session);

        if ($hasLogin == false) {
            return new JsonResponse(['status'=>false, 'message'=>"Utilizador ou Password inv&aacute;lidos!"]);
        } else {
            return new JsonResponse(['status'=>true]);
        }
    }
    
    public function logoutAction()
    {
        $session = $this->session;
        $session->remove('omc_bo_user');
        return new JsonResponse(['status'=>true]);
    }

    public function removeOption()
    {
        $stk = $this->request->get("stk");
        $idProduct = $this->request->get("id");
        $product = $this->getProducts();
                
        $delete = $product->deleteOption($stk);

        return $this->redirectToRoute('backoffice-edit-produto', ['product' => $idProduct]);
    }
}
