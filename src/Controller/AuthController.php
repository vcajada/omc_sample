<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use \App\Controller\BaseController;
use \App\Service\Users;
use \App\Service\Logs;

use Hybrid\Auth;

 
class AuthController extends BaseController {

/**
 * Login Action
 */
    public function loginAction(Users $user)
    {
        $username =  $this->request->get("email");
        $password =  $this->request->get("password");

        $hasLogin = $user->login($username, $password);

        if ($hasLogin == false) {
            return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Utilizador ou Password inv&aacute;lidos!")]);
        } else {              
            return new JsonResponse(['status'=>true]);
        }
    }
    
    public function logoutAction()
    {
        $this->session->remove('omc_user');
        return new JsonResponse(['status'=>true]);
    }
    
    public function registerAction(Users $user)
    {
        $name = $this->request->get("nome");
        $email = $this->request->get("email");
        $password = $this->request->get("password");
        $accept_data = $this->request->get("accept-data");
        $accept_terms = $this->request->get("accept-terms");
        $recaptcha = $this->request->get("g-recaptcha-response");

        $secret = '6Ldqq1gUAAAAAEA1wa3AGQ-fMAVRxUM3ZiKYtyVW';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha);
        $responseData = json_decode($verifyResponse);
        
         if (empty($name) && empty($email) && empty($password)){
             return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Tem de preencher todos os campos")]);
        }
        
        if (empty($accept_data)) {
            return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Tem de aceitar a política de dados")]);
        }
        
        if (empty($accept_terms)) {
            return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Tem de aceitar os termos e condições")]);
        }
        
        if (!empty($name) &&
            !empty($email) &&
            !empty($password) &&
            false !== filter_var($email, FILTER_VALIDATE_EMAIL)
        ) {
            // Validating if member already exists
            $hasUser = $user->existsAndIsActive($email);

            if ($hasUser) {
                return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("O utilizador já existe, por favor faça login!")]);
            }

            if(!$responseData->success){
                return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Valide o Recaptcha, por favor.")]);
            }

            $userRegistration = $user->create($name, $email, $password);

            if ($userRegistration['status'] == true) {
                // Sending the email

                $contents =
                     array(
                         'to' => array( $email => $name),
                         'name' => $name,
                         'username' => $email,
                         'password' => $password,
                         'sson' => $userRegistration['sson_hash'],
                     );

                $result = $this->send_email('user_register', $contents);

                // Auto login member
                $user->tryAutologin($userRegistration['sson_hash']);

                return new JsonResponse(['status'=>true]);
            }
        }

        return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans("Ocorreu um erro no seu registo")]);
    }

    public function recoverpasswordAction(Users $user)
    {
        $email = $this->request->get("email");

        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O email fornecido é inválido!')]);
        }

        $userData = $user->resetPassword($email);

        $newPassword = $userData[1];
        $userObject = $userData[0];
        $sson = $userObject->sson;

        if (!$userObject->id) {
            return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O email fornecido não se encontra registado!')]);
        }

        // Send email to user with new password

        $contents =
             array(
                'to' => array( $userObject->email => $userObject->name),
                'name' => $userObject->name,
                'username' => $userObject->email,
                'sson'=>$userObject->sson,
                'password'=>$newPassword,
                'title'=>'Recuperação de Password'
             );

        $result = $this->send_email('recover_pass', $contents);

        ##TODO proper error handling

        return new JsonResponse(['status'=>true, 'message'=>$this->translator->trans('Foi enviada uma nova password por email.')]);
    }

    public function callbackFacebookAction()
    {
        $endPoint = new \Hybrid_Endpoint();
        $endPoint->process();
    }

    public function loginFacebookAction(Users $member)
    {
        $referer = $this->request->server->get('HTTP_REFERER');

        // This callback is called in several steps. Prevent external redirects
        if(strpos($referer,'omeucolchao') !== false){
            $this->session->set('origin_url', $referer);
        }

        $config = array(
            "base_url" => $this->getParameter('facebook_oauth.callback'),
                "providers" => array(
                    "Facebook" => array(
                        "enabled" => true,
                        "keys" => array(
                            "id" => $this->getParameter('facebook_oauth.id'),
                            "secret" => $this->getParameter('facebook_oauth.secret'),
                        ),
                        "scope" => $this->getParameter('facebook_oauth.scope'),
                        "display" => $this->getParameter('facebook_oauth.display')
                    )
                )
        );


        try {
            $hybridauth = new \Hybrid_Auth($config);
            $adapter = $hybridauth->authenticate("Facebook");

            $user_profile = $adapter->getUserProfile();
        } catch (Exception $e) {
            $this->logger->warning("FB authentication Exception: ". $e->getMessage());
        }

        // User may opt out from giving us an email address, so check it

        $this->logger->warning("PROFILE:".json_encode($user_profile));

        if ($user_profile && $user_profile->emailVerified) {
         
            $this->session->set('facebook_authentication', $user_profile);

            $existingMember = $member->getByEmail($user_profile->email);

            if ($existingMember) {
                $member->forceLogin($existingMember);
                return $this->redirectToRoute('homepage');
            }

            return $this->redirectToRoute('facebook_welcome');
  
        }

        return $this->redirectToRoute('homepage');
        // no action register in homepagecontroller TODO check
        $url = $app['url_generator']->generate('homepage', array( 'action' => 'register' , 'error' => 'fb_auth' ));
        return $app->redirect($url);
    }

    
    
    public function facebookWelcomeAction(Users $user)
    {
        $contents = [
            'lockWebsite' => false,
            'skipRegistrationModal' => true,
            //'content' => $content,
            'contentTitle' => $this->translator->trans('Confirme o seu registo através do facebook'),
        ];

        if (!$this->session->get('facebook_authentication')) {
            $contents['fb_error'] = $this->translator->trans('Ocorreu um erro a receber informação do facebook, reinicie o processo de inscrição.');
        }

        // If we don't have session info or if this isn't a submit/post, return with view
        if (!$this->request->isMethod('post') || isset($contents['fb_error']) ) {
            return $this->getRenderedContent($this->app, 'facebook_welcome.twig', $contents);
        }


        $terms = $this->request->get("termos");
        $politica_de_dados = $this->request->get("politica_de_dados");

        if (!$terms) {
            $contents['errors'][] = $this->translator->trans('Aceite os Termos e Condições');
        }
        if (!$politica_de_dados) {
            $contents['errors'][] = $this->translator->trans('Aceite a Política de Tratamento de Dados');
        }

        // Return if errors
        if (isset($contents['errors'])) {
            return $this->getRenderedContent($this->app, 'facebook_welcome.twig', $contents);
        }
        
        $user_profile = $this->session->get('facebook_authentication');

        $this->logger->warning(json_encode($user_profile));
        //$log->addInfo(var_dump($user_profile));

        $email = $user_profile->emailVerified;
        $name = $user_profile->displayName;
        $result = $this->_social_network_login($user, $email, $name/*function doesn't do anything with this, 108773*/);

        $this->logger->warning(json_encode($result));

          if (! empty($result['newUserId']) && $result['newUserId']) {
              //  $app->log("Registering new user");
                
                $userId = $result['newUserId'];
                $facebookId = $user_profile->identifier;
                $user->register_from_facebook($userId, $facebookId);
                $user_info = $result['user'];

                $contents =
                     array(
                         'to' => array( $user_profile->email => $user_profile->name),
                         'name' => $user_profile->name,
                         'username' => $user_profile->email,
                         'sson' => $user_info['sson'],
                     );

                $result = $this->send_email('user_register', $contents);

                $analytics = new \OMC\Analytics();
                $analytics->trackSignup();
            }

        
        return $this->redirectToRoute('homepage');
    }
    
    
    
    public function rgpdWelcomeAction(Users $user, Logs $log)
    {   
        $usersession = $this->session->get('omc_user');
        $userData = $user->getUserData($usersession['id_user']);
        
        if(!$userData->rgpd){                
            
           $contents = [
            'lockWebsite' => false,
            'skipRegistrationModal' => true,
            //'content' => $content,
            'contentTitle' => $this->translator->trans('Aceitar nos Termos e Condições'),
        ];
        if (!$this->request->isMethod('post')) {
            return $this->getRenderedContent($this->app, 'rgpd_email.twig', $contents);
        }

        $terms = $this->request->get("termos");
        $politica_de_dados = $this->request->get("politica_de_dados");

        if (!$terms) {
            $contents['errors'][] = $this->translator->trans('Aceite os Termos e Condições');
        }
        if (!$politica_de_dados) {
            $contents['errors'][] = $this->translator->trans('Aceite a Política de Tratamento de Dados');
        }

        // Return if errors
        if (isset($contents['errors'])) {
            return $this->getRenderedContent($this->app, 'rgpd_email.twig', $contents);
        }
        
        
        
        $update = $user->updateRGPD($usersession['id_user']);
        
        if($update){
            $resultlogs = $log->insertLog($usersession['id_user'], 'RGPD Accepted');
        }
        

        
            return $this->redirectToRoute('homepage'); 
        }
        
        return $this->redirectToRoute('homepage'); 
    }
    
    public function registerNewsletterAction() {

        $username = $this->request->get("username");

        $UserNewsletter = new UserNewsletter($username);

        if (empty($username) || false === filter_var($username, FILTER_VALIDATE_EMAIL) ) {
            return new JsonResponse([
                'status' => false,
                'message' =>$this->translator->trans('O endere&ccedil;o email inserido n&atilde;o &eacute; v&aacute;lido. Por favor tente outra vez.') 
            ]);
        }

        if (!empty($UserNewsletter->get())) {
            return new JsonResponse([
                'status' => false,
                'message' =>$this->translator->trans('O endere&ccedil;o email inserido j&aacute; se encontra registado.') 
            ]);
        }

        $UserNewsletter->create($username);

        return new JsonResponse([
            'status' => true,
            'message' => $this->translator->trans('Vai agora passar a receber os melhores descontos em colch&otilde;es no seu email!') 
        ]);
    }

    private function _social_network_login($user, $email, $name)
    {
        // verify if member already exists in database, if not create new member
        $existingUser = $user->getByEmail($email);

        $newUserResult = null;
        if (false == $existingUser) {
            // user does not exit, create
            $password = '';

            $newUserResult = $user->create($name, $email, $password);
            $existingUser = $user->getByEmail($email);
        }

        // autenticate member
        $user->forceLogin($existingUser);

        $newUserId = (empty($newUserResult) ? 0 : $newUserResult['user_id']);
        return array( 'newUserId' => $newUserId, 'user' => $existingUser );
    }

}

?>
