<?php

namespace App\Controller;

use \App\Controller\BaseController;
use \App\Service\Products;

class SandboxController extends BaseController {

    public function sandboxAction() {

        $analytics = new \App\Service\Analytics();
        $analytics->trackTest();

    /*
        $ga_visitor_id = '';
        $ga_cookie = $request->cookies->get('_ga');

        if($ga_cookie){
            $ga_visitor_id = explode(".",$ga_cookie)[2];
        }

        $contents = array();

        $analytics = new \OMC\Analytics;
        $analytics->trackOrder(
            array('type' => 'CC', 'total' => '1' )
        );
    */

        $contents = array( 'output' => var_export($analytics->getDataLayer() ) );

        return $this->getRenderedContent('/sandbox.twig', $contents);
        

    #    $contents =
    #         array(
    #             'to' => array( 'manuel.capinha@clubefashion.com' => 'Manuel Capinha'),
    #             'name' => 'Manuel Capinha',
    #             'username' => 'manuel.capinha@clubefashion.com',
    #             'password' => 'XPTO',
    #             'website_url' => 'https://www.omeucolchao.pt',
    #             'sson' => 'SSON',
    #             'param1' => 'PARAM1',
    #             'title' => 'Bem vindo.'
    #         );
    #
    #    $result = $this->send_email('user_register', $contents);
    #    #$contents = [ 'result' => $result ];
    #    
    #    return $this->getRenderedContent($app, '/emails/user_register.twig', $contents);
    
    }
}

?>
