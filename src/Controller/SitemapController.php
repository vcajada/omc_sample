<?php

namespace App\Controller;

use \App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends BaseController
{

    public function sitemapAction()
    {

        $products = \App\Model\Article::active()->get();

        $contents = [
            'products' => $products,
        ];

        return new Response(
            $this->render(
                'sitemap.xml.twig',
                $contents
            ),
            200,
            array('Content-Type' => 'application/xml')
        );
    }
}
