<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use \App\Controller\BaseController;
use \App\Service\Carts;
use \App\Service\Users;
use \App\Service\Analytics;

class CartController extends BaseController {

    public function cartStart(Carts $cart, Users $user) {

        $cartsession = $this->session->get('omc_cart');
        $usersession = $this->session->get('omc_user');

        if($cartsession){
            $items = $cart->getCartList($cartsession);
        }else{
            $items = [];
        }
        if($usersession){
            $user_data = $user->getUserData($usersession['id_user']);
            $user_address = $user->getUserAddress($usersession['id_user']);
            $user_billing = $user->getUserBilling($usersession['id_user']);
        }else{
            $user_data = '';
            $user_address = '';
            $user_billing = '';
        }

        $ecomm_prodid = implode(',',array_map(function($i){return $i['article_id'];},$items));

         $contents = [
                'items' => $items,
                'userdata' => $user_data,
                'address'   => $user_address,
                'billing'   => $user_billing,
                'ecomm_prodid' => $ecomm_prodid
            ];

        return $this->getRenderedContent('cart.twig', $contents);

        }

    public function addCart(Carts $cart) {

        $cartsession = $this->session->get('omc_cart');
        $article_id =  $this->request->get("article_id");
        $user_id =  $this->request->get("user_id");

        if(!$user_id) $user_id = null;

        if(!$cartsession){
            $newcart = $cart->create($user_id, $this->session);
            $cartsession = $this->session->get('omc_cart');
        }

        $has_item = $cart->has_item($article_id, $cartsession);
        $item_status = $has_item[0];

        if(isset($has_item[1])){
            $cart_item = $has_item[1];
        }


        if($item_status == true){
            $currentqtd = $cart_item->quant;
            $currentid = $cart_item->item_id;

            $item_quant = ($currentqtd + 1);
            $update = $cart->update_item($currentid, $item_quant);
        }else{
            $item_quant = '1';
            $newitem = $cart->add_item($article_id, $cartsession, $item_quant);
        }

        return new JsonResponse(['status'=>true, 'message'=>$this->translator->trans('O Produto foi adicionado ao seu carrinho!'), 'article_sum' => $cart->getArticleSum($cartsession)]);
    }

    public function removeItem(Carts $cart) {

        $item_id =  $this->request->get("item_id");
        $remove = $cart->remove_item($item_id);
        if($remove[0]){
            if($remove[1]){
                $this->session->remove('omc_cart');
            }
            return new JsonResponse(['status'=>true]);
        }else{
            return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('De momento não é possível efectuar a operação!')]);
        }

    }

    public function updateCart(Carts $cart) {

        $item_id =  $this->request->get("item_id");
        $item_quant =  $this->request->get("item_quant");
        $update = $cart->update_item($item_id, $item_quant);

        return new JsonResponse(['status'=>true], $cart->getArticleSum($this->session->get('omc_cart')));

    }

    public function checkPromo(Carts $cart) {

        $promo =  $this->request->get("value");
        $user = $this->request->get("user");
        $amount = $this->request->get("buy");
        $is_valid = $cart->checkPromo($promo, $user, $amount);

        $currentdate = date("Y-m-d H:i:s");

        if($is_valid[0]){
            $promocode = $is_valid[1];
            return new JsonResponse(['status'=>true, 'promoval'=>$promocode->value, 'promotype'=>$promocode->type]);
        }else{
            $promocode = $is_valid[1];
            if(!$promocode){
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O Código não é válido!')]);
            }elseif($promocode->status != 'ACTIVE'){
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O Código não se encontra activo!')]);
            }elseif($promocode->end_date < $currentdate){
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O validade do código já se encontra expirada!')]);
            }elseif($promocode->user_id != '0' && $promocode->user_id != $user){
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O código não é válido para si')]);
            }elseif($promocode->min_buy != '0' && $promocode->min_buy >= $amount){
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans(
                    'O valor mínimo de compra para a utilização deste código é %code%€',
                    ['%code%' => $promocode->min_buy]
                )
            ]);
            }else{
                return new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('Atingiu o limite de utilizações para este código')]);
            }

        }
    }

    public function getCartInfo(Carts $cart){
        $cartsession = $this->session->get('omc_cart');
        $articles = $cart->getCartArticlesInfoFromSession($cartsession);

        return  new JsonResponse(['articles'=>$articles]);
    }


    public function processCart(\Swift_Mailer $mailer, Carts $cart, Users $user)
    {

        $analytics = new Analytics();

        $promo =  $this->request->get("promocode");
        $shipping = $this->request->get("shipopt");
        $username = $this->request->get("nome");
        $useremail = $this->request->get("email");
        $userphone = $this->request->get("phone");
        $usernif = $this->request->get("nif");
        $userstreet = $this->request->get("street");
        $userstreet2 = $this->request->get("street2");
        $usercp1 = $this->request->get("cp1");
        $usercp2 = $this->request->get("cp2");
        $userlocal = $this->request->get("local");
        $billing = $this->request->get("bill");
        $billname = $this->request->get("bname");
        $billstreet = $this->request->get("bstreet");
        $billstreet2 = $this->request->get("bstreet2");
        $billcp1 = $this->request->get("bcp1");
        $billcp2 = $this->request->get("bcp2");
        $billlocal = $this->request->get("blocal");
        $billphone = $this->request->get("bphone");
        $billnif = $this->request->get("bnif");
        $paymethod = strtoupper($this->request->get("pay"));
        $register = $this->request->get("new");
        $cartsession = $this->session->get('omc_cart');
        $ship_cost_continent = $this->request->get("ship_cost_continent");

        // Extract ga_visitor_id straight from the cookies
        $ga_visitor_id = '';
        $ga_cookie = $this->request->cookies->get('_ga');

        if($ga_cookie){
            $ary  = explode(".",$ga_cookie);
            $ga_visitor_id = $ary[2] . "." . $ary[3];
        }


        // Check if logged is
        $usersession = $this->session->get('omc_user');
        if(!$usersession['id_user']){

            // Verifies if the given email is in use by a user
            $hasUser = $user->existsAndIsActive($useremail);
            if ($hasUser) {
                return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('O utilizador já existe, por favor faça login!')]);
            }

            $password = $user->GenerateHash();
            $userRegistration = $user->create($username, $useremail, $password);
            if ($userRegistration['status'] == true) {
                // Sending the email

                $content = $this->render(
                    'emails/user_register.twig',
                    [
                        'username'=>$useremail,
                        'sson'=>$userRegistration['sson_hash'],
                        'password'=>$userRegistration['password'],
                        'title'=>'Dados de Registo'
                    ]
                );

                $message = new \Swift_Message();
                $message->setSubject('Dados de Registo')
                    ->setFrom(array('noreply@omeucolchao.pt' => $this->translator->trans("O Meu Colchão")))
                    ->setTo(array($useremail))
                    ->setBody($content, 'text/html');
                $mailer->send($message);

                // Auto login member
                $this->autologin($userRegistration['sson_hash']);
                $usersession = $this->session->get('omc_user');

                $analytics->trackSignup();
            }
        }


        if($shipping != '3'){ // Se a opção de entrega for diferente de levantamento em loja atualiza a morada do cliente.
            $user_update = $user->updateDetails($usersession['id_user'], $username, $userstreet, $userstreet2, $usercp1, $usercp2, $userlocal, $userphone, $usernif);
        }
        $main_address = 'yes';
        $user_del_addr_id = $user->getUserAddrId($usersession['id_user'], $main_address);


        if($billing == 'yes'){
            $main_address = 'no';
            // Update user secondary address
            $billing_update = $user->updateBilling($usersession['id_user'], $billname, $billstreet, $billstreet2, $billcp1, $billcp2, $billlocal, $billphone, $billnif);
            $user_inv_addr_id = $user->getUserAddrId($usersession['id_user'], $main_address);
        }else{
            $user_inv_addr_id = $user->getUserAddrId($usersession['id_user'], $main_address);
        }

        $del_addr_id = '1'; // Id do levantamento em loja
        if($shipping != '3'){ // Se a opção não for levantamento em loja...
            $del_addr_id = $user_del_addr_id;
        }

        // Save Cart Information
        $cart_update = $cart->updateCart($cartsession, $usersession['id_user'], $promo, $shipping, $del_addr_id, $user_inv_addr_id, $paymethod, $ga_visitor_id);

        // Update Cart Items to Orders --> Transforms cart items into orders by adding a order status tag and order details
        if (!$orders = $cart->itemstoOrders($cartsession)) {
            return  new JsonResponse(['status'=>false, 'message'=>$this->translator->trans('Ocorreu um erro. Por favor contacte o Apoio ao Cliente para obter suporte.')]);
        }

        // Update Stocks
        $stocks = $cart->updateStocks($cartsession);

        // Set payment session

        if($paymethod == 'PAY-MB'){
            $this->session->set('order-payments-mb', $paymethod);
        }
        elseif($paymethod == 'PAY-FREE'){
            $this->session->set('order-payments-free', $paymethod);
        }

        // Get cart info from session to compare shipping costs and know if it changed
        $cartArr = $cart->getCartFromSession($cartsession);
        $total = $cartArr['cart_total'];
        if($cartArr['cart_shipping'] > 0) {
            $total = $cartArr['cart_total'] + $cartArr['cart_shipping'];
        }

        // If shipping to islands show modal with updated shipping cost and redirect user to payment
        if ($shipping == 2 && $cartArr['cart_shipping'] != $ship_cost_continent) {
            return new JsonResponse([
                'status' => true,
                'message' => $this->translator->trans(
                    'Atualizámos o preço dos portes uma vez que escolheu uma morada de entrega em Portugal não Continental. O novo preço dos portes é '.$cartArr['cart_shipping'].'€, e o novo valor total da encomenda é %total%€.',
                    ['%total%' => $total]
                ),
                'ship_cost_updated' => true
            ]);
        }

        // Return Response
        return new JsonResponse([
            'status'=>true,
            'ship_cost_updated' => false
        ]);


    }

    public function cartRecover($cart_hash) {
        $cart = \App\Service\Carts::getCartfromSession($cart_hash);

        if($cart){
            $ok = true;

            if($cart->user_id ){
                $user = $this->currentUser();
                if(!$user || $user->id != $cart->user_id){
                    $this->logger->debug("Refusing cart recovery for hash {$cart_hash} that doesn't belong to current_user_id {$user->id}") ;
                    $ok = false;
                }
            }

            if($cart->status != "NEW"){
                $this->logger->debug("Refusing cart recovery for hash {$cart_hash} with current status {$cart->status}") ;
                $ok = false;
            }

            if($ok){
                $this->logger->debug("Recovering cart for hash {$cart_hash}") ;
                $this->session->set('omc_cart',$cart_hash);
            }
        }

        return $this->redirectToRoute('carrinho' );
    }

}

?>
