<?php

namespace App\Controller;

interface EnsureBoAccessController {

    // As per symfony documentation https://symfony.com/doc/current/event_dispatcher/before_after_filters.html
    // empty interface to implement on all controllers needing BO Authentification
}
