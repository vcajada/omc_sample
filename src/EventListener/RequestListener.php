<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    private $domains;

    public function __construct($domains, $default_locale)
    {
        $this->domains = $domains;
        $this->default_locale = $default_locale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $request_host = $request->getHost();

        foreach ($this->domains as $locale_domain) {
            list($locale, $domain) = explode(':', $locale_domain);

            if ($domain === $request_host) {
                $request->setLocale($locale);
            }
        }

        $request->setDefaultLocale($this->default_locale);
    }

}
