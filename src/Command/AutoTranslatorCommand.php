<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


use Google\Cloud\Translate\TranslateClient;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Session\Session;

class AutoTranslatorCommand extends Command
{
    protected static $defaultName = 'auto-translator';
    private $translationClient = null;

    protected function configure()
    {
        $this
            ->setDescription('Translates the site using Google Translator')
            ->addArgument('locale', InputArgument::REQUIRED, 'Target locale')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $target_locale = $input->getArgument('locale');

        /* Nasty hack */
        $GLOBALS['in_bo'] = true;
        $session = new Session();
        $session->set('locale',$target_locale);


        $filename = __DIR__."/../../translations/messages.${target_locale}.yml";

        $translatable_strings = Yaml::parseFile($filename);
        $translated = [];
        if(!count($translatable_strings)){
            die("No strings to translate");
        }

        echo "Translating YAML strings" . PHP_EOL;

        foreach($translatable_strings as $k => $v){
            $translation = $this->translate($target_locale, $k);
            echo "\t$k".PHP_EOL;
            //$translate->translate($k, [
            //    'source' => 'pt',
            //    'target' => $target_locale
            //]);
            //$translated[$k] = $translation['text'];

            $translated[$k] = $translation;
        }

        $yaml = Yaml::dump($translated);
        file_put_contents($filename.'.auto_translated', $yaml);
        echo $yaml;

        echo "Saved YAML strings" . PHP_EOL;

        $articles = \App\Model\Article::where('id','>=','1')->orderBy('id')->get();

        echo "Translating articles" . PHP_EOL;

        foreach($articles as $article){

            if(in_array($article->id,[572,576, 578, 579, 625, 626])){
                echo "Skipping " . $article->id . PHP_EOL;
                continue;
            }

            echo PHP_EOL;
            echo "======================================" . PHP_EOL;
            echo "Translating for id " . $article->id . PHP_EOL;

            $title = $article->translate('pt')->title . PHP_EOL;
            $description = $article->translate('pt')->description . PHP_EOL;
            $conditions = $article->translate('pt')->conditions . PHP_EOL;

            $tr_title = $this->translate($target_locale, $title);
            echo $tr_title.PHP_EOL;

            $tr_description = $this->translate($target_locale, $description);
            echo $tr_description.PHP_EOL;

            $tr_conditions = $this->translate($target_locale, $conditions);
            echo $tr_conditions.PHP_EOL;

            $article->title = $tr_title;
            $article->description = $tr_description;
            $article->conditions = $tr_conditions;
            $article->save();

            /*
            echo $article->translate($target_locale)->title . PHP_EOL;
            echo $article->translate($target_locale)->description . PHP_EOL;
            echo $article->translate($target_locale)->details . PHP_EOL;
            */
        }


        $io->success('Translation done');
    }

    private function translate($locale, $string){

        $client = $this->getTranslationClient();
        $translation = $client->translate($string, [
            'source' => 'pt',
            'target' => $locale
        ]);

        return $translation['text'];
    }

    private function getTranslationClient()
    {
        if (!$this->translationClient) {
            $this->translationClient = new TranslateClient([
                'projectId' => 'repetitions-41a75'
            ]);
        }

        return $this->translationClient;
    }
}
